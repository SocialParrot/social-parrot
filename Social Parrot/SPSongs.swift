//
//  SPSongs.swift
//  Social Parrot
//
//  Created by Abdul Malik Aman on 28/06/16.
//  Copyright © 2016 INTELLIPATTERN. All rights reserved.
//

import UIKit
import AVFoundation
import MediaPlayer
import Social
import GoogleMobileAds

class SPSongs: UIViewController, FloatRatingViewDelegate, SocialParrot_Delegate, keyBoardUpDelegate, GADBannerViewDelegate, AVAudioPlayerDelegate, UIDocumentInteractionControllerDelegate {
    
    // MARK: - Variables
    @IBOutlet weak var ratingView: FloatRatingView!
    var socialDelegate: SocialParrot_Delegate?
    @IBOutlet weak var BackBtn: SPRoundButton!
    @IBOutlet weak var PlayListTitle: UILabel!
    @IBOutlet weak var PlayListBG: UIImageView!
    @IBOutlet weak var SongsTable: UITableView!
    @IBOutlet weak var CommentContainer: UIView!
    @IBOutlet weak var mainContainer: UIView!
    @IBOutlet weak var txtFav: UILabel!
    @IBOutlet weak var txtCmt: UILabel!
    @IBOutlet weak var shareContainer: UIView!
    @IBOutlet weak var shareMenu: UIView!
    var is_AdShow = false
    var adTimer = NSTimer()
    
    @IBOutlet weak var addSongPlus: SPRoundButton!
    
    @IBOutlet weak var FavImg: UIImageView!
    
    var activityLoadIndex: Int!
    var screenShot: UIImage!
    
    var documentController: UIDocumentInteractionController!
    var is_shareShown = false
    var SongsArray: NSArray!
    var playListId: String!
    var playListName: String!
    var playListUserId: String!
    var BGImageData: NSURL!
    var socialPlayer: AVAudioPlayer!
    var is_Playing = false
    var timeObserver: AnyObject!
    var is_Favourite = false
    var is_Show_Comments = false
    var commentHeight: CGFloat!
    var commentController: SPComments!
    var RateResults: NSDictionary!
    var songColor: UIColor!
    var selectedIndex: Int!
    var tempSelectedIndex: Int!
    let playImage = UIImage(named: "Songs_Play")
    let pauseImage = UIImage(named: "Songs_Pause")
    var swipeRight = UISwipeGestureRecognizer()
    var load: SPSongsCell!
    var fav: Int!
   // var newcell: UITableViewCell
    
    // MARK: - View Methods
    override func viewWillDisappear(animated: Bool) {
        socialDelegate?.hideBackButton!(false)
        socialDelegate?.hideLogoutButton!(false)
        
        if(is_Playing == true) {
            self.socialPlayer.pause()
        }
    }
    override func viewWillAppear(animated: Bool) {
        
        addSongPlus.hidden = true
        
        CommentContainer.hidden = true
        is_Show_Comments = false
        socialDelegate?.hideBackButton!(true)
        socialDelegate?.hideLogoutButton!(true)
        if playListUserId == nil {
            playListUserId = USER_ID
        }
        
        if playListUserId == USER_ID {
            addSongPlus.hidden = false
        }
        
        if fav == 1{
            addSongPlus.hidden = true
        }
        
        
        let params = ["PlayListId": playListId, "UserId": playListUserId]
        postSongs(params)
//        if BGImageData != nil {
//            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0)) {
//                if let data = NSData(contentsOfURL: self.BGImageData) {
//                    dispatch_async(dispatch_get_main_queue()) {
//                        self.PlayListBG.image = UIImage(data: data)
//                    }
//                }
//            }
//        }
        
        adTimer = NSTimer.scheduledTimerWithTimeInterval(5, target: self, selector: "showAd", userInfo: nil, repeats: false)
    }
    override func viewDidLoad() {
        super.viewDidLoad()

        swipeRight = UISwipeGestureRecognizer(target: self, action: "respondToSwipeGesture:")
        swipeRight.direction = UISwipeGestureRecognizerDirection.Right
        self.view.addGestureRecognizer(swipeRight)
        
        commentHeight = mainContainer.frame.size.height
        
        PlayListTitle.text = playListName
        if BGImageData != "" {
//            PlayListBG.image = SP_DELEGATE.decodeImage(BGImageData)
        }
        ratingView.delegate = self
        
        self.selectedIndex = nil
        self.activityLoadIndex = nil
        self.is_Playing = false
        SongsTable.reloadData()
        // Do any additional setup after loading the view.
    }
    
    // MARK: - Actions
    func showAd() {
        if is_AdShow == false {
            is_AdShow = true
        }else {
            is_AdShow = false
        }
        SongsTable.reloadData()
    }
    func respondToSwipeGesture(gesture: UIGestureRecognizer) {
        if let swipeGesture = gesture as? UISwipeGestureRecognizer {
            switch swipeGesture.direction {
                case UISwipeGestureRecognizerDirection.Right:
                    if playListUserId == USER_ID {
                        if SongsArray != nil {
                            self.screenShot = self.takeScreenShots()
                            showShareOption()
                        }
                    }
                
            default:
                break
            }
        }
    }
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        if is_Show_Comments == true {
            CommentContainer.hidden = true
            is_Show_Comments = false
            commentController.view.endEditing(true)
            let params = ["PlayListId": playListId, "UserId": USER_ID!]
            postSongs(params)
        }
        if is_shareShown == true {
            showShareOption()
        }
    }
    @IBAction func Purchase(sender: AnyObject) {
        let url = SongsArray[sender.tag]["iTunesURL"] as? String
        if let appURL = NSURL(string: url!) {
            let canOpen = UIApplication.sharedApplication().canOpenURL(appURL)
            print("Can open \"\(appURL)\": \(canOpen)")
            UIApplication.sharedApplication().openURL(appURL)
        }else {
            UIApplication.sharedApplication().openURL(NSURL(string: url!)!)
        }
        
    }
    @IBAction func Play(sender: AnyObject) {
        playSong(sender.tag)
    }
    
    func playSong(playIndex: Int) {
        activityLoadIndex = playIndex
        SongsTable.reloadData()
        let musicUrl = SongsArray[playIndex]["URL"] as? String
        
        dispatch_async(dispatch_get_main_queue(), { () -> Void in
            if self.is_Playing == false {
                self.selectedIndex = playIndex
                self.tempSelectedIndex = playIndex
                do {
                    let fileURL = NSURL(string:musicUrl!)
                    let soundData = NSData(contentsOfURL:fileURL!)
                    self.socialPlayer = try AVAudioPlayer(data: soundData!)
                    self.socialPlayer.prepareToPlay()
                    self.socialPlayer.volume = 1.0
                    self.socialPlayer.delegate = self
                    self.socialPlayer.play()
                    self.activityLoadIndex = nil
                    self.is_Playing = true
                    print("Playing")
                } catch {
                    print("Error getting the audio file")
                }
                self.SongsTable.reloadData()
            }else {
                if self.tempSelectedIndex == playIndex {
                    self.selectedIndex = nil
                    self.activityLoadIndex = nil
                    self.is_Playing = false
                    self.socialPlayer.pause()
                    print("Paused")
                }else {
                    self.is_Playing = false
                    self.activityLoadIndex = nil
                    self.playSong(playIndex)
                    print("Stopped")
                }
                self.SongsTable.reloadData()
            }
        })
    }
    
    @IBAction func Comments(sender: AnyObject) {
        if is_Show_Comments == false {
            CommentContainer.hidden = false
            is_Show_Comments = true
        }else {
            CommentContainer.hidden = true
            is_Show_Comments = false
        }
        commentController = self.childViewControllers[0] as! SPComments
        commentController.viewWillAppear(true)
    }
    @IBAction func hideComments(sender: AnyObject) {
        CommentContainer.hidden = true
        is_Show_Comments = false
        commentController.view.endEditing(true)
        let params = ["PlayListId": playListId, "UserId": USER_ID!]
        postSongs(params)
    }
    
    @IBAction func Favourite(sender: AnyObject) {
        
    }
    
    @IBAction func Back(sender: AnyObject) {
        self.navigationController?.popViewControllerAnimated(true)
    }
    @IBAction func AddSongs(sender: AnyObject) {
        performSegueWithIdentifier("addSongs", sender: self)
    }
    func commentkeyBoardUp(isUp: Bool) {
        if isUp == true {
            mainContainer.frame.size.height = self.view.frame.height - (KEYBOARD_FRAME)
        }else {
            mainContainer.frame.size.height = commentHeight
        }
    }
    func UIColorFromRGB(rgbValue: UInt) -> UIColor {
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
    func takeScreenShots() -> UIImage {
        let window = UIApplication.sharedApplication().keyWindow
        let screen = UIScreen.mainScreen()
        UIGraphicsBeginImageContextWithOptions(screen.bounds.size, false, 0);
        window!.drawViewHierarchyInRect(window!.bounds, afterScreenUpdates: false)
        let image = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        return image
    }
    @IBAction func shareFacebook(sender: AnyObject) {
        showShareOption()
        if SLComposeViewController.isAvailableForServiceType(SLServiceTypeFacebook){
            let facebookSheet:SLComposeViewController = SLComposeViewController(forServiceType: SLServiceTypeFacebook)
            facebookSheet.setInitialText("*Social Parrots*\n\n" + "http://www.socialParrots.com/playlist/PlayListId")
            facebookSheet.addImage(screenShot)
            self.presentViewController(facebookSheet, animated: true, completion: nil)
        
            
        } else {
            let alert = UIAlertController(title: "Accounts", message: "Please login to a Facebook account to share.", preferredStyle: UIAlertControllerStyle.Alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: nil))
            self.presentViewController(alert, animated: true, completion: nil)
        }
    }
    
    @IBAction func shareTwitter(sender: AnyObject) {
        showShareOption()
        if SLComposeViewController.isAvailableForServiceType(SLServiceTypeFacebook){
            let twitterSheet:SLComposeViewController = SLComposeViewController(forServiceType: SLServiceTypeTwitter)
            twitterSheet.setInitialText("Social Parrots\n")
            twitterSheet.addImage(screenShot)
            twitterSheet.addURL(NSURL(string: "http://www.socialParrots.com/playlist/PlayListId"))
            self.presentViewController(twitterSheet, animated: true, completion: nil)
        } else {
            let alert = UIAlertController(title: "Accounts", message: "Please login to a Twitter account to share.", preferredStyle: UIAlertControllerStyle.Alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: nil))
            self.presentViewController(alert, animated: true, completion: nil)
        }
    }
    
    @IBAction func shareInstagram(sender: AnyObject) {
        showShareOption()
        let instagramURL = NSURL(string: "instagram://app")
        if (UIApplication.sharedApplication().canOpenURL(instagramURL!)) {
            let imageData = UIImageJPEGRepresentation(screenShot!, 1.0)
            let captionString = "Social Parrots \n http://www.socialParrots.com/playlist/1234"
            let writePath = (NSTemporaryDirectory() as NSString).stringByAppendingPathComponent("instagram.igo")
            if imageData?.writeToFile(writePath, atomically: true) == false {
                return
            } else {
                let fileURL = NSURL(fileURLWithPath: writePath)
                self.documentController = UIDocumentInteractionController(URL: fileURL)
                self.documentController.delegate = self
                self.documentController.UTI = "com.instagram.exlusivegram"
                self.documentController.annotation = NSDictionary(object: captionString, forKey: "InstagramCaption")
                self.documentController.presentOpenInMenuFromRect(self.view.frame, inView: self.view, animated: true)
            }
        } else {
            print(" Instagram isn't installed ")
        }
    }
    func showShareOption() {
        if self.shareContainer.hidden == true {
            is_shareShown = true
            UIView.animateWithDuration(0.5, delay: 0.0, options: UIViewAnimationOptions.TransitionNone, animations: { () -> Void in
                self.shareContainer.hidden = false
                self.shareMenu.frame.origin.x = 0
                }) { (finished: Bool) -> Void in
                    
            }
        }else {
            is_shareShown = false
            UIView.animateWithDuration(0.5, delay: 0.0, options: UIViewAnimationOptions.TransitionNone, animations: { () -> Void in
                self.shareMenu.frame.origin.x = -80
                }) { (finished: Bool) -> Void in
                    self.shareContainer.hidden = true
            }
        }
        
    }
    
    // MARK: - TableView Delegate Methods
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        var customSection: Int!
        if is_AdShow == false {
            customSection = 0
        }else {
            customSection = 1
        }
        if SongsArray != nil {
            if is_AdShow == false {
                return SongsArray.count
            }else {
                if section == 1 {
                    return 1
                }else {
                    return SongsArray.count
                }
            }
        }else {
            return 0
        }
    }
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        if is_AdShow == false {
            return 1
        }else {
            return 2
        }
    }
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var identifier: String!
        
        if is_AdShow == false {
            identifier = "Songs"
        }else {
            if indexPath.section == 0 {
                identifier = "Songs"
            }else {
                identifier = "Ad_Cell"
            }
        }
        let cell = SongsTable.dequeueReusableCellWithIdentifier(identifier, forIndexPath: indexPath) as! SPSongsCell
        
        if indexPath.section == 0 {
            cell.ActLoad.tag = indexPath.row
            cell.PlayButton.tag = indexPath.row
            cell.iTunesButton.tag = indexPath.row
            cell.SerialNum.text = "\(indexPath.row + 1)"
            cell.SongName.text = SongsArray[indexPath.row]["Title"] as? String
            cell.SongArtist.text = SongsArray[indexPath.row]["ArtistName"] as? String
            
            cell.SongName.textColor = songColor
            cell.SongArtist.textColor = songColor
            cell.SerialNum.textColor = songColor
            
            if let url = NSURL(string: (SongsArray[indexPath.row]["ImageURL"] as? String)!) {
                cell.SongImage.sd_setImageWithURL(url, placeholderImage: UIImage(named: "playlist"))
            }
            
            
            if self.activityLoadIndex != nil {
                if self.activityLoadIndex == indexPath.row {
                    cell.ActLoad.startAnimating()
                    cell.ActLoad.hidden = false
                }else {
                    cell.ActLoad.startAnimating()
                    cell.ActLoad.hidden = true
                }
            }else {
                cell.ActLoad.startAnimating()
                cell.ActLoad.hidden = true
            }
            if selectedIndex != nil {
                if selectedIndex == indexPath.row {
                    cell.playingImage.image = pauseImage
                }else {
                    cell.playingImage.image = playImage
                }
            }else {
                cell.playingImage.image = playImage
            }
        }else {
            cell.ad_View.adUnitID = "ca-app-pub-8796755118307878/4871416143"
            cell.ad_View.rootViewController = self
            cell.ad_View.loadRequest(GADRequest())
            cell.ad_View.delegate = self
        }
        return cell
    }
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        if is_AdShow == false {
            return 75
        }else {
            if indexPath.section == 1 {
                return 55
            }else {
                return 75
            }
        }
        
    }
    func tableView(tableView: UITableView, editActionsForRowAtIndexPath indexPath: NSIndexPath) -> [AnyObject]? {
            if indexPath.section == 0 {
                if playListUserId == USER_ID {
                    let songID = (SongsArray.objectAtIndex(indexPath.row).objectForKey("ID") as? String)!
                    
                    let delete = UITableViewRowAction(style: UITableViewRowActionStyle.Default, title: "Delete" , handler: { (action:UITableViewRowAction, indexPath:NSIndexPath) -> Void in
                        
                        let params: NSDictionary = ["ID": Int(songID)!]
                        self.postDeleteSongs(params)
                    })
                    
                    delete.backgroundColor = UIColor.redColor()
                    return [delete]
                }else {
                    return []
                }
            }else {
                return []
            }
    }

    // MARK: - WebServices
    func postSongs(params: NSDictionary) {
        socialDelegate?.showActivity!("Loading..")
        SP_NETWORK.postJson(WEB_SERVICE + "api/PlayList/SearchSongSForPlayList", method: "POST", params: params as! Dictionary<String, AnyObject>, is_Dictionary: true) { (Succeeded, Json) -> () in
            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                print(Succeeded)
                print(Json)
                self.socialDelegate?.removeActivity!()
                if Succeeded == true {
                    self.SongsArray = Json["ResultSet"]!["Songs"] as? NSArray
                    
                    if let rate = Json["ResultSet"]!["Rate"] as? String {
                        self.ratingView.rating = Float(rate)!
                    }
                    if let opacity = Json["ResultSet"]!["Opacity"] {
                        if let n = NSNumberFormatter().numberFromString(opacity as! (String)) {
                            self.PlayListBG.alpha = CGFloat(n) / 100
                        }
                    }
                    if let url = NSURL(string: Json["ResultSet"]!["BgImageURL"] as! String) {
                        self.PlayListBG.sd_setImageWithURL(url, placeholderImage: UIImage(named: "playlist"))
                    }
                    if let songColor = Json["ResultSet"]!["Colors"] as? String {
                        if songColor != "" {
                            self.songColor = self.UIColorFromRGB(UInt(songColor)!)
                        }                        
                    }else {
                        self.songColor = UIColor.blackColor()
                    }
                    
                    self.RateResults = Json["ResultSet"] as? NSDictionary
                    
                    self.txtFav.text = self.RateResults["Favorite"] as? String
                    
                    self.txtCmt.text = self.RateResults["Comments"] as? String
                    
                    
                    let params = ["PlayListId": self.playListId, "UserId": self.playListUserId, "CurrentUser": USER_ID!]
                    self.postGetAllComments(params)
                    
                    
                }
                self.SongsTable.reloadData()
            })
        }
    }
    func postDeleteSongs(params: NSDictionary) {
        socialDelegate?.showActivity!("Deleting..")
        SP_NETWORK.postJson(WEB_SERVICE + "api/PlayList/SongsDelete", method: "POST", params: params as! Dictionary<String, AnyObject>, is_Dictionary: true) { (Succeeded, Json) -> () in
            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                print(Succeeded)
                print(Json)
                self.socialDelegate?.removeActivity!()
                if Succeeded == true {
                    if let status = Json["Status"] {
                        if status as? String == "1" {
                            let params = ["PlayListId": self.playListId, "UserId": USER_ID!]
                            self.postSongs(params)
                        }
                    }
                }
            })
        }
    }
    func postRatingPlayList(params: NSDictionary) {
        SP_NETWORK.postJson(WEB_SERVICE + "api/PlayList/UpdateRate", method: "POST", params: params as! Dictionary<String, AnyObject>, is_Dictionary: true) { (Succeeded, Json) -> () in
            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                print(Succeeded)
                print(Json)
                if Succeeded == true {
                    if let status = Json["Status"] {
                        
                    }
                }
            })
        }
    }
    
    
    func postGetAllComments(params: NSDictionary) {
        socialDelegate?.showActivity!("Loading..")
        SP_NETWORK.postJson(WEB_SERVICE + "api/PlayList/CommentsGetAll", method: "POST", params: params as! Dictionary<String, AnyObject>, is_Dictionary: true) { (Succeeded, Json) -> () in
            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                print(Succeeded)
                print(Json)
                self.socialDelegate?.removeActivity!()
                if Succeeded == true {
                    if let status = Json["Status"] {
                        if status as? String == "1" {
                            
                            if let is_favourite = Json["ResultSet"]!["Favorite"] {
                                if is_favourite as? String == "Y" {
                                    
                                    self.FavImg.image = UIImage(named: "menu_Favourite_Sel")
                                }else {
                                    
                                    self.FavImg.image = UIImage(named: "fav_black")
                                }
                                
                            }
                            
                        }
                    }
                }
            })
        }
    }
    
    // MARK: - Google Ad Delegates
    func adViewDidReceiveAd(bannerView: GADBannerView!) {
        print("adViewDidReceiveAd")
    }
    
    func adView(bannerView: GADBannerView!, didFailToReceiveAdWithError error: GADRequestError!) {
        print("adView:didFailToReceiveAdWithError: \(error.localizedDescription)")
    }
    
    func adViewWillPresentScreen(bannerView: GADBannerView!) {
        print("adViewWillPresentScreen")
    }
    
    func adViewWillDismissScreen(bannerView: GADBannerView!) {
        print("adViewWillDismissScreen")
    }
    
    func adViewDidDismissScreen(bannerView: GADBannerView!) {
        print("adViewDidDismissScreen")
    }

    
    // MARK: - Audio Player Delegate
    func audioPlayerDidFinishPlaying(player: AVAudioPlayer, successfully flag: Bool) {
        if is_Playing == true {
            selectedIndex = nil
            is_Playing = false
            socialPlayer.stop()
            print("Stopped")
        }
        SongsTable.reloadData()
    }
    
    // MARK: - TextField Delegates
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
//        searchSongs(textField.text!)
        return true
    }
    
    // MARK: - Custom Rating
    func floatRatingView(ratingView: FloatRatingView, isUpdating rating:Float) {
        
        let params = ["PlayListId": playListId, "RatingUserId": USER_ID!, "Rate": "\(rating)"]
        postRatingPlayList(params)
    }
    
    func floatRatingView(ratingView: FloatRatingView, didUpdate rating: Float) {
        // To get last rating
    }
    
    // MARK: - Custom Delegates
    func hideLogoutButton(isHide: Bool) {
        socialDelegate?.hideLogoutButton!(isHide)
    }
    func navigateTab(tab: Int) {
        socialDelegate?.navigateTab!(tab)
    }
    func hideBackButton(isHide: Bool) {
        socialDelegate?.hideBackButton!(isHide)
    }
    func showActivity(title: String) {
        socialDelegate?.showActivity!(title)
    }
    func removeActivity() {
        socialDelegate?.removeActivity!()
    }
    
    
    // MARK: - System
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "addSongs" {
            let addSongs = segue.destinationViewController as! SPAddSongs
            addSongs.socialDelegate = self
            addSongs.playListId = playListId
        }else if segue.identifier == "Comments" {
            let comments = segue.destinationViewController as! SPComments
            comments.playListId = playListId
            comments.socialDelegate = self
            comments.keyBoardDelegate = self
            comments.playListUserId = playListUserId
        }
    }

}
class SPSongsCell: UITableViewCell {
    
    @IBOutlet weak var ad_View: GADBannerView!
    @IBOutlet weak var ActLoad: UIActivityIndicatorView!
    @IBOutlet weak var playingImage: UIImageView!
    @IBOutlet weak var SerialNum: UILabel!
    @IBOutlet weak var SongImage: UIImageView!
    @IBOutlet weak var SongName: UILabel!
    @IBOutlet weak var SongArtist: UILabel!
    @IBOutlet weak var iTunesButton: UIButton!
    @IBOutlet weak var PlayButton: UIButton!
    @IBOutlet weak var addSongsButton: UIButton!
    
}
