//
//  SPLogin.swift
//  Social Parrot
//
//  Created by Abdul Malik Aman on 10/06/16.
//  Copyright © 2016 INTELLIPATTERN. All rights reserved.
//

import UIKit

class SPLogin: UIViewController {

    // MARK:- Variables
    @IBOutlet weak var LoginTable: UITableView!
    var table_h: CGFloat!
    var is_keyboard_up = false
    var passURL: String!
    var passTitle: String!
    let facebookReadPermissions = ["public_profile", "email", "user_friends"]
    var faceBook_Clicked = false
    var userName: String = ""
    var nicName: String = ""
    var fstName: String = ""
    var lstName: String = ""
    var password: String = ""
    var becomeFirstResponder: Int!
    var is_Remember = false
  //var is_Agree = false
    var loginAttempt: Int!
    
    // MARK:- View Methods
    override func viewWillAppear(animated: Bool) {
        
        if EMAIL_ID != nil {
            if EMAIL_ID != "" {
                userName = EMAIL_ID!
            }else {
                userName = ""
            }
        }else {
            userName = ""
        }
        
        nicName = ""
        password = ""
        is_Remember = false
       // is_Agree = false
        self.navigationController?.navigationBarHidden = true
        LoginTable.reloadData()
    }
    override func viewDidLoad() {
        super.viewDidLoad()

        NSNotificationCenter.defaultCenter().addObserver(self, selector: "keyboardWillShow:", name: UIKeyboardWillShowNotification, object: nil)
        // Do any additional setup after loading the view.
    }
    
    // MARK:- Action
    @IBAction func FacebookLogin(sender: AnyObject) {
        self.view.endEditing(true)
        self.faceBook_Clicked = true
        loginToFacebookWithSuccess({ () -> () in
            print("FB login Success")
            self.faceBook_Clicked = false
            
            SP_DELEGATE.showAlert("Message", message: "Successfully Logged in with facebook", buttonTitle: "OK")
            }) { (NSError) -> () in
                print("Error")
                self.faceBook_Clicked = false
        }
    }
    @IBAction func SpecialInvitees(sender: AnyObject) {
        self.view.endEditing(true)
        performSegueWithIdentifier("SpecialInvitees", sender: self)
    }
    @IBAction func SignUp(sender: AnyObject) {
        self.view.endEditing(true)
        performSegueWithIdentifier("SignUp", sender: self)
    }
    @IBAction func ForgetPassword(sender: AnyObject) {
        self.view.endEditing(true)
        if userName != "" {
            if SP_DELEGATE.isValidEmail(userName) {
                let params = ["Email": userName]
                postForgetPassword(params)
            }
        }
    }
    @IBAction func Login(sender: AnyObject) {
        self.view.endEditing(true)
        ValidateLogin()
    }
    @IBAction func Reset(sender: AnyObject) {
        self.view.endEditing(true)
        userName = ""
        password = ""
        LoginTable.reloadData()
    }
    @IBAction func PrivacyPolicy(sender: AnyObject) {
        self.view.endEditing(true)
        if sender.tag == 0 {
            
            passURL = "http://www.apple.com/legal/internet-services/itunes/appstore/jm/terms.html"
            passTitle = "Terms of Use"
        }else {
            passURL = "http://www.apple.com/legal/privacy/en-ww/"
            passTitle = "Privacy Policy"
        }
        performSegueWithIdentifier("PrivacyPolicy", sender: self)
    }
    @IBAction func AgreementSign(sender: AnyObject) {
       // loginAttempt = nil
       // if is_Agree == false {
      //     is_Agree = true
     //   }else {
       //     is_Agree = false
       // }
     //   LoginTable.reloadData()
   }
    @IBAction func RememberMe(sender: AnyObject) {
        if is_Remember == false {
            is_Remember = true
        }else {
            is_Remember = false
        }
        LoginTable.reloadData()
    }
    
    func ValidateLogin() {
        if SP_DELEGATE.TrimString(userName) == "" {
            loginAttempt = 0
        }else if SP_DELEGATE.isValidEmail(userName) == false {
            loginAttempt = 0
        }else if SP_DELEGATE.TrimString(password) == "" {
            loginAttempt = 1
     //   }else if is_Agree == false {
     //       loginAttempt = 2
        }else {
            let params: String = "grant_type=password&username=\(userName)&password=\(password)"
            postLogin(params)
        }
        LoginTable.reloadData()
    }
    func keyboardWillShow(notification:NSNotification) {
        let userInfo:NSDictionary = notification.userInfo!
        let keyboardFrame:NSValue = userInfo.valueForKey(UIKeyboardFrameEndUserInfoKey) as! NSValue
        let keyboardRectangle = keyboardFrame.CGRectValue()
        let keyboardHeight = keyboardRectangle.height
        KEYBOARD_FRAME = keyboardHeight + 10
    }
    func SaveImage(image: UIImage) {
        if PROFILE_PICTURE != "" {
            SP_DELEGATE.removeFileFromPath("ProfilePicture")
            PROFILE_PICTURE = SP_DELEGATE.storeImage(image, imageName: "ProfilePicture")
            SP_DEFAULTS.setValue(PROFILE_PICTURE, forKey: "ProfilePic")
        }else {
            PROFILE_PICTURE = SP_DELEGATE.storeImage(image, imageName: "ProfilePicture")
            SP_DEFAULTS.setValue(PROFILE_PICTURE, forKey: "ProfilePic")
        }
    }
    
    // MARK:- TableView
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 4
    }
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var identifier: String!
        if indexPath.row == 0 || indexPath.row == 1 {
            identifier = "text"
        }else if indexPath.row == 2 {
            
            
            identifier = "button"
        }else {
            identifier = "radioButton"
        }
        let cell = LoginTable.dequeueReusableCellWithIdentifier(identifier, forIndexPath: indexPath) as! SPLoginCell
        if indexPath.row == 0 {
            cell.LoginText.tag = 0
            cell.LoginText.placeholder = "E-mail Address"
            cell.LoginText.Image_Name = "mail"
            
            cell.LoginText.text = userName
            cell.LoginText.returnKeyType = UIReturnKeyType.Next
            if loginAttempt != nil {
                if loginAttempt == 0 {
                    SP_DELEGATE.ShakeAnimation(cell.LoginText)
                }
            }
        }else if indexPath.row == 1 {
            cell.LoginText.tag = 1
            cell.LoginText.secureTextEntry = true
            cell.LoginText.placeholder = "Password"
            cell.LoginText.Image_Name = "password"
            cell.LoginText.text = password
            cell.LoginText.returnKeyType = UIReturnKeyType.Default
            if becomeFirstResponder != nil {
                if becomeFirstResponder == 1 {
                    cell.LoginText.becomeFirstResponder()
                }
            }
            if loginAttempt != nil {
                if loginAttempt == 1 {
                    SP_DELEGATE.ShakeAnimation(cell.LoginText)
                }
            }
        }else if indexPath.row == 2 {
            if is_Remember == false {
                cell.rememberImage.image = UIImage(named: "uncheck")
                
            }else {
                
                cell.rememberImage.image = UIImage(named: "checkin")
            }
            var SignUpAttributedString: NSMutableAttributedString!
            var ForgetAttributedString: NSMutableAttributedString!
            
            SignUpAttributedString = SP_DELEGATE.attributed(NSAttributedString(string: "SignUp Now"), isBold: false, underLine: 1, fontsize: 14, fontColor: UIColor.darkGrayColor())
            ForgetAttributedString = SP_DELEGATE.attributed(NSAttributedString(string: "Forgot Password"), isBold: false, underLine: 1, fontsize: 14, fontColor: UIColor.darkGrayColor())
            
            cell.SignUpButton.setAttributedTitle(SignUpAttributedString, forState: .Normal)
            cell.ForgetButton.setAttributedTitle(ForgetAttributedString, forState: .Normal)
        }else {
            if faceBook_Clicked == false {
                cell.facebookLogin.userInteractionEnabled = true
            }else {
                cell.facebookLogin.userInteractionEnabled = false
            }
           // cell.AgreementLabel.adjustsFontSizeToFitWidth = true
       //     var agreement: NSMutableAttributedString!
        /*    if is_Agree == false {
           //     cell.agreementImage.image = UIImage(named: "uncheck")
            }else {
               // cell.agreementImage.image = UIImage(named: "checkin")
            }
            if loginAttempt != nil {
                if loginAttempt == 2 {
             //       SP_DELEGATE.ShakeAnimation(cell.agreementImage)
                }
            }*/
            
         /*   let agree = SP_DELEGATE.attributed(NSAttributedString(string: "I agree "), isBold: false, underLine: 0, fontsize: 14, fontColor: UIColor.darkGrayColor())
            let terms = SP_DELEGATE.attributed(NSAttributedString(string: "Terms of Use"), isBold: true, underLine: 1, fontsize: 14, fontColor: UIColor.darkGrayColor())
            let and = SP_DELEGATE.attributed(NSAttributedString(string: " and "), isBold: false, underLine: 0, fontsize: 14, fontColor: UIColor.darkGrayColor())
            let Privacy = SP_DELEGATE.attributed(NSAttributedString(string: "Privacy Policy"), isBold: true, underLine: 1, fontsize: 14, fontColor: UIColor.darkGrayColor())
            
            agreement = agree
            agreement.appendAttributedString(terms)
            agreement.appendAttributedString(and)
            agreement.appendAttributedString(Privacy)
            
            cell.AgreementLabel.attributedText = agreement*/
        }
        return cell
    }
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
    }
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        if indexPath.row == 0 || indexPath.row == 1  {
            return 50
        }else if indexPath.row == 2 {
            return 100
        }else {
            return 180
        }
    }
    
    // MARK: - TextField Delegates
    func textFieldDidBeginEditing(textField: UITextField) {
        
    }
    func textFieldDidEndEditing(textField: UITextField) {
        if textField.tag == 0 {
            userName = textField.text!
        }else {
            password = textField.text!
        }
    }
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        if textField.tag == 0 {
            becomeFirstResponder = 1
        }else {
            becomeFirstResponder = nil
            textField.resignFirstResponder()
        }
        LoginTable.reloadData()
        return true
    }

    // MARK: - WebServices
    func postLogin(params: String) {
        SP_DELEGATE.showActivity(self.view, myTitle: "Authenticating..")
        postJson(WEB_SERVICE + "Token", method: "POST", params: params, is_Dictionary: true) { (Succeeded, Json) -> () in
            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                print(Succeeded)
                print(Json)
                SP_DELEGATE.removeActivity(self.view)
                if Succeeded == true {
                    
 //                   if let mail = Json["error_description"]{
   //                      SP_DELEGATE.showAlert("Message", message: "User name or Password incorrect", buttonTitle: "Ok")
     //               }
                    
                    
                    if let mail = Json["userName"]  {
                        if mail != nil {
                            SP_DEFAULTS.setValue(false, forKey: "is_SpecialInvitee")
                            IS_SPECIAL_INVITEE = false
                            SP_DEFAULTS.setValue(false, forKey: "Login")
                            
                            if EMAIL_ID != nil {
                                if EMAIL_ID! == mail as? String {
                                    APPDELEGATE?.SessionExpare(false)
                                }else {
                                    SP_DEFAULTS.setValue(mail, forKey: "mailId")
                                    EMAIL_ID = mail as? String
                                    APPDELEGATE?.SessionExpare(false)
                                }
                            }else {
                                SP_DEFAULTS.setValue(mail, forKey: "mailId")
                                EMAIL_ID = mail as? String
                                SP_DEFAULTS.setValue(mail, forKey: "mailId")
                                APPDELEGATE?.SessionExpare(false)
                            }
                            
                            
                        }else {
                            SP_DELEGATE.showAlert("Message", message: "User name or Password incorrect", buttonTitle: "Ok")
                        }
                    }else {
                        SP_DELEGATE.showAlert("Message", message: "User name or Password incorrect", buttonTitle: "Ok")
                    }
                    
                }
                
            })
        }
    }
    func postJson(url: String, method: String, params: String, is_Dictionary: Bool, postCompleted : (Succeeded: Bool, Json: AnyObject) -> ()) {
        if IS_REACHABLE == true {
            let request = NSMutableURLRequest(URL: NSURL(string: url)!)
            let session = NSURLSession.sharedSession()
            
            request.HTTPMethod = method
            request.HTTPBody = params.dataUsingEncoding(NSUTF8StringEncoding)
            
            request.addValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
            request.addValue("application/json", forHTTPHeaderField: "Accept")
            print(params, terminator: "")
            let task = session.dataTaskWithRequest(request) {(data, response, error) in
                dispatch_async(dispatch_get_main_queue(), {
                    if response != nil {
                        var jsonResult: AnyObject!
                        if is_Dictionary == true {
                            jsonResult = (try! NSJSONSerialization.JSONObjectWithData(data!, options: NSJSONReadingOptions.MutableContainers)) as! NSDictionary
                        }else {
                            jsonResult = (try! NSJSONSerialization.JSONObjectWithData(data!, options: NSJSONReadingOptions.MutableContainers)) as! NSArray
                        }
                        postCompleted(Succeeded: true, Json: jsonResult)
                    }else {
                        postCompleted(Succeeded: false, Json: "")
                    }
                })
                
            }
            task.resume()
        }else {
            postCompleted(Succeeded: false, Json: "")
            SP_DELEGATE.showAlert("Message", message: "No Internet Connection", buttonTitle: "OK")
        }
    }
    func loginToFacebookWithSuccess(successBlock: () -> (), andFailure failureBlock: (NSError?) -> ()) {
        if FBSDKAccessToken.currentAccessToken() != nil {
            FBSDKLoginManager().logOut()
        }
        FBSDKLoginManager().logInWithReadPermissions(self.facebookReadPermissions, fromViewController: nil, handler: {(result:FBSDKLoginManagerLoginResult!, error:NSError!) -> Void in
            
            if error != nil {
                FBSDKLoginManager().logOut()
                failureBlock(error)
            } else if result.isCancelled {
                FBSDKLoginManager().logOut()
                failureBlock(nil)
            } else {
                var allPermsGranted = true
                
                //result.grantedPermissions returns an array of _NSCFString pointers
                let grantedPermissions = result.grantedPermissions // .allObjects.map( {"\($0)"} )
                for permission in self.facebookReadPermissions {
                    if grantedPermissions.contains(permission) {
                        allPermsGranted = false
                        break
                    }
                }
                if allPermsGranted == false {
                    let fbToken = result.token.tokenString
                    //                    let fbUserID = result.token.userID
                    let req = FBSDKGraphRequest(graphPath: "me", parameters: ["fields":"id,email,name,picture.width(600).height(600)"], tokenString: fbToken, version: nil, HTTPMethod: "GET")
                    req.startWithCompletionHandler({ (connection, result, error : NSError!) -> Void in
                        if(error == nil)
                        {
                            print("Facebook Info: \(result)")
                            
                            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                                var email = result["email"] as! String
                                let user_id = result["id"] as! String
                                let name = result["name"] as! String
                                let fullName = name.componentsSeparatedByString(" ")
                                let fName = fullName[0]
                                let lName = fullName[1]
                                
                                SP_DEFAULTS.setValue(name, forKey: "User_Name")
//                                SP_DEFAULTS.setValue(email, forKey: "User_Email")
//                                SP_DEFAULTS.setValue("", forKey: "User_Phone")
                                if email == ""{
                                    email = "default@socialparrots.com"
                                }
                                
                                EMAIL_ID = email
                                
                                self.nicName = name
                            
                                if EMAIL_ID != nil {
                                    if EMAIL_ID != "" {
                                        self.userName = EMAIL_ID!
                                    }else {
                                        self.userName = ""
                                    }
                                }else {
                                    self.userName = ""
                                }
                                
                                
                                if self.nicName != "" {
                                        self.nicName = name
                                        self.fstName = fName
                                        self.lstName = lName
                                }else {
                                        self.nicName = ""
                                        self.fstName = ""
                                        self.lstName = ""
                                }
                                
                                
                                
                                let params = ["ProviderKey": user_id, "LoginProvider": "FB", "Email": email, "NickName": name, "SpecialInvitee": "N",  "Password":"Password@123", "ConfirmPassword":"Password@123"]
                                self.postFacebookSignUp(params)
                                
                            })
                            let imageArray = result["picture"] as? NSDictionary
                            let imageData = imageArray!["data"] as? NSDictionary
                            let urlString = imageData!["url"] as? String
                            
                            if let url = NSURL(string: urlString!) {
                                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0)) {
                                    if let data = NSData(contentsOfURL: url) {
                                        dispatch_async(dispatch_get_main_queue()) {
                                            SP_DEFAULTS.setValue(data, forKey: "ProfilePic")
                                            //                                            self.proImage?.image = UIImage(data: data)
//                                            self.performSegueWithIdentifier("Registration", sender: self)
                                        }
                                    }
                                }
                            }
                        }
                        else
                        {
                            print("error \(error)")
                        }
                    })
                    successBlock()
                } else {
                    print("Failed to login")
                    //                    failureBlock((nil)
                }
            }
        })
    }
    func postForgetPassword(params: NSDictionary) {
        SP_DELEGATE.showActivity(self.view, myTitle: "Loading..")
        SP_NETWORK.postJson(WEB_SERVICE + "api/Account/ForgotPassword", method: "POST", params: params as! Dictionary<String, AnyObject>, is_Dictionary: true) { (Succeeded, Json) -> () in
            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                print(Succeeded)
                print(Json)
                SP_DELEGATE.removeActivity(self.view)
                if Succeeded == true {
                    
                    if let status = Json["Status"] {
                        if status as? String == "1" {
                    SP_DELEGATE.showAlert("Message", message: "Password Reset Successfully Please check your mail", buttonTitle: "OK")
                            
                        }else{
                            
                             SP_DELEGATE.showAlert("Message", message: "UnSuccessfull, Try again", buttonTitle: "OK")
                        }
                        
                    }
                }
            })
        }
    }
    func postFacebookSignUp(params: NSDictionary) {
        SP_DELEGATE.showActivity(self.view, myTitle: "Loading..")
        SP_NETWORK.postJson(WEB_SERVICE + "api/Account/Register", method: "POST", params: params as! Dictionary<String, AnyObject>, is_Dictionary: true) { (Succeeded, Json) -> () in
            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                print(Succeeded)
                print(Json)
                SP_DELEGATE.removeActivity(self.view)
                if Succeeded == true {
                    if let status = Json["Status"] {
                        if status as? String == "1" {
                            
                            SP_DEFAULTS.setValue(EMAIL_ID, forKey: "mailId")
                            SP_DEFAULTS.setValue(Json["Id"], forKey: "User_Id")
                            USER_ID = Json["Id"] as? String
                            self.performSegueWithIdentifier("Registration", sender: self)
                            
                            
                        }else if status as? String == "2" {
                            SP_DEFAULTS.setValue(EMAIL_ID, forKey: "mailId")
                            SP_DEFAULTS.setValue(Json["Id"], forKey: "User_Id")
                            USER_ID = Json["Id"] as? String
                            APPDELEGATE?.SessionExpare(false)
                        }else {
                            let messages = Json["Message"] as! NSArray
                            SP_DELEGATE.showAlert("Message", message: messages[0] as! String, buttonTitle: "OK")
                        }
                        
                    }
                }
                self.LoginTable.reloadData()
            })
        }
    }
    
    // MARK: - System
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "Registration" {
            let registration = segue.destinationViewController as! SPRegistration
            registration.valueArray[0] = fstName
            registration.valueArray[1] = lstName
            registration.valueArray[8] = userName
            registration.valueArray[9] = nicName
            
        }
    }

}
class SPLoginCell: UITableViewCell {
    @IBOutlet weak var LoginText: SPImagedTextFiled!
    @IBOutlet weak var SignUpButton: UIButton!
    @IBOutlet weak var ForgetButton: UIButton!
   // @IBOutlet weak var AgreementLabel: UILabel!
    @IBOutlet weak var facebookLogin: SPRoundButton!
  //  @IBOutlet weak var agreementImage: UIImageView!
    
    @IBOutlet weak var rememberImage: UIImageView!
    
}