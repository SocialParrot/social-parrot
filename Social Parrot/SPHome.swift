
//
//  SPHome.swift
//  Social Parrot
//
//  Created by Abdul Malik Aman on 11/06/16.
//  Copyright © 2016 INTELLIPATTERN. All rights reserved.
//

import UIKit
import GoogleMobileAds

@objc protocol SocialParrot_Delegate {
    optional func hideBackButton(isHide: Bool)
    optional func hideLogoutButton(isHide: Bool)
    optional func navigateTab(tab: Int)
    optional func navigateBack(controller: UINavigationController, tag: Int)
    optional func showActivity(title: String)
    optional func removeActivity()
}
class SPHome: UIViewController, SocialParrot_Delegate {

    // MARK: - Variables
    var rootContanerView = SPRootTab()
    @IBOutlet var TabButtons: [UIImageView]!
    @IBOutlet weak var SliderContainer: UIView!
    @IBOutlet weak var SlideMenu: UIView!
    @IBOutlet weak var MenuTable: UITableView!
    @IBOutlet weak var MenuButton: UIButton!
    @IBOutlet weak var customNavigation: UIView!
    @IBOutlet weak var logoutNavigation: UIView!
    var searchNavigation = UINavigationController()
    var playlistNavigation = UINavigationController()
    var favouriteNavigation = UINavigationController()
    @IBOutlet weak var bannerView: DFPBannerView!
    var is_menuShown = false
    var selectedIndex: Int!
    var is_PlayListClicked = true
    
    var menuArray = ["", "Profile","Blocked Profiles", "Playlist", "Terms & Conditions", "Privacy Policy", "Logout"]
    var imageArrayArray = ["", "slide_profile","slide_blocked", "slide_playlist", "slide_terms", "slide_privacy", "slide_logout"]
    
    var socialNavigation = UINavigationController()
    
    // MARK: - View Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        self.MenuTable.tableFooterView=UIView()
        SliderContainer.hidden = true
        
        TabButtons[0].highlighted = true
        
        print("Google Mobile Ads SDK version: \(DFPRequest.sdkVersion())")
        
        bannerView.adUnitID = "ca-app-pub-8796755118307878/4871416143"
        bannerView.rootViewController = self
        bannerView.loadRequest(DFPRequest())
        bannerView.hidden = true
        
//        let tap = UITapGestureRecognizer(target: self, action: Selector("showHideMenu:"))
//        SliderContainer.addGestureRecognizer(tap)
        // Do any additional setup after loading the view.
    }

    // MARK: - Actions
    
    @IBAction func Back(sender: AnyObject) {
        socialNavigation.popViewControllerAnimated(true)
    }
    @IBAction func SelectTabs(sender: AnyObject) {
        if sender.tag == 1 {
            searchNavigation.popToRootViewControllerAnimated(false)
        }else if sender.tag == 2 {
            playlistNavigation.popToRootViewControllerAnimated(false)
        }else if sender.tag == 3 {
            favouriteNavigation.popToRootViewControllerAnimated(false)
        }
        highlightTabs(sender.tag)
        print(sender.tag)
    }
    func highlightTabs(tag: Int) {
        self.rootContanerView.selectedIndex = tag
        print(tag)
        for i in 0...3 {
            if tag == i {
                TabButtons[i].highlighted = true
            }else {
                TabButtons[i].highlighted = false
            }
            if tag == 0 {
                APPDELEGATE?.SessionExpare(false)
            }
           
        }
    }
    @IBAction func Logout(sender: AnyObject) {
        Logout()
    }
    func Logout() {
        
        
        let logoutAlert = UIAlertController(title: "Log Out", message: "Are You Sure to Log Out ? ", preferredStyle: UIAlertControllerStyle.Alert)
        
        logoutAlert.addAction(UIAlertAction(title: "Confirm", style: .Default, handler:
            
            { (action: UIAlertAction!) in
                self.navigationController?.popToRootViewControllerAnimated(true)
                SP_DEFAULTS.setValue(nil, forKey: "User_Id")
                USER_ID = nil
                //        SP_DEFAULTS.setValue(nil, forKey: "mailId")
                //        EMAIL_ID = nil
                SP_DEFAULTS.setValue(true, forKey: "Login")
                IS_SESSION_EXPIRED = true
                APPDELEGATE?.SessionExpare(true)
            }
            ))
        
        logoutAlert.addAction(UIAlertAction(title: "Cancel", style: .Default, handler:
            { (action: UIAlertAction!) in
                
                logoutAlert .dismissViewControllerAnimated(true, completion: nil)
                
                
            }
            ))
        
        presentViewController(logoutAlert, animated: true, completion: nil)
        
    }
    @IBAction func showMenu(sender: AnyObject) {
        showMenuTab()
    }
    func showMenuTab() {
        MenuTable.reloadData()
        if self.SliderContainer.hidden == true {
            is_menuShown = true
            UIView.animateWithDuration(0.5, delay: 0.0, options: UIViewAnimationOptions.TransitionNone, animations: { () -> Void in
                self.SliderContainer.hidden = false
                self.SlideMenu.frame.origin.x = 0
                }) { (finished: Bool) -> Void in
                    
            }
        }else {
            is_menuShown = false
            UIView.animateWithDuration(0.5, delay: 0.0, options: UIViewAnimationOptions.TransitionNone, animations: { () -> Void in
                self.SlideMenu.frame.origin.x = -250
                }) { (finished: Bool) -> Void in
                    self.SliderContainer.hidden = true
            }
        }
        
    }
    func showHideMenu(sender: UITapGestureRecognizer? = nil) {
        showMenuTab()
    }
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        if is_menuShown == true {
            showMenuTab()
        }
    }
    
    // MARK:- TableView
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if is_PlayListClicked == true {
            if section == 1 || section == 3 {
                return 2
            }else {
                return 1
            }
        }else {
            return 1
        }
    }
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return menuArray.count
    }
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var identifier: String!
        if indexPath.section == 0 {
            identifier = "profile"
        }else {
            if is_PlayListClicked == true {
                if indexPath.section == 1 || indexPath.section == 3  {
                    if indexPath.row == 0 {
                        identifier = "menuCell"
                    }else {
                        identifier = "subCell"
                    }
                }else {
                    identifier = "menuCell"
                }
            }else {
                identifier = "menuCell"
            }
        }
        let cell = MenuTable.dequeueReusableCellWithIdentifier(identifier, forIndexPath: indexPath) as! SPMenuCell
        
        if indexPath.section == 0 {
            cell.ProfilePic.layer.cornerRadius = 37.5
            cell.ProfilePic.layer.masksToBounds = true
            cell.UserName.text = USER_NAME
            //cell.UserName.text = "USER NAME"
          
            if PROFILE_PICTURE != nil {
                cell.ProfilePic.image = SP_DELEGATE.loadImageFromPath(PROFILE_PICTURE!)
            }
            
        }else {
            if indexPath.section != 1 && indexPath.section != 3 {
                cell.PlaceHolderLabel.text = menuArray[indexPath.section]
                cell.PlaceHolderImage.image = UIImage(named: imageArrayArray[indexPath.section])
            }else {
                if indexPath.row == 0 {
                    cell.PlaceHolderLabel.text = menuArray[indexPath.section]
                    cell.PlaceHolderImage.image = UIImage(named: imageArrayArray[indexPath.section])
                }else {
                    if indexPath.section == 1 {
                        cell.subCellLabel.text = "Edit Profile"
                    }else {
                        cell.subCellLabel.text = "Create Playlist"
                    }
                }
            }
        }
        
        if selectedIndex != nil {
            if indexPath.section != 0 {
                if selectedIndex == indexPath.row {
                    cell.backgroundColor = UIColor.darkGrayColor()
                }else {
                    cell.backgroundColor = UIColor.blackColor()
                }
            }
        }
        return cell
    }
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        selectedIndex = indexPath.section
        switch indexPath.section {
            case 1:
                    if indexPath.row == 0 {
                        navigateTab(0)
                    }else {
                        let register = storyboard?.instantiateViewControllerWithIdentifier("SPRegistration") as! SPRegistration
                        register.userId = USER_ID!
                        showViewController(register, sender: self)
                    }
            case 2: navigateTab(5)
            case 3:
                    if indexPath.row == 0 {
                        navigateTab(2)
                        showMenuTab()
                    }else {
                        navigateTab(4)
                        showMenuTab()
                    }
            case 4: navigateTab(7)
            case 5: navigateTab(0)
            case 6: Logout()
            default:
                break
        }
        if indexPath.section != 0 && indexPath.section != 3 {
            showMenuTab()
        }
        
        MenuTable.reloadData()
    }
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        if indexPath.section == 0 {
            return 160
        }else {
            return 50
        }
    }
    
    // MARK: - Custom delegates
    func navigateTab(tab: Int) {
        if tab == 0 {
            APPDELEGATE?.SessionExpare(false)
        }
        
        if tab < 4 {
            self.rootContanerView.selectedIndex = tab
            highlightTabs(tab)
            
        }else {
            self.rootContanerView.selectedIndex = tab
            if is_menuShown == true {
                showMenuTab()
            }
        }
    }
    func hideBackButton(isHide: Bool) {
        customNavigation.hidden = isHide
    }
    func hideLogoutButton(isHide: Bool) {
        logoutNavigation.hidden = isHide
    }
    func navigateBack(controller: UINavigationController, tag: Int) {
        socialNavigation = controller
        controller.popToRootViewControllerAnimated(true)
        navigateTab(1)
//        socialNavigation.popViewControllerAnimated(true)
    }
    func showActivity(title: String) {
        SP_DELEGATE.showActivity(self.view, myTitle: title)
    }
    func removeActivity() {
        SP_DELEGATE.removeActivity(self.view)
    }
    // MARK: - System
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "rootTab" {
            rootContanerView = segue.destinationViewController as! SPRootTab
            let profileNavigation = rootContanerView.viewControllers![0] as! UINavigationController
            let profile = profileNavigation.viewControllers[0] as! SPProfile
            profile.socialDelegate = self
            
            searchNavigation = rootContanerView.viewControllers![1] as! UINavigationController
            let searchSearch = searchNavigation.viewControllers[0] as! SPSearch
            searchSearch.socialDelegate = self
            
            playlistNavigation = rootContanerView.viewControllers![2] as! UINavigationController
            let PlayList = playlistNavigation.viewControllers[0] as! SPPlayList
            PlayList.socialDelegate = self
            
            favouriteNavigation = rootContanerView.viewControllers![3] as! UINavigationController
            let favourites = favouriteNavigation.viewControllers[0] as! SPFavourites
            favourites.socialDelegate = self
            
            let createPlayListNavigation = rootContanerView.viewControllers![4] as! UINavigationController
            let createPlayList = createPlayListNavigation.viewControllers[0] as! SPCreatePlayList
            createPlayList.socialDelegate = self
            createPlayList.editPlayList = nil

            let Blocked = rootContanerView.viewControllers![5] as! SPBlockedProfiles
            Blocked.socialDelegate = self
        }
    }
}

class SPMenuCell: UITableViewCell {
    
    @IBOutlet weak var ProfilePic: UIImageView!
    @IBOutlet weak var UserName: UILabel!
    @IBOutlet weak var PlaceHolderLabel: UILabel!
    @IBOutlet weak var PlaceHolderImage: UIImageView!
    @IBOutlet weak var subCellLabel: UILabel!
    
}
