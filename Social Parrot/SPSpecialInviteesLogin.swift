//
//  SPSpecialInviteesLogin.swift
//  Social Parrot
//
//  Created by Abdul Malik Aman on 11/06/16.
//  Copyright © 2016 INTELLIPATTERN. All rights reserved.
//

import UIKit

class SPSpecialInviteesLogin: UIViewController {

    // MARK: - Variables
    @IBOutlet weak var SpecialInviteesTable: UITableView!
    @IBOutlet weak var backButton: SPRoundButton!
    var becomeResponder: Int!
    var email: String = ""
    var verificationCode: String = ""
    var is_keyboard_up = false
    var table_h: CGFloat!
    var is_Failed: Int!
    var errorMessage: String!
    
    // MARK: - View Methods
    override func viewDidLayoutSubviews() {
        if is_keyboard_up == true {
            SpecialInviteesTable.frame.size.height = self.view.frame.height - (KEYBOARD_FRAME + 175)
        }else {
            SpecialInviteesTable.frame.size.height = table_h
        }
    }
    override func viewWillAppear(animated: Bool) {
        self.navigationController?.navigationBarHidden = true
        if verificationCode != "" {
            backButton.hidden = true
        }
        SpecialInviteesTable.reloadData()
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        table_h = SpecialInviteesTable.frame.size.height
        // Do any additional setup after loading the view.
    }
    
    // MARK: - Actions
    @IBAction func goToRegistration(sender: AnyObject) {
        self.view.endEditing(true)
        Validate()
    }
    func Validate() {
        is_Failed = nil
        if SP_DELEGATE.TrimString(email) == "" {
            is_Failed = 0
        }else if SP_DELEGATE.isValidEmail(email) == false {
            is_Failed = 0
        }else if SP_DELEGATE.TrimString(verificationCode) == "" {
            is_Failed = 1
        }else {
            let params: NSDictionary = ["Code":verificationCode, "Email": email]
            postSpecialInvitee(params)
        }
        SpecialInviteesTable.reloadData()
    }
    
    @IBAction func Back(sender: AnyObject) {
        self.navigationController?.popViewControllerAnimated(true)
    }
    func scrollToTop(row: Int, section: Int, table: UITableView) {
        let indexPath = NSIndexPath(forRow: row, inSection: section)
        table.scrollToRowAtIndexPath(indexPath, atScrollPosition: .Top, animated: true)
    }
    
    // MARK: - TableView Delegates
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var identifier: String!
        if indexPath.row == 2 {
            identifier = "button"
        }else {
            identifier = "text"
        }
        let cell = SpecialInviteesTable.dequeueReusableCellWithIdentifier(identifier, forIndexPath: indexPath) as! SPSpecialInviteesCell
        
        if indexPath.row != 2 {
            if indexPath.row == 0 {
                cell.SpecialInviteesText.tag = 0
                cell.SpecialInviteesText.Image_Name = "mail"
                cell.SpecialInviteesText.placeholder = "E-mail Address"
                cell.SpecialInviteesText.text = email
            }else {
                cell.SpecialInviteesText.tag = 1
                cell.SpecialInviteesText.Image_Name = "password"
                cell.SpecialInviteesText.placeholder = "Verification Code"
                cell.SpecialInviteesText.secureTextEntry = true
                cell.SpecialInviteesText.keyboardType = UIKeyboardType.NumbersAndPunctuation
                cell.SpecialInviteesText.text = verificationCode
            }
            if is_Failed != nil {
                if is_Failed == indexPath.row {
                    SP_DELEGATE.ShakeAnimation(cell)
                }
            }
        }
        return cell
    }
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
    }
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        if indexPath.row == 2 {
            return 45
        }else {
            return 50
        }
    }
    
    // MARK: - TextField Delegates
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        if textField.tag == 0 {
            becomeResponder = 1
        }else {
            becomeResponder = nil
            textField.resignFirstResponder()
        }
        SpecialInviteesTable.reloadData()
        return true
    }
    func textFieldDidBeginEditing(textField: UITextField) {
        is_keyboard_up = true
        scrollToTop(textField.tag, section: 0, table: SpecialInviteesTable)
        viewDidLayoutSubviews()
    }
    func textFieldDidEndEditing(textField: UITextField) {
        is_keyboard_up = false
        if textField.tag == 0 {
            email = textField.text!
            print(email)
        }else{
            verificationCode = textField.text!
        }
        
        viewDidLayoutSubviews()
    }
    
    // MARK: - WebServices
    func postSpecialInvitee(params: NSDictionary) {
        SP_DELEGATE.showActivity(self.view, myTitle: "Loading..")
        SP_NETWORK.postJson(WEB_SERVICE + "api/Account/SpecialInviteeVerify", method: "POST", params: params as! Dictionary<String, AnyObject>, is_Dictionary: true) { (Succeeded, Json) -> () in
            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                print(Succeeded)
                print(Json)
                SP_DELEGATE.removeActivity(self.view)
                if Succeeded == true {
                    if let status = Json["Status"] {
                        if status as? String == "1" {
                            self.verificationCode = ""
                            SP_DEFAULTS.setValue(true, forKey: "is_SpecialInvitee")
                            IS_SPECIAL_INVITEE = true
                            let signUp = self.storyboard?.instantiateViewControllerWithIdentifier("SPSignUp") as! SPSignUp
                            signUp.is_SpecialInvitee = true
                            signUp.ValueArray[0] = self.email
                            SP_DEFAULTS.setValue(self.email, forKey: "User_Email")
                            self.showViewController(signUp, sender: self)
                        }
                        else {
                            let messages = Json["Message"] as! NSArray
                            SP_DELEGATE.showAlert("Message", message: messages[0] as! String, buttonTitle: "OK")}
                }}
                self.SpecialInviteesTable.reloadData()
            })
        }
    }

    // MARK: - System
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    // MARK: - Navigation
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "Registration" {
            let registration = segue.destinationViewController as! SPRegistration
            registration.is_SpecialInvitee = true
            registration.valueArray[8] = email
        }
    }

}
class SPSpecialInviteesCell: UITableViewCell {
    @IBOutlet weak var SpecialInviteesText: SPImagedTextFiled!
    
}