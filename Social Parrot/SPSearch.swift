//
//  SPSearch.swift
//  Social Parrot
//
//  Created by Abdul Malik Aman on 18/06/16.
//  Copyright © 2016 INTELLIPATTERN. All rights reserved.
//

import UIKit
import GoogleMobileAds

class SPSearch: UIViewController, SocialParrot_Delegate, GADBannerViewDelegate {

    // MARK: - Variables
    var socialDelegate: SocialParrot_Delegate?
    @IBOutlet weak var SearchTable: UITableView!
    @IBOutlet weak var SearchText: SPImagedTextFiled!
    var SearchResults: NSArray!
    var ImageResults: NSDictionary!
    var followIndex: Int!
    var is_AdShow = false
    var adTimer = NSTimer()
    
    // MARK: - View Methods
    override func viewWillAppear(animated: Bool) {
        self.navigationController?.navigationBarHidden = true
        socialDelegate?.navigateTab!(1)
        socialDelegate?.hideBackButton!(false)
        socialDelegate?.navigateBack!(self.navigationController!, tag: 1)
        SearchText.text = ""
        let params = ["UserId": USER_ID!, "Favorite": "Y", "Search": ""]
        postSearchAllUser(params)
        adTimer = NSTimer.scheduledTimerWithTimeInterval(5, target: self, selector: "showAd", userInfo: nil, repeats: false)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        // Do any additional setup after loading the view. 
    }

    // MARK: - Actions
    @IBAction func CloseAd(sender: AnyObject) {
        adTimer.invalidate()
        is_AdShow = false
        SearchTable.reloadData()
    }
    
    func showAd() {
        if is_AdShow == false {
            is_AdShow = true
        }else {
            is_AdShow = false
        }
        SearchTable.reloadData()
    }
    @IBAction func Search(sender: AnyObject) {
        ImageResults = nil
        
        let params = ["UserId": USER_ID!, "Favorite": "Y", "Search": SP_DELEGATE.TrimString(SearchText.text!)]
        postSearchAllUser(params)
        
        SearchText.resignFirstResponder()
        
    }
    @IBAction func Follow(sender: AnyObject) {
        print(sender.tag)
        followIndex = sender.tag!
        var params: NSDictionary!
        if let followed = self.SearchResults[self.followIndex]["Following"] {
            if followed as? String == "1" {
                params = ["FollowerId": USER_ID!, "FollowingId": SearchResults[followIndex]["UserId"], "Follow": "N"]
            }else {
                params = ["FollowerId": USER_ID!, "FollowingId": SearchResults[followIndex]["UserId"], "Follow": "Y"]
            }
        }else {
            params = ["FollowerId": USER_ID!, "FollowingId": SearchResults[followIndex]["UserId"], "Follow": "Y"]
        }
        
        postFollowUser(params)
    }
    
    // MARK: - TableView Delegate Methods
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if SearchResults != nil {
            if is_AdShow == false {
                return SearchResults.count
            }else {
                if section == 0 {
                    return SearchResults.count
                }else {
                    return 1
                }
            }
        }else {
            return 0
        }
    }
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        if is_AdShow == false {
            return 1
        }else {
            return 2
        }
    }
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var identifier: String!
        
        if is_AdShow == false {
            identifier = "searchUser"
        }else {
            if indexPath.section == 0 {
                identifier = "searchUser"
            }else {
                identifier = "Ad_Cell"
            }
        }
        let cell = SearchTable.dequeueReusableCellWithIdentifier(identifier, forIndexPath: indexPath) as! SPFollowSearch
        
        if indexPath.section == 0 {
            if self.SearchResults[indexPath.row]["IsBlocked"] as? String != "Y"{
                cell.SearchImage.layer.cornerRadius = 32.5
                cell.SearchImage.layer.masksToBounds = true
                cell.FollowButton.tag = indexPath.row
                let firstName = SearchResults[indexPath.row]["FirstName"] as? String
                let lastname = SearchResults[indexPath.row]["LastName"] as? String
                cell.SearchName.text = firstName! + " " + lastname!
                cell.SearchDescription.text = SearchResults[indexPath.row]["Description"] as? String
                
//                let ImageParams = ["UserId":SearchResults[indexPath.row]["UserId"]]
                
                if let followed = self.SearchResults[indexPath.row]["Following"] {
                    if followed as? String == "1" {
                        cell.FollowButton.setBackgroundImage(UIImage(named: "unfollowButton"), forState: UIControlState.Normal)
                        cell.FollowButton.setTitle("UnFollow", forState: UIControlState.Normal)
                    }else {
                        cell.FollowButton.setBackgroundImage(UIImage(named: "followButton"), forState: UIControlState.Normal)
                        cell.FollowButton.setTitle("Follow", forState: UIControlState.Normal)
                    }
                }
                
                if let url = NSURL(string: SearchResults[indexPath.row]["ImageURL"] as! String) {
                    cell.SearchImage.sd_setImageWithURL(url, placeholderImage: UIImage(named: "Profile_bg"))
                }
            }
        }else {
            cell.ad_View.adUnitID = "ca-app-pub-8796755118307878/4871416143"
            cell.ad_View.rootViewController = self
            cell.ad_View.loadRequest(GADRequest())
            cell.ad_View.delegate = self
        }
        return cell
    }
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        if indexPath.section == 0 {
            return 105
        }else {
            return 55
        }
    }
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        if indexPath.section == 0 {
            let profile = storyboard?.instantiateViewControllerWithIdentifier("SPProfile") as? SPProfile
            profile!.profileUserId = SearchResults[indexPath.row]["UserId"] as? String
            profile?.socialDelegate = self
            showViewController(profile!, sender: self)
        }
        
    }
    
    // MARK: - Custom Delegates
    func hideLogoutButton(isHide: Bool) {
        socialDelegate?.hideLogoutButton!(isHide)
    }
    func navigateTab(tab: Int) {
        socialDelegate?.navigateTab!(tab)
    }
    func hideBackButton(isHide: Bool) {
        socialDelegate?.hideBackButton!(isHide)
    }
    func showActivity(title: String) {
        socialDelegate?.showActivity!(title)
    }
    func removeActivity() {
        socialDelegate?.removeActivity!()
    }
    
    // MARK: - Google Ad Delegates
    func adViewDidReceiveAd(bannerView: GADBannerView!) {
        print("adViewDidReceiveAd")
    }
    
    func adView(bannerView: GADBannerView!, didFailToReceiveAdWithError error: GADRequestError!) {
        print("adView:didFailToReceiveAdWithError: \(error.localizedDescription)")
    }
    
    func adViewWillPresentScreen(bannerView: GADBannerView!) {
        print("adViewWillPresentScreen")
    }
    
    func adViewWillDismissScreen(bannerView: GADBannerView!) {
        print("adViewWillDismissScreen")
    }
    
    func adViewDidDismissScreen(bannerView: GADBannerView!) {
        print("adViewDidDismissScreen")
    }

    
    // MARK: - TextField Delegates
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        let params = ["UserId": USER_ID!, "Favorite": "Y", "Search": SP_DELEGATE.TrimString(textField.text!)]
        postSearchAllUser(params)
        return true
    }

    // MARK: - WebServices
    func postFollowUser(params: NSDictionary) {
        socialDelegate?.showActivity!("Loading..")
        SP_NETWORK.postJson(WEB_SERVICE + "api/Account/UserFollowUpUpdate", method: "POST", params: params as! Dictionary<String, AnyObject>, is_Dictionary: true) { (Succeeded, Json) -> () in
            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                print(Succeeded)
                print(Json)
                self.socialDelegate?.removeActivity!()
                if Succeeded == true {
                    if let status = Json["Status"] {
                        if status as? String == "1" {
                            if let followed = self.SearchResults[self.followIndex]["Following"] {
                                if followed as? String != "1" {
                                    self.SearchResults[self.followIndex].setValue("1", forKey: "Following")
                                }else {
                                    self.SearchResults[self.followIndex].setValue("0", forKey: "Following")
                                }
                            }
                            print(self.SearchResults)
                        }
                    }
                }
                self.SearchTable.reloadData()
            })
        }
    }
    func postSearchAllUser(params: NSDictionary) {
        socialDelegate?.showActivity!("Loading..")
        SP_NETWORK.postJson(WEB_SERVICE + "api/Account/SearchAllUsers", method: "POST", params: params as! Dictionary<String, AnyObject>, is_Dictionary: true) { (Succeeded, Json) -> () in
            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                print(Succeeded)
                print(Json)
                self.socialDelegate?.removeActivity!()
                if Succeeded == true {
                    self.SearchResults = Json["ResultSet"] as? NSArray
                }
                self.SearchTable.reloadData()
            })
        }
    }
    
    
    // MARK: - System
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
   

}
class SPFollowSearch: UITableViewCell {
    
    @IBOutlet weak var ad_View: GADBannerView!
    @IBOutlet weak var FollowButton: UIButton!
    @IBOutlet weak var SearchDescription: UILabel!
    @IBOutlet weak var SearchName: UILabel!
    @IBOutlet weak var SearchImage: UIImageView!
}