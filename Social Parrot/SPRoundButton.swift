//
//  SPRoundButton.swift
//  Social Parrot
//
//  Created by Abdul Malik Aman on 10/06/16.
//  Copyright © 2016 INTELLIPATTERN. All rights reserved.
//

import UIKit

class SPRoundButton: UIButton {
    
    let gradientLayer = CAGradientLayer()
    @IBInspectable var Boarder_Colour: UIColor = UIColor.clearColor()
    @IBInspectable var Boarder_Width: CGFloat = 1.0
    @IBInspectable var Corner_Radius: CGFloat = 1.0
    @IBInspectable var Top_Gradient_Colour: UIColor = UIColor.clearColor()
    @IBInspectable var Bottom_Gradient_Colour: UIColor = UIColor.clearColor()
    @IBInspectable var Title: String = ""
    
    override func drawRect(rect: CGRect) {
        
        self.layer.borderColor = Boarder_Colour.CGColor
        self.layer.borderWidth = Boarder_Width
        self.layer.cornerRadius = Corner_Radius
        self.layer.masksToBounds = true
        
        gradientLayer.frame = self.bounds
        let Color1 = Top_Gradient_Colour.CGColor as CGColorRef
        let Color2 = Bottom_Gradient_Colour.CGColor as CGColorRef
        gradientLayer.colors = [Color1, Color2]
        gradientLayer.locations = [0.0, 0.6]
        self.layer.addSublayer(gradientLayer)
        gradientLayer.colors = [Color1, Color2]
        gradientLayer.locations = [0.0, 0.6]
        self.layer.insertSublayer(gradientLayer, atIndex: 0)
        self.setTitle(Title, forState: UIControlState.Normal)
    }
    

}
