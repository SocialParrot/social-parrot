//
//  SPCreatePlayList.swift
//  Social Parrot
//
//  Created by Abdul Malik Aman on 25/06/16.
//  Copyright © 2016 INTELLIPATTERN. All rights reserved.
//

import UIKit

class SPCreatePlayList: UIViewController, UIPopoverPresentationControllerDelegate, SwiftColorPickerDelegate, UINavigationControllerDelegate, UIImagePickerControllerDelegate, UIActionSheetDelegate, imageEditiDelegate, SocialParrot_Delegate {
    
    // MARK: - Variables
    var socialDelegate: SocialParrot_Delegate?
    @IBOutlet weak var opacityLabel: UILabel!
    @IBOutlet weak var opacitySlider: UISlider!
    @IBOutlet weak var fontColorPreview: UIView!
    @IBOutlet weak var playListName: UITextField!
    @IBOutlet weak var playListImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    
    var imagePicker = UIImagePickerController()
    var opacity: Int = 20
    var buttonTitles:[ImagePickerActionSheetButtons:String]!
    var UploadImage: String = ""
    var PlayListImage: UIImage!
    var editPlayList: NSDictionary!
    var playListId = ""
    var playListColour: Int!
    enum ImagePickerActionSheetButtons {
        case Camera
        case Chooser
    }
    
    // MARK: - View Methods
    override func viewWillDisappear(animated: Bool) {
        socialDelegate?.hideLogoutButton!(false)
    }
    override func viewWillAppear(animated: Bool) {
        self.navigationController?.navigationBarHidden = true
        socialDelegate?.hideBackButton!(true)
        socialDelegate?.hideLogoutButton!(true)
        imagePicker.delegate = self
        playListImageView.alpha = CGFloat(opacity) / 100
        opacitySlider.value = 10
        opacityLabel.text = "\(opacity)%"
        //        opacityLabel.text = "20%"
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if editPlayList != nil {
            titleLabel.text = "Edit PlayList"
            if let editOpacity = editPlayList["Opacity"] {
                opacity = Int((editOpacity as? String)!)!
                playListImageView.alpha = CGFloat(opacity) / 100
                opacitySlider.value = Float(opacity)
                opacityLabel.text = "\(opacity)%"
            }
            if let iD = editPlayList["ID"] {
                playListId = iD as! String
                //   playListId = "\(iD as? Int)"
                print("iddd:",playListId)
            }
            if let name = editPlayList["Name"] {
                playListName.text = name as? String
            }
            if let color = editPlayList["Colors"] {
                if color as? String != "" {
                    self.fontColorPreview.backgroundColor = UIColorFromRGB(UInt(color as! String)!)
                }
                
            }
            if editPlayList != nil {
                if let url = NSURL(string: editPlayList["BgImageURL"] as! String) {
                    playListImageView.sd_setImageWithURL(url, placeholderImage: UIImage(named: "playlist"))
                }
            }
            
        }else {
            titleLabel.text = "Create PlayList"
            if let rgb = UIColor.blackColor().colourToHex() {
                print(rgb)
                playListColour = rgb
            }
        }
        // Do any additional setup after loading the view.
    }
    
    // MARK: - Actions
    @IBAction func selectPlayListImage(sender: AnyObject) {
        let croppingEnabled = true
        let cameraViewController = CameraViewController(croppingEnabled: croppingEnabled) { [weak self] image, asset in
            
            if image != nil {
                self!.PlayListImage = image
                self!.UploadImage = SP_DELEGATE.encodeImage(image!)
                self!.playListImageView.image = self!.PlayListImage
            }
            self?.dismissViewControllerAnimated(true, completion: nil)
        }
        
        presentViewController(cameraViewController, animated: true, completion: nil)
        
//        let alert = UIAlertController(title: "Choose Image", message: "Choose an image for Playlist", preferredStyle: UIAlertControllerStyle.ActionSheet)
//        alert.addAction(UIAlertAction(title: "Camera", style: UIAlertActionStyle.Default, handler: { action in
//            self.CameraAction()
//        }))
//        alert.addAction(UIAlertAction(title: "Photo Library", style: UIAlertActionStyle.Default, handler: { action in
//            self.PhotosAction()
//        }))
//        alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.Cancel, handler: nil))
//        self.presentViewController(alert, animated: true, completion: nil)
    }
    func CameraAction() {
        imagePicker.sourceType = UIImagePickerControllerSourceType.Camera
        self.presentViewController(imagePicker, animated: true, completion: nil)
        imagePicker.allowsEditing = false
    }
    func PhotosAction() {
        imagePicker.sourceType = UIImagePickerControllerSourceType.PhotoLibrary
        self.presentViewController(imagePicker, animated: true, completion: nil)
        imagePicker.allowsEditing = false
    }
    func actionSheet(sheet: UIActionSheet, clickedButtonAtIndex buttonIndex: Int){
        
        if (sheet.buttonTitleAtIndex(buttonIndex) == buttonTitles[.Chooser]!) {
            PhotosAction()
        }else if (sheet.buttonTitleAtIndex(buttonIndex) == buttonTitles[.Camera]!) {
            CameraAction()
        }else if (buttonIndex == sheet.cancelButtonIndex) {
            
        }
    }
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]) {
        let image = info[UIImagePickerControllerOriginalImage] as! UIImage
        //        self.playListImageView.image = image
        PlayListImage = image
        self.dismissViewControllerAnimated(true, completion: { () -> Void in
            self.performSegueWithIdentifier("imageEditor", sender: self)
        })
    }
    func UIColorFromRGB(rgbValue: UInt) -> UIColor {
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
    @IBAction func Save(sender: AnyObject) {
        self.view.endEditing(true)
        Validate()
    }
    func Validate() {
        if editPlayList != nil {
            UploadImage = SP_DELEGATE.encodeImage(playListImageView.image!)
        }
        if playListColour == nil {
            if let rgb = UIColor.blackColor().colourToHex() {
                print(rgb)
                playListColour = rgb
            }
        }
        if SP_DELEGATE.TrimString(playListName.text!) == "" {
            SP_DELEGATE.showAlert("Message", message: "Please enter Playlist name", buttonTitle: "OK")
        }else if UploadImage == "" {
            SP_DELEGATE.showAlert("Message", message: "Please select a Playlist image", buttonTitle: "OK")
        }else {
            let params = ["ID" : playListId, "UserId" : USER_ID!, "Name" : SP_DELEGATE.TrimString(playListName.text!), "BgImage" : UploadImage, "Opacity": "\(opacity)", "Colors": "\(playListColour)"]
            print(params)
            postCreatePlayList(params)
        }
    }
    
    @IBAction func Back(sender: AnyObject) {
        dismissVIew()
    }
    func dismissVIew() {
        fontColorPreview.backgroundColor = UIColor.blackColor()
        opacity = 20
        playListName.text = ""
        playListImageView.image = UIImage(named: "camera_bg")
        UploadImage = ""
        playListName.text = ""
        opacity = 20
        playListImageView.alpha = CGFloat(opacity) / 100
        opacityLabel.text = "\(opacity)%"
        opacitySlider.value = 10
        if editPlayList != nil {
            editPlayList = nil
            self.navigationController?.popViewControllerAnimated(true)
        }else {
            if self.tabBarController!.selectedIndex == 2 {
                self.navigationController?.popViewControllerAnimated(true)
            }else {
                self.tabBarController!.selectedIndex = 2
            }
        }
        
    }
    
    @IBAction func opacitySlider(sender: UISlider) {
        opacity = Int(opacitySlider.value) * Int(5.0)
        print(opacity)
        opacityLabel.text = "\(opacity)%"
        playListImageView.alpha = CGFloat(opacity) / 100
    }
    func passEditedImage(image: UIImage) {
        PlayListImage = image
        UploadImage = SP_DELEGATE.encodeImage(image)
        self.playListImageView.image = PlayListImage
    }
    // MARK: - Color Matrix (only for test case)
    var colorMatrix = [ [UIColor]() ]
    private func fillColorMatrix(numX: Int, _ numY: Int) {
        colorMatrix.removeAll()
        if numX > 0 && numY > 0 {
            
            for _ in 0..<numX {
                var colInX = [UIColor]()
                for _ in 0..<numY {
                    colInX += [UIColor.randomColor()]
                }
                colorMatrix += [colInX]
            }
        }
    }
    
    
    // MARK: Color Picker Delegate
    func colorSelectionChanged(selectedColor color: UIColor) {
        fontColorPreview.backgroundColor = color
        print(color)
        if let rgb = color.colourToHex() {
            print(rgb)
            playListColour = rgb
        } else {
            print("conversion failed")
        }
    }
    
    // MARK: - WebServices
    func postCreatePlayList(params: NSDictionary) {
        socialDelegate?.showActivity!("Loading..")
        SP_NETWORK.postJson(WEB_SERVICE + "api/PlayList/UpdatePlayList", method: "POST", params: params as! Dictionary<String, AnyObject>, is_Dictionary: true) { (Succeeded, Json) -> () in
            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                print(Succeeded)
                print(Json)
                self.socialDelegate?.removeActivity!()
                if Succeeded == true {
                    let pid = Json["Id"]
                    if let status = Json["Status"] {
                        if status as? String == "1" {
//                            self.dismissVIew()
                            let addSong = self.storyboard?.instantiateViewControllerWithIdentifier("SPAddSongs") as? SPAddSongs
                            if self.editPlayList == nil {
                                addSong?.fromCreate = false
                            }else {
                                addSong?.fromCreate = true
                            }
                            addSong?.playListId = pid as? String
                            self.fontColorPreview.backgroundColor = UIColor.blackColor()
                            self.opacity = 20
                            self.playListName.text = ""
                            self.playListImageView.image = UIImage(named: "camera_bg")
                            self.UploadImage = ""
                            self.playListName.text = ""
                            self.opacity = 20
                            self.playListImageView.alpha = CGFloat(self.opacity) / 100
                            self.opacityLabel.text = "\(self.opacity)%"
                            self.opacitySlider.value = 10
                            if self.editPlayList != nil {
                                self.editPlayList = nil
                            }
                            self.showViewController(addSong!, sender: self)
                        }else {
                            SP_DELEGATE.showAlert("Message", message: "Playlist update failed", buttonTitle: "OK")
                        }
                        
                    }
                    
                }
            })
        }
    }
    
    
    
    // MARK: - Custom Delegates
    func hideLogoutButton(isHide: Bool) {
        socialDelegate?.hideLogoutButton!(isHide)
    }
    func navigateTab(tab: Int) {
        socialDelegate?.navigateTab!(tab)
    }
    func hideBackButton(isHide: Bool) {
        socialDelegate?.hideBackButton!(isHide)
    }
    func showActivity(title: String) {
        socialDelegate?.showActivity!(title)
    }
    func removeActivity() {
        socialDelegate?.removeActivity!()
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func adaptivePresentationStyleForPresentationController(controller: UIPresentationController) -> UIModalPresentationStyle {
        
        return UIModalPresentationStyle.None
    }
    
    // MARK: - System
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "imageEditor" {
            let editor = segue.destinationViewController as! SPImageEditor
            editor.delegate = self
            editor.socialDelegate = self
            editor.imageToEdit = PlayListImage
        }else {
            let colorPickerVC = segue.destinationViewController as! SwiftColorPickerViewController
            colorPickerVC.delegate = self
            //            colorPickerVC.dataSource = self
            if let popPresentationController = colorPickerVC.popoverPresentationController {
                popPresentationController.delegate = self
            }
        }
    }
    
    
}
extension UIColor {
    
    func colourToHex() -> Int? {
        var fRed : CGFloat = 0
        var fGreen : CGFloat = 0
        var fBlue : CGFloat = 0
        var fAlpha: CGFloat = 0
        if self.getRed(&fRed, green: &fGreen, blue: &fBlue, alpha: &fAlpha) {
            let iRed = Int(fRed * 255.0)
            let iGreen = Int(fGreen * 255.0)
            let iBlue = Int(fBlue * 255.0)
            let iAlpha = Int(fAlpha * 255.0)
            
            //  (Bits 24-31 are alpha, 16-23 are red, 8-15 are green, 0-7 are blue).
            let rgb = (iAlpha << 24) + (iRed << 16) + (iGreen << 8) + iBlue
            return rgb
        } else {
            // Could not extract RGBA components:
            return nil
        }
    }
}
