//
//  SPRegistration.swift
//  Social Parrot
//
//  Created by Abdul Malik Aman on 11/06/16.
//  Copyright © 2016 INTELLIPATTERN. All rights reserved.
//

import UIKit

class SPRegistration: UIViewController, UINavigationControllerDelegate, UIImagePickerControllerDelegate, UIActionSheetDelegate {

    // MARK: - Variables
    var socialDelegate: SocialParrot_Delegate?
    @IBOutlet weak var RegistrationTable: UITableView!
    @IBOutlet weak var backBtn: SPRoundButton!
    var placeHolderArray = [["Add team", "Add position", "Add jersey No."], ["Email", "Username", "Mobile"]]
    var imageArray = [["reg_addteam", "reg_position", "reg_position"], ["reg_email", "reg_username", "reg_mobile"]]
    var becomeResponder: Int!
    var table_h: CGFloat!
    var is_keyboard_up = false
    var is_SpecialInvitee = false
    var imagePicker = UIImagePickerController()
    var valueArray: [String] = [String](count: 13, repeatedValue: "")
    var ProfileImage: UIImage!
    var imageURL: String!
    var UploadImage: String = ""
    var is_Failed: Int!
    var userId: String!
    var buttonTitles:[ImagePickerActionSheetButtons:String]!
    enum ImagePickerActionSheetButtons {
        case Camera
        case Chooser
    }
    
    // MARK: - View Methods
    override func viewDidLayoutSubviews() {
        if is_keyboard_up == true {
            RegistrationTable.frame.size.height = self.view.frame.height - (KEYBOARD_FRAME + 40)
        }else {
            RegistrationTable.frame.size.height = table_h
        }
    }
    override func viewWillAppear(animated: Bool) {
        self.navigationController?.navigationBarHidden = true
        if userId != nil {
            let params: NSDictionary = ["currentUserId": USER_ID!, "UserId": USER_ID!,"Email" : EMAIL_ID!]
            postUserProfile(params)
            backBtn.hidden = false
            //socialDelegate?.hideBackButton!(false)
        }
        else
        {
            backBtn.hidden = true
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()

        print(valueArray)
        if IS_SPECIAL_INVITEE == false {
            is_SpecialInvitee = false
            placeHolderArray.removeAtIndex(0)
            imageArray.removeAtIndex(0)
        }else {
            is_SpecialInvitee = true
        }
        table_h = RegistrationTable.frame.size.height
        imagePicker.delegate = self
        // Do any additional setup after loading the view.
        
//        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: "dismissKeyboard")
//        view.addGestureRecognizer(tap)
    }
    
    // MARK: - Actions
    @IBAction func Back(sender: AnyObject) {
        self.navigationController?.popViewControllerAnimated(true)
    }
    @IBAction func Save(sender: AnyObject) {
        self.view.endEditing(true)
        validate()
    }
    func validate() {
        is_Failed = nil
        if SP_DELEGATE.TrimString(valueArray[0]) == "" {
            is_Failed = 0
            SP_DELEGATE.showAlert("Message", message: "Enter First Name", buttonTitle: "OK")
        }else if SP_DELEGATE.TrimString(valueArray[1]) == "" {
            is_Failed = 1
            SP_DELEGATE.showAlert("Message", message: "Enter Last Name", buttonTitle: "OK")
        }else if SP_DELEGATE.TrimString(valueArray[8]) == "" {
            is_Failed = 8
        }else if SP_DELEGATE.isValidEmail(valueArray[8]) == false {
            is_Failed = 8
        }else if SP_DELEGATE.TrimString(valueArray[9]) == "" {
            is_Failed = 9
        }
        
        
        else {
            let params: NSDictionary = ["UserId": "", "Status": "Active", "FirstName": valueArray[0], "LastName": valueArray[1], "FBProfile": valueArray[2], "TweetProfile": valueArray[3], "InstaProfile": valueArray[4], "Team": valueArray[5], "Position": valueArray[6], "Jersey": valueArray[7], "Email": valueArray[8], "NickName": valueArray[9], "Mobile": valueArray[10], "Description": valueArray[11], "Image": UploadImage]
            
            postRegistration(params)
        }
        RegistrationTable.reloadData()
    }
    func SaveImage(image: UIImage) {
        if PROFILE_PICTURE != "" {
            SP_DELEGATE.removeFileFromPath("ProfilePicture")
            PROFILE_PICTURE = SP_DELEGATE.storeImage(image, imageName: "ProfilePicture")
            SP_DEFAULTS.setValue(PROFILE_PICTURE, forKey: "ProfilePic")
        }else {
            PROFILE_PICTURE = SP_DELEGATE.storeImage(image, imageName: "ProfilePicture")
            SP_DEFAULTS.setValue(PROFILE_PICTURE, forKey: "ProfilePic")
        }
    }
    
    @IBAction func choosePhoto(sender: AnyObject) {
        let croppingEnabled = true
        let cameraViewController = CameraViewController(croppingEnabled: croppingEnabled) { [weak self] image, asset in
            
            if image != nil {
                self!.ProfileImage = image
                self!.UploadImage = SP_DELEGATE.encodeImage(image!)
                self!.RegistrationTable.reloadData()
            }
            
            self?.dismissViewControllerAnimated(true, completion: nil)
        }
        
        presentViewController(cameraViewController, animated: true, completion: nil)
//        
//        let alert = UIAlertController(title: "Choose Image", message: "Choose an image for Profile", preferredStyle: UIAlertControllerStyle.ActionSheet)
//        alert.addAction(UIAlertAction(title: "Camera", style: UIAlertActionStyle.Default, handler: { action in
//            self.CameraAction()
//        }))
//        alert.addAction(UIAlertAction(title: "Photo Library", style: UIAlertActionStyle.Default, handler: { action in
//            self.PhotosAction()
//        }))
//        alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.Cancel, handler: nil))
//        self.presentViewController(alert, animated: true, completion: nil)
    }
    func scrollToTop(row: Int, section: Int, table: UITableView) {
        let indexPath = NSIndexPath(forRow: row, inSection: section)
        table.scrollToRowAtIndexPath(indexPath, atScrollPosition: .Top, animated: true)
    }
    func CameraAction() {
        imagePicker.sourceType = UIImagePickerControllerSourceType.Camera
        self.presentViewController(imagePicker, animated: true, completion: nil)
        imagePicker.allowsEditing = false
    }
    func PhotosAction() {
        imagePicker.sourceType = UIImagePickerControllerSourceType.PhotoLibrary
        self.presentViewController(imagePicker, animated: true, completion: nil)
        imagePicker.allowsEditing = false
    }
    func actionSheet(sheet: UIActionSheet, clickedButtonAtIndex buttonIndex: Int){
        
        if (sheet.buttonTitleAtIndex(buttonIndex) == buttonTitles[.Chooser]!) {
            PhotosAction()
        }else if (sheet.buttonTitleAtIndex(buttonIndex) == buttonTitles[.Camera]!) {
            CameraAction()
        }else if (buttonIndex == sheet.cancelButtonIndex) {
            
        }
    }
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]) {
            let image = info[UIImagePickerControllerOriginalImage] as! UIImage
            UploadImage = SP_DELEGATE.encodeImage(image)
      // print(UploadImage)
        ProfileImage = image
        self.dismissViewControllerAnimated(true, completion: { () -> Void in
            self.RegistrationTable.reloadData()
        })
    }
    
    // MARK:- TableView
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if is_SpecialInvitee == false {
            if section == 0 {
                return 1
            }else if section == 2 {
                return 1
            }else {
                return 3
            }
        }else {
            if section == 0 {
                return 1
            }else if section == 3 {
                return 1
            }else {
                return 3
            }
        }
    }
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        if is_SpecialInvitee == false {
            return 3
        }else {
            return 4
        }
    }
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var textViewSection: Int!
        if is_SpecialInvitee == false {
            textViewSection = 2
        }else {
            textViewSection = 3
        }
        var identifier: String!
        if indexPath.section == 0 {
            identifier = "photoCell"
        }else if indexPath.section == textViewSection {
            identifier = "textView"
        }else {
            identifier = "textField"
        }
        let cell = RegistrationTable.dequeueReusableCellWithIdentifier(identifier, forIndexPath: indexPath) as! SPRegistrationCell
        if indexPath.section != 0 && indexPath.section != textViewSection {
            cell.registrationText.placeholder = placeHolderArray[indexPath.section - 1][indexPath.row]
            cell.registrationImage.image = UIImage(named: imageArray[indexPath.section - 1][indexPath.row])
            if indexPath.section == 1 {
                if is_SpecialInvitee == false {
                    if indexPath.row == 0 {
                        cell.registrationText.keyboardType = UIKeyboardType.EmailAddress
                    }
                    cell.registrationText.tag = indexPath.row + 8
                    cell.registrationImage.tag = indexPath.row + 8
                }else {
                    cell.registrationText.tag = indexPath.row + 5
                    cell.registrationImage.tag = indexPath.row + 5
                }
                if is_Failed != nil {
                    if is_Failed == 8 {
                        if indexPath.row == 0 {
                            SP_DELEGATE.ShakeAnimation(cell)
                        }
                    }else if is_Failed == 9 {
                        if indexPath.row == 1 {
                            SP_DELEGATE.ShakeAnimation(cell)
                        }
                    }
                }
            }else {
                cell.registrationText.tag = indexPath.row + 8
                cell.registrationImage.tag = indexPath.row + 8
                if is_Failed != nil {
                    if is_Failed == 8 {
                        if indexPath.row == 0 {
                            SP_DELEGATE.ShakeAnimation(cell)
                        }
                    }else if is_Failed == 9 {
                        if indexPath.row == 1 {
                            SP_DELEGATE.ShakeAnimation(cell)
                        }
                    }
                }
            }
        }else if indexPath.section == 0 {
            if userId != nil {
                if ProfileImage != nil {
                    cell.ProfilePic.image = ProfileImage
                }else {
                    if imageURL != nil {
                        if let url = NSURL(string: imageURL) {
                            cell.ProfilePic.sd_setImageWithURL(url, placeholderImage: UIImage(named: "Profile_bg"))
                            ProfileImage = cell.ProfilePic.image
                            UploadImage = SP_DELEGATE.encodeImage(ProfileImage)
                        }
                    }
                }
                
            }else {
                if ProfileImage != nil {
                    cell.ProfilePic.image = ProfileImage
                }
            }
            
            
                    }
        if indexPath.section != textViewSection {
            if indexPath.section == 0 {
                cell.FirstNameText.text = valueArray[0]
                cell.LastNameText.text = valueArray[1]
                cell.facebookText.text = valueArray[2]
                cell.twitterText.text = valueArray[3]
                cell.instagramText.text = valueArray[4]
            }else if indexPath.section == 1 {
                if is_SpecialInvitee == true {
                    if indexPath.row == 0 {
                        cell.registrationText.text = valueArray[5]
                    }else if indexPath.row == 1 {
                        cell.registrationText.text = valueArray[6]
                    }else {
                        cell.registrationText.keyboardType = UIKeyboardType.NumbersAndPunctuation
                        cell.registrationText.text = valueArray[7]
                    }
                }else {
                    cell.registrationText.userInteractionEnabled = true
                    if indexPath.row == 0 {
                        cell.registrationText.userInteractionEnabled = false
                        cell.registrationText.text = valueArray[8]
                    }else if indexPath.row == 1 {
                        cell.registrationText.text = valueArray[9]
                    }else {
                        cell.registrationText.keyboardType = UIKeyboardType.NumbersAndPunctuation
                        cell.registrationText.text = valueArray[10]
                    }
                }
            }else {
                cell.registrationText.userInteractionEnabled = true
                if indexPath.row == 0 {
                    cell.registrationText.userInteractionEnabled = false
                    cell.registrationText.text = valueArray[8]
                }else if indexPath.row == 1 {
                    cell.registrationText.text = valueArray[9]
                }else {
                    cell.registrationText.keyboardType = UIKeyboardType.NumbersAndPunctuation
                    cell.registrationText.text = valueArray[10]
                }
            }
        }else {
            if valueArray[11] == "" || valueArray[11] == "Add your description     " {
                cell.descriptionText.text = "Add your description     "
                cell.descriptionText.textColor = UIColor.lightGrayColor()
            }else {
                cell.descriptionText.text = valueArray[11]
                cell.descriptionText.textColor = UIColor.blackColor()
            }
            
        }
        return cell
    }
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
    }
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        if is_SpecialInvitee == false {
            if indexPath.section == 0 {
                return 210
            }else if indexPath.section == 2 {
                return 170
            }else {
                return 27
            }
        }else {
            if indexPath.section == 0 {
                return 210
            }else if indexPath.section == 3 {
                return 70
            }else {
                return 27
            }
        }
        
    }
    func tableView(tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        if is_SpecialInvitee == false {
            return 5
        }else {
            if section == 0 {
                return 2
            }else if section == 1 {
                return 10
            }else {
                return 5
            }
        }
    }
    func tableView(tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        var height: CGFloat = 0.0
        if section == 0 {
            height = 2
        }else if section == 1 {
            height = 10
        }else {
            height = 5
        }
        let footer = UIView(frame: CGRectMake(0, 0, self.RegistrationTable.frame.width, height))
        footer.backgroundColor = UIColor.whiteColor()
        return footer
    }

    // MARK: - TextField Delegates
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        if textField.tag != 10 {
            becomeResponder = textField.tag + 1
        }else {
            becomeResponder = nil
            textField.resignFirstResponder()
        }
        RegistrationTable.reloadData()
        return true
    }
    func textFieldDidBeginEditing(textField: UITextField) {
        print(textField.tag)
        is_keyboard_up = true
        var textViewSection: Int!
        if is_SpecialInvitee == false {
            textViewSection = 1
        }else {
            textViewSection = 2
        }
        switch textField.tag {
            case 0, 1, 2, 3, 4: scrollToTop(0, section: 0, table: RegistrationTable)
            case 5: scrollToTop(1, section: 1, table: RegistrationTable)
            case 6: scrollToTop(2, section: 1, table: RegistrationTable)
            case 7: scrollToTop(2, section: 1, table: RegistrationTable)
            case 8: scrollToTop(1, section: textViewSection, table: RegistrationTable)
            case 9: scrollToTop(2, section: textViewSection, table: RegistrationTable)
            case 10: scrollToTop(2, section: textViewSection, table: RegistrationTable)
        default:
            break
        }
//        if textField.tag < 5 {
//            scrollToTop(textField.tag, section: 0, table: RegistrationTable)
//        }else if textField.tag < 8 && textField.tag > 4 {
//            scrollToTop(textField.tag - 5, section: 1, table: RegistrationTable)
//        }else if textField.tag < 12 && textField.tag > 7 {
//            scrollToTop(textField.tag - 3, section: textViewSection, table: RegistrationTable)
//        }
        viewDidLayoutSubviews()
    }
    func textFieldDidEndEditing(textField: UITextField) {
        is_keyboard_up = false
        valueArray[textField.tag] = SP_DELEGATE.TrimString(textField.text!)
        viewDidLayoutSubviews()
    }
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        if textField.tag == 10 {
            let newString = (textField.text! as NSString).stringByReplacingCharactersInRange(range, withString: string)
            let components = newString.componentsSeparatedByCharactersInSet(NSCharacterSet.decimalDigitCharacterSet().invertedSet)
            
            let decimalString = components.joinWithSeparator("") as NSString
            let length = decimalString.length
            let hasLeadingOne = length > 0 && decimalString.characterAtIndex(0) == (1 as unichar)
            
            if length == 0 || (length > 10 && !hasLeadingOne) || length > 11
            {
                let newLength = ((textField.text)?.characters.count)! + (string as String).characters.count - range.length as Int
                
                return (newLength > 10) ? false : true
            }
            var index = 0 as Int
            let formattedString = NSMutableString()
            
            if hasLeadingOne
            {
                formattedString.appendString("1 ")
                index += 1
            }
            if (length - index) > 3
            {
                let areaCode = decimalString.substringWithRange(NSMakeRange(index, 3))
                formattedString.appendFormat("(%@) ", areaCode)
                index += 3
            }
            if length - index > 3
            {
                let prefix = decimalString.substringWithRange(NSMakeRange(index, 3))
                formattedString.appendFormat("%@- ", prefix)
                index += 3
            }
            
            let remainder = decimalString.substringFromIndex(index)
            formattedString.appendString(remainder)
            textField.text = formattedString as String
            return false
        }else {
            return true
        }
    }
    
    // MARK: - TextView Delegates
    func textView(textView: UITextView, shouldChangeTextInRange range: NSRange, replacementText text: String) -> Bool {
        if(text == "\n") {
            textView.resignFirstResponder()
            return false
        }
        return true
    }
    func textViewDidBeginEditing(textView: UITextView) {
        is_keyboard_up = true
        viewDidLayoutSubviews()
        var textViewSection: Int!
        if is_SpecialInvitee == false {
            textViewSection = 2
        }else {
            textViewSection = 3
        }
        scrollToTop(0, section: textViewSection, table: RegistrationTable)
        if textView.text == "Add your description     " {
            textView.text = ""
            textView.textColor = UIColor.blackColor()
        }
    }
    func textViewDidEndEditing(textView: UITextView) {
        is_keyboard_up = false
        viewDidLayoutSubviews()
        if textView.text == "" {
            textView.text = "Add your description     "
            textView.textColor = UIColor.lightGrayColor()
        }else {
            valueArray[11] = SP_DELEGATE.TrimString(textView.text!)
        }
    }
    
    // MARK: - WebServices
    func postRegistration(params: NSDictionary) {
        SP_DELEGATE.showActivity(self.view, myTitle: "Loading..")
        SP_NETWORK.postJson(WEB_SERVICE + "api/Account/UpdateUser", method: "POST", params: params as! Dictionary<String, AnyObject>, is_Dictionary: true) { (Succeeded, Json) -> () in
            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                print(Succeeded)
                print(Json)
                SP_DELEGATE.removeActivity(self.view)
                if Succeeded == true {
                    if let status = Json["Status"] {
                        if status as? String == "1" {
                            
                            if self.ProfileImage != nil{
                            self.SaveImage(self.ProfileImage)
                            }
                            
                            SP_DEFAULTS.setValue(false, forKey: "Login")
                            SP_DEFAULTS.setValue(self.valueArray[8], forKey: "mailId")
                            EMAIL_ID = self.valueArray[8]
                            self.performSegueWithIdentifier("Home", sender: self)
                            
                            
                        } else {
                            let messageArray = Json["Message"] as? NSArray
                            SP_DELEGATE.showAlert("Message", message: messageArray![0] as! String, buttonTitle: "OK")
                        }
                    }}
            })
        }
    }
    func postUserProfile(params: NSDictionary) {
        SP_DELEGATE.showActivity(self.view, myTitle: "Loading..")
        SP_NETWORK.postJson(WEB_SERVICE + "api/Account/UserView", method: "POST", params: params as! Dictionary<String, AnyObject>, is_Dictionary: true) { (Succeeded, Json) -> () in
            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                print(Succeeded)
                print(Json)
                SP_DELEGATE.removeActivity(self.view)
                if Succeeded == true {
                    if let status = Json["Status"] {
                        if status as? String == "1" {
                            if let Result = Json["UserDataView"] {
                                
                                if let url = Result["ImageURL"] {
                                    if url as! String != "" {
                                        self.imageURL = url as! String
                                    }
                                }
//                                let ImageParams = ["UserId": Result["UserId"]]
//                                self.postImage(ImageParams)
                                
                                if let userType = Result["SpecialInvitee"] {
                                    if userType as? String == "N" {
                                        self.is_SpecialInvitee = false
                                    }else {
                                        self.is_SpecialInvitee = true
                                    }
                                } else {
                                    self.is_SpecialInvitee = false
                                }
                                
                                self.valueArray[0] = (Result["FirstName"] as? String)!
                                self.valueArray[1] = (Result["LastName"] as? String)!
                                self.valueArray[2] = (Result["FBProfile"] as? String)!
                                self.valueArray[3] = (Result["TweetProfile"] as? String)!
                                self.valueArray[4] = (Result["InstaProfile"] as? String)!
                                self.valueArray[5] = (Result["Team"] as? String)!
                                self.valueArray[6] = (Result["Position"] as? String)!
                                self.valueArray[7] = (Result["Jersey"] as? String)!
                                self.valueArray[8] = (Result["Email"] as? String)!
                                self.valueArray[9] = (Result["NickName"] as? String)!
                                self.valueArray[10] = (Result["Mobile"] as? String)!
                                self.valueArray[11] = (Result["Description"] as? String)!
                            }
                        }
                    }
                }
                self.RegistrationTable.reloadData()
            })
        }
    }
    func postImage(params: NSDictionary) {
        //socialDelegate?.showActivity!("Loading..")
        SP_NETWORK.postJson(WEB_SERVICE + "api/Account/SearchUserImage", method: "POST", params: params as! Dictionary<String, AnyObject>, is_Dictionary: true) { (Succeeded, Json) -> () in
            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                print(Succeeded)
                print(Json)
                //self.socialDelegate?.removeActivity!()
                if Succeeded == true {
                    let ImageResults = Json["ResultSet"] as? NSDictionary
                    
                    if ImageResults != nil {
                        let ImageString = ImageResults!["Image"] as? String
                        
                        if PROFILE_PICTURE != "" {
                            SP_DELEGATE.removeFileFromPath("ProfilePicture")
                        }
                        
                        if ImageString != "" {
                            
                            self.ProfileImage = SP_DELEGATE.decodeImage(ImageString!)
                        }
                    }else {
                        if PROFILE_PICTURE != "" {
                            SP_DELEGATE.removeFileFromPath("ProfilePicture")
                        }
                    }
                    
                }else {
                    if PROFILE_PICTURE != "" {
                        SP_DELEGATE.removeFileFromPath("ProfilePicture")
                    }
                }
                self.RegistrationTable.reloadData()
            })
        }
    }
    // MARK: - System
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
class SPRegistrationCell: UITableViewCell {
    
    @IBOutlet weak var FirstNameText: SPTextField!
    @IBOutlet weak var LastNameText: SPTextField!
    @IBOutlet weak var facebookText: SPTextField!
    @IBOutlet weak var twitterText: SPTextField!
    @IBOutlet weak var instagramText: SPTextField!
    @IBOutlet weak var registrationText: SPTextField!
    @IBOutlet weak var descriptionText: SPTextView!
    @IBOutlet weak var registrationImage: UIImageView!
    @IBOutlet weak var ProfilePic: UIImageView!
    
}