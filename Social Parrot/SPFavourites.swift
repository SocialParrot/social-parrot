//
//  SPFavourites.swift
//  Social Parrot
//
//  Created by Abdul Malik Aman on 03/07/16.
//  Copyright © 2016 INTELLIPATTERN. All rights reserved.
//

import UIKit

class SPFavourites: UIViewController, UIGestureRecognizerDelegate, SocialParrot_Delegate, keyBoardUpDelegate {
    
    // MARK: - Variables
    @IBOutlet weak var FavouritesCollection: UICollectionView!
    @IBOutlet weak var AlertContainer: UIView!
    @IBOutlet weak var mainAlert: UIView!
    @IBOutlet weak var CommentContainer: UIView!
    @IBOutlet weak var mainContainer: UIView!
    
    var is_AlertShown = false
    var selectedPlayList: Int!
    var favouriteArray: NSArray!
    var socialDelegate: SocialParrot_Delegate?
    var passplayListId: String!
    var passPlayListName: String!
    var passBGImage: NSURL!
    var passPlayList: NSDictionary!
    var checkLiked: Int!
    var commentHeight: CGFloat!
    var commentController: SPComments!
    var is_Show_Comments = false
    var passPlayListUserID: String!
    var favpass: Int!
    
    // MARK: - View Methods
    override func viewWillAppear(animated: Bool) {
        self.navigationController?.navigationBarHidden = true
        is_Show_Comments = false
        socialDelegate?.hideBackButton!(false)
        socialDelegate?.hideLogoutButton!(false)
        fetchPlayLists()
        
        if is_AlertShown == true {
            AlertContainer.hidden = true
            is_AlertShown = false
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        commentHeight = mainContainer.frame.size.height
        mainAlert.layer.cornerRadius = 15
        mainAlert.layer.masksToBounds = true
        
        let gesture = UILongPressGestureRecognizer(target: self, action: "handleLongPress:")
        gesture.minimumPressDuration = 0.5
        gesture.delaysTouchesBegan = true
        gesture.delegate = self
        self.FavouritesCollection.addGestureRecognizer(gesture)
        
        // Do any additional setup after loading the view.
    }
    // MARK: - Actions
    func fetchPlayLists() {
        let params = ["UserId": USER_ID!, "CurrentUser": USER_ID!]
        postFavouriteList(params)
    }
    
    @IBAction func CommentFavorites(sender: AnyObject) {
        socialDelegate?.hideLogoutButton!(true)
        passplayListId = (favouriteArray[sender.tag]["ID"] as? String)!
        if is_Show_Comments == false {
            CommentContainer.hidden = false
            is_Show_Comments = true
        }else {
            CommentContainer.hidden = true
            is_Show_Comments = false
        }
        commentController = self.childViewControllers[0] as! SPComments
        commentController.playListId = passplayListId
        commentController.viewWillAppear(true)
    }
    @IBAction func LikePlayList(sender: AnyObject) {
        checkLiked = sender.tag
        var likeBool: String!
        if favouriteArray[sender.tag]["Like"] as? String == "Y" {
            likeBool = "N"
        } else{
            likeBool = "Y"
        }
        let params: NSDictionary = ["PlayListId":favouriteArray[sender.tag]["ID"], "RatingUserId": USER_ID!, "Liked": likeBool]
        postLikePlayList(params)
    }
    @IBAction func hideComments(sender: AnyObject) {
        socialDelegate?.hideLogoutButton!(false)
        CommentContainer.hidden = true
        is_Show_Comments = false
        commentController.view.endEditing(true)
        fetchPlayLists()
    }
    func handleLongPress(gestureReconizer: UILongPressGestureRecognizer) {
        if gestureReconizer.state != UIGestureRecognizerState.Ended {
            return
        }
        let p = gestureReconizer.locationInView(self.FavouritesCollection)
        let indexPath = self.FavouritesCollection.indexPathForItemAtPoint(p)
        if let index = indexPath {
            //            let cell = self.PlayListCollection.cellForItemAtIndexPath(index)
            print(index.row)
            selectedPlayList = index.row
            //if favouriteArray[selectedPlayList]["UserId"] as? String == USER_ID {
                if is_AlertShown == false {
                    AlertContainer.hidden = false
                    is_AlertShown = true
                }
            //}
            
        } else {
            print("Could not find index path")
        }
    }
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        if is_AlertShown == true {
            AlertContainer.hidden = true
            is_AlertShown = false
        }
        if is_Show_Comments == true {
            socialDelegate?.hideLogoutButton!(false)
            CommentContainer.hidden = true
            is_Show_Comments = false
            commentController.view.endEditing(true)
            fetchPlayLists()
        }
    }
    func commentkeyBoardUp(isUp: Bool) {
        if isUp == true {
            mainContainer.frame.size.height = self.view.frame.height - (KEYBOARD_FRAME)
        }else {
            mainContainer.frame.size.height = commentHeight
        }
    }
    @IBAction func DeletePlayList(sender: AnyObject) {
    
       let params = ["PlayListId": favouriteArray[selectedPlayList]["ID"], "RatingUserId": USER_ID!, "Favorite": "N", "CurrentUser": USER_ID!]
        postFavouritePlayList(params)
        if is_AlertShown == true {
            AlertContainer.hidden = true
            is_AlertShown = false
        }
    }
    
    // MARK: - CollectionView Delegate Methods
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if favouriteArray != nil {
            return favouriteArray.count
        }else {
            return 0
        }
    }
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = FavouritesCollection.dequeueReusableCellWithReuseIdentifier("playList", forIndexPath: indexPath) as! SPPlayListCell
        
        cell.layer.borderColor = UIColor.blackColor().CGColor
        cell.layer.borderWidth = 0.4
        cell.PlayListName.text = "Test"
        cell.cdRound.layer.cornerRadius = 10
        cell.cdRound.layer.masksToBounds = true
        cell.playlistIcon.layer.cornerRadius = cell.imageContainer.frame.width / 2
        cell.playlistIcon.layer.masksToBounds = true
        cell.numberOfLikes.adjustsFontSizeToFitWidth = true
        cell.numberOfComments.adjustsFontSizeToFitWidth = true
        cell.likeButton.tag = indexPath.row
        cell.likeImage.tag = indexPath.row
        cell.commentButton.tag = indexPath.row
        
        
        if favouriteArray[indexPath.row]["Like"] as? String == "Y" {
            cell.likeImage.image = UIImage(named: "playList_Like_Sel")
        } else{
            cell.likeImage.image = UIImage(named: "playList_Like")
        }
        /*if (favouriteArray[indexPath.row]["BgImage"] as? String != "") {
        cell.playlistIcon.image = SP_DELEGATE.decodeImage((favouriteArray[indexPath.row]["BgImage"] as? String)!)
        }*/
        
        if let url = NSURL(string: favouriteArray[indexPath.row]["BgImageURL"] as! String) {
            cell.playlistIcon.sd_setImageWithURL(url, placeholderImage: UIImage(named: "playlist"))
        }
        
        cell.PlayListName.text = favouriteArray[indexPath.row]["Name"] as? String
        cell.numberOfLikes.text = favouriteArray[indexPath.row]["LikeCount"] as? String
        cell.numberOfComments.text = favouriteArray[indexPath.row]["Comments"] as? String
        return cell
    }
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        
        passPlayListName = favouriteArray[indexPath.row]["Name"] as? String
        passBGImage = NSURL(string: (favouriteArray[indexPath.row]["BgImageURL"] as? String)!)
        passplayListId = (favouriteArray[indexPath.row]["ID"] as? String)!
        passPlayListUserID = (favouriteArray[indexPath.row]["UserId"] as? String)!
        favpass = 1
        performSegueWithIdentifier("Songs", sender: self)
        
    }
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAtIndex section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        let viewWidth = self.FavouritesCollection.frame.width
        let collectionSize = viewWidth / 2
        return CGSizeMake(collectionSize , collectionSize + 10)
    }
    
    // MARK: - Custom Delegates
    func hideLogoutButton(isHide: Bool) {
        socialDelegate?.hideLogoutButton!(isHide)
    }
    func navigateTab(tab: Int) {
        socialDelegate?.navigateTab!(tab)
    }
    func hideBackButton(isHide: Bool) {
        socialDelegate?.hideBackButton!(isHide)
    }
    func showActivity(title: String) {
        socialDelegate?.showActivity!(title)
    }
    func removeActivity() {
        socialDelegate?.removeActivity!()
    }
    
    // MARK: - WebServices
    func postFavouriteList(params: NSDictionary) {
        socialDelegate?.showActivity!("Loading..")
        SP_NETWORK.postJson(WEB_SERVICE + "api/PlayList/SearchFavoritePlayList", method: "POST", params: params as! Dictionary<String, AnyObject>, is_Dictionary: true) { (Succeeded, Json) -> () in
            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                print(Succeeded)
                print(Json)
                self.socialDelegate?.removeActivity!()
                if Succeeded == true {
                    self.favouriteArray = Json["ResultSet"] as? NSArray
                }
                self.FavouritesCollection.reloadData()
            })
        }
    }
    
    func postFavouritePlayList(params: NSDictionary) {
        socialDelegate?.showActivity!("Loading..")
        SP_NETWORK.postJson(WEB_SERVICE + "api/PlayList/MakeFavorite", method: "POST", params: params as! Dictionary<String, AnyObject>, is_Dictionary: true) { (Succeeded, Json) -> () in
            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                print(Succeeded)
                print(Json)
                self.socialDelegate?.removeActivity!()
                if Succeeded == true {
                    
                    self.fetchPlayLists()
                }
            })
        }
    }

   
    
    func postLikePlayList(params: NSDictionary) {
        SP_NETWORK.postJson(WEB_SERVICE + "api/PlayList/UpdateLike", method: "POST", params: params as! Dictionary<String, AnyObject>, is_Dictionary: true) { (Succeeded, Json) -> () in
            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                print(Succeeded)
                print(Json)
                if Succeeded == true {
                    
                    if let status = Json["Status"] {
                        if status as? String == "1" {
                            
                            let likeCount = self.favouriteArray[self.checkLiked]["LikeCount"] as! String
                            
                            if self.favouriteArray[self.checkLiked]["Like"] as? String == "Y" {
                                self.favouriteArray[self.checkLiked].setValue("N", forKey: "Like")
                                self.favouriteArray[self.checkLiked].setValue(String(Int(likeCount)! - 1), forKey: "LikeCount")
                            } else{
                                self.favouriteArray[self.checkLiked].setValue(String(Int(likeCount)! + 1), forKey: "LikeCount")
                                self.favouriteArray[self.checkLiked].setValue("Y", forKey: "Like")
                            }
                            
                            self.FavouritesCollection.reloadData()
                            
                        }
                    }
                    
                }
                self.FavouritesCollection.reloadData()
            })
        }
    }
    
    // MARK: - System
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "Songs" {
            let songs = segue.destinationViewController as! SPSongs
            songs.socialDelegate = self
            songs.playListId = passplayListId
            songs.playListName = passPlayListName
            songs.BGImageData = passBGImage
            songs.playListUserId = passPlayListUserID
            songs.fav = favpass
            
        }else if segue.identifier == "toEditPlayList" {
            let edit = segue.destinationViewController as! SPCreatePlayList
            edit.editPlayList = passPlayList
            edit.socialDelegate = self
        }else if segue.identifier == "Comments" {
            commentController = segue.destinationViewController as! SPComments
            commentController.playListId = passplayListId
            commentController.socialDelegate = self
            commentController.keyBoardDelegate = self
            commentController.playListUserId = passplayListId
        }
    }
    /*
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    // Get the new view controller using segue.destinationViewController.
    // Pass the selected object to the new view controller.
    }
    */
    
}
