//
//  SPPlayList.swift
//  Social Parrot
//
//  Created by Abdul Malik Aman on 24/06/16.
//  Copyright © 2016 INTELLIPATTERN. All rights reserved.
//

import UIKit

class SPPlayList: UIViewController, UIGestureRecognizerDelegate, SocialParrot_Delegate, keyBoardUpDelegate {
    
    // MARK: - Variables
    @IBOutlet weak var PlayListCollection: UICollectionView!
    @IBOutlet weak var AlertContainer: UIView!
    @IBOutlet weak var mainAlert: UIView!
    @IBOutlet weak var backButton: SPRoundButton!
    @IBOutlet weak var CommentContainer: UIView!
    @IBOutlet weak var mainContainer: UIView!
    @IBOutlet weak var searchBar: SPImagedTextFiled!
    
    var socialDelegate: SocialParrot_Delegate?
    var playListArray: NSArray!
    var is_AlertShown = false
    var selectedPlayList: Int!
    var passplayListId: String!
    var passPlayListName: String!
    var passBGImage: NSURL!
    var passPlayList: NSDictionary!
    var rootContanerView = SPRootTab()
    var ImageResults: [String]!
    var PlayListUserId: String!
    var checkLiked: Int!
    var commentHeight: CGFloat!
    var commentController: SPComments!
    var is_Show_Comments = false
    var is_Searching = false
    var searchResults: NSMutableArray!
    var favpass: Int!
    
    // MARK: - View Methods
    override func viewWillAppear(animated: Bool) {
        self.navigationController?.navigationBarHidden = true
        is_Show_Comments = false
        passPlayList = nil
        if PlayListUserId == nil {
            socialDelegate?.navigateTab!(2)
            socialDelegate?.hideBackButton!(false)
            backButton.hidden = true
        }else{
            socialDelegate?.hideBackButton!(true)
            backButton.hidden = false
        }
        if is_AlertShown == true {
            AlertContainer.hidden = true
            is_AlertShown = false
        }
        fetchPlayLists()
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        commentHeight = mainContainer.frame.size.height
        mainAlert.layer.cornerRadius = 15
        mainAlert.layer.masksToBounds = true

        let gesture = UILongPressGestureRecognizer(target: self, action: "handleLongPress:")
        gesture.minimumPressDuration = 0.5
        gesture.delaysTouchesBegan = true
        gesture.delegate = self
        self.PlayListCollection.addGestureRecognizer(gesture)
        // Do any additional setup after loading the view.
    }

    // MARK: - Actions
    
    @IBAction func Back(sender: AnyObject) {
        self.navigationController?.popViewControllerAnimated(true)
    }
    func fetchPlayLists() {
        if PlayListUserId != nil {
            let params: NSDictionary = ["UserId": PlayListUserId!, "CurrentUser": USER_ID!]
            postPlayList(params)
        }else {
            if USER_ID != nil {
                let params: NSDictionary = ["UserId": USER_ID!, "CurrentUser": USER_ID!]
                postPlayList(params)
            }
        }
        
    }
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        if is_AlertShown == true {
            AlertContainer.hidden = true
            is_AlertShown = false
        }
        if is_Show_Comments == true {
            socialDelegate?.hideLogoutButton!(false)
            CommentContainer.hidden = true
            is_Show_Comments = false
            commentController.view.endEditing(true)
            fetchPlayLists()
        }
    }
    @IBAction func DeletePlayList(sender: AnyObject) {
        var mainArray: NSArray!
        if self.is_Searching == true {
            mainArray = self.searchResults
        }else {
            mainArray = self.playListArray
        }
        let params = ["ID": mainArray[selectedPlayList]["ID"]]
        postDeletePlayList(params)
    }
    @IBAction func EditPlayList(sender: AnyObject) {
        var mainArray: NSArray!
        if self.is_Searching == true {
            mainArray = self.searchResults
        }else {
            mainArray = self.playListArray
        }
        passPlayList = mainArray[selectedPlayList] as? NSDictionary
        performSegueWithIdentifier("toEditPlayList", sender: self)
    }
    @IBAction func LikePlayList(sender: AnyObject) {
        checkLiked = sender.tag
        var likeBool: String!
        var mainArray: NSArray!
        if self.is_Searching == true {
            mainArray = self.searchResults
        }else {
            mainArray = self.playListArray
        }
        if mainArray[sender.tag]["Like"] as? String == "Y" {
            likeBool = "N"
        } else{
            likeBool = "Y"
        }
        let params: NSDictionary = ["PlayListId":mainArray[sender.tag]["ID"], "RatingUserId": USER_ID!, "Liked": likeBool]
        postLikePlayList(params)
        
    }
    func commentkeyBoardUp(isUp: Bool) {
        if isUp == true {
            mainContainer.frame.size.height = self.view.frame.height - (KEYBOARD_FRAME)
        }else {
            mainContainer.frame.size.height = commentHeight
        }
    }
    @IBAction func CommentPlayList(sender: AnyObject) {
        socialDelegate?.hideLogoutButton!(true)
        var mainArray: NSArray!
        if self.is_Searching == true {
            mainArray = self.searchResults
        }else {
            mainArray = self.playListArray
        }
        passplayListId = (mainArray[sender.tag]["ID"] as? String)!
        if is_Show_Comments == false {
            CommentContainer.hidden = false
            is_Show_Comments = true
        }else {
            CommentContainer.hidden = true
            is_Show_Comments = false
        }
        commentController = self.childViewControllers[0] as! SPComments
        commentController.playListId = passplayListId
        commentController.viewWillAppear(true)
    }
    @IBAction func hideComments(sender: AnyObject) {
        socialDelegate?.hideLogoutButton!(false)
        CommentContainer.hidden = true
        is_Show_Comments = false
        commentController.view.endEditing(true)
        fetchPlayLists()
    }
    func handleLongPress(gestureReconizer: UILongPressGestureRecognizer) {
        if gestureReconizer.state != UIGestureRecognizerState.Ended {
            return
        }
        let p = gestureReconizer.locationInView(self.PlayListCollection)
        let indexPath = self.PlayListCollection.indexPathForItemAtPoint(p)
        if let index = indexPath {
            //            let cell = self.PlayListCollection.cellForItemAtIndexPath(index)
            print(index.row)
            selectedPlayList = index.row
            var mainArray: NSArray!
            if self.is_Searching == true {
                mainArray = self.searchResults
            }else {
                mainArray = self.playListArray
            }
            if mainArray[selectedPlayList]["UserId"] as? String == USER_ID {
                if is_AlertShown == false {
                    AlertContainer.hidden = false
                    is_AlertShown = true
                }
            }
        } else {
            print("Could not find index path")
        }
    }
    
    func searchFromPlayList(keyWord: String) {
        if keyWord != "" {
            if playListArray != nil {
                searchResults = []
                for i in 0..<playListArray.count {
                    if playListArray[i]["Name"]!.lowercaseString.rangeOfString(keyWord.lowercaseString) != nil {
                        searchResults.addObject(playListArray[i])
                    }
                }
                is_Searching = true
            }else {
                is_Searching = false
            }
        }else {
            is_Searching = false
        }
        
        PlayListCollection.reloadData()
    }
    @IBAction func SearchPlaylist(sender: AnyObject) {
        self.view.endEditing(true)
        searchFromPlayList(searchBar.text!)
    }
    
    // MARK: - CollectionView Delegate Methods
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if is_Searching == true {
            if searchResults != nil {
                return searchResults.count
            }else {
                return 0
            }
        }else {
            if playListArray != nil {
                return playListArray.count
            }else {
                return 0
            }
        }
    }
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = PlayListCollection.dequeueReusableCellWithReuseIdentifier("playList", forIndexPath: indexPath) as! SPPlayListCell
        
        cell.layer.borderColor = UIColor.blackColor().CGColor
        cell.layer.borderWidth = 0.4
        cell.PlayListName.text = "Test"
        cell.cdRound.layer.cornerRadius = 10
        cell.cdRound.layer.masksToBounds = true
        cell.playlistIcon.layer.cornerRadius = cell.imageContainer.frame.width / 2
        cell.playlistIcon.layer.masksToBounds = true
        cell.numberOfLikes.adjustsFontSizeToFitWidth = true
        cell.numberOfComments.adjustsFontSizeToFitWidth = true
        cell.likeButton.tag = indexPath.row
        cell.likeImage.tag = indexPath.row
        cell.commentButton.tag = indexPath.row
        
        var mainArray: NSArray!
        
        if is_Searching == true {
            mainArray = searchResults
        }else {
            mainArray = playListArray
        }
        
        if mainArray[indexPath.row]["Like"] as? String == "Y" {
            cell.likeImage.image = UIImage(named: "playList_Like_Sel")
        } else{
            cell.likeImage.image = UIImage(named: "playList_Like")
        }
        
        cell.PlayListName.text = mainArray[indexPath.row]["Name"] as? String
        cell.numberOfLikes.text = mainArray[indexPath.row]["LikeCount"] as? String
        cell.numberOfComments.text = mainArray[indexPath.row]["Comments"] as? String
        //        cell.playlistIcon.image = SP_DELEGATE.decodeImage(ImageResults[indexPath.row])
        
        if let url = NSURL(string: mainArray[indexPath.row]["BgImageURL"] as! String) {
            cell.playlistIcon.sd_setImageWithURL(url, placeholderImage: UIImage(named: "playlist"))
        }
        return cell
    }
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        var mainArray: NSArray!
        if is_Searching == true {
            mainArray = searchResults
        }else {
            mainArray = playListArray
        }
        passPlayListName = mainArray[indexPath.row]["Name"] as? String
        passBGImage = NSURL(string: (mainArray[indexPath.row]["BgImageURL"] as? String)!)
        passplayListId = (mainArray[indexPath.row]["ID"] as? String)!
        favpass = 0
        performSegueWithIdentifier("Songs", sender: self)
    }
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAtIndex section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        let viewWidth = self.PlayListCollection.frame.width
        let collectionSize = viewWidth / 2
        return CGSizeMake(collectionSize , collectionSize + 10)
    }
    
    // MARK: - Custom Delegates
    func hideLogoutButton(isHide: Bool) {
        socialDelegate?.hideLogoutButton!(isHide)
    }
    func navigateTab(tab: Int) {
        socialDelegate?.navigateTab!(tab)
    }
    func hideBackButton(isHide: Bool) {
        socialDelegate?.hideBackButton!(isHide)
    }
    func showActivity(title: String) {
        socialDelegate?.showActivity!(title)
    }
    func removeActivity() {
        socialDelegate?.removeActivity!()
    }
    
    
    // MARK: - WebServices
    func postGetImages(params: NSArray) {
        ImageResults = [String](count: params.count, repeatedValue: "")
        for i in 0...params.count-1 {
            let ImageParams = ["PlayListId":params[i]["ID"]]
            SP_NETWORK.postJson(WEB_SERVICE + "api/PlayList/SearchPlaylistImage", method: "POST", params: ImageParams as Dictionary<String, AnyObject>, is_Dictionary: true) { (Succeeded, Json) -> () in
                dispatch_async(dispatch_get_main_queue(), { () -> Void in
                    print(Succeeded)
                    print(Json)
                    //self.socialDelegate?.removeActivity!()
                    if Succeeded == true {
                        let image = Json["ResultSet"] as? NSDictionary
                        
                        if(Json["Status"]as! String != "0") {
                            
                            let image = image!["BgImage"] as? String
                            
                            if(image != "") {
                                self.ImageResults[i] = image!
                            }
                            self.PlayListCollection.reloadData()
                        }
                    }
                    
                })
            }
        }
    }
    func postPlayList(params: NSDictionary) {
        socialDelegate?.showActivity!("Loading..")
        SP_NETWORK.postJson(WEB_SERVICE + "api/PlayList/SearchAllPlayList", method: "POST", params: params as! Dictionary<String, AnyObject>, is_Dictionary: true) { (Succeeded, Json) -> () in
            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                print(Succeeded)
                print(Json)
                self.socialDelegate?.removeActivity!()
                if Succeeded == true {
                    if let status = Json["Status"] {
                        if status as? String == "0" {
                            SP_DELEGATE.showAlert("Message", message: "No playlist found", buttonTitle: "OK")
                        }
                    }
                    self.playListArray = Json["ResultSet"] as? NSArray
                }
                self.PlayListCollection.reloadData()
            })
        }
    }
    func postDeletePlayList(params: NSDictionary) {
        socialDelegate?.showActivity!("Deleting..")
        SP_NETWORK.postJson(WEB_SERVICE + "api/PlayList/PlayListDelete", method: "POST", params: params as! Dictionary<String, AnyObject>, is_Dictionary: true) { (Succeeded, Json) -> () in
            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                print(Succeeded)
                print(Json)
                self.socialDelegate?.removeActivity!()
                if Succeeded == true {
                    if let status = Json["Status"] {
                        if status as? String == "1" {
                            if self.is_AlertShown == true {
                                self.AlertContainer.hidden = true
                                self.is_AlertShown = false
                            }
                            
                            self.fetchPlayLists()
                        }
                    }
                }
                self.PlayListCollection.reloadData()
            })
        }
    }
    func postLikePlayList(params: NSDictionary) {
        SP_NETWORK.postJson(WEB_SERVICE + "api/PlayList/UpdateLike", method: "POST", params: params as! Dictionary<String, AnyObject>, is_Dictionary: true) { (Succeeded, Json) -> () in
            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                print(Succeeded)
                print(Json)
                if Succeeded == true {
                    
                    if let status = Json["Status"] {
                        if status as? String == "1" {
                            
                            var mainArray: NSArray!
                            if self.is_Searching == true {
                                mainArray = self.searchResults
                            }else {
                                mainArray = self.playListArray
                            }
                            let likeCount = mainArray[self.checkLiked]["LikeCount"] as! String
                            
                            if mainArray[self.checkLiked]["Like"] as? String == "Y" {
                                mainArray[self.checkLiked].setValue("N", forKey: "Like")
                                mainArray[self.checkLiked].setValue(String(Int(likeCount)! - 1), forKey: "LikeCount")
                            } else{
                                mainArray[self.checkLiked].setValue(String(Int(likeCount)! + 1), forKey: "LikeCount")
                                mainArray[self.checkLiked].setValue("Y", forKey: "Like")
                            }
                            
                            self.PlayListCollection.reloadData()
                            
                        }
                    }
                    
                }
                self.PlayListCollection.reloadData()
            })
        }
    }
    
    // MARK: - TextField Delegates
    func textFieldDidBeginEditing(textField: UITextField) {
        is_Searching = true
    }
    func textFieldDidEndEditing(textField: UITextField) {
        if SP_DELEGATE.TrimString(textField.text!).isEmpty == true {
            is_Searching = false
        }else {
            is_Searching = true
        }
        PlayListCollection.reloadData()
    }
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        if SP_DELEGATE.TrimString(textField.text!) == "" {
            is_Searching = false
        }else {
            searchFromPlayList(textField.text!)
        }
        PlayListCollection.reloadData()
        return true
    }
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        searchFromPlayList(textField.text!)
        return true
    }
    // MARK: - System
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "Songs" {
            let songs = segue.destinationViewController as! SPSongs
            songs.socialDelegate = self
            songs.playListId = passplayListId
            songs.playListName = passPlayListName
            songs.BGImageData = passBGImage
            songs.playListUserId = PlayListUserId
            songs.fav = favpass
        }else if segue.identifier == "toEditPlayList" {
            let edit = segue.destinationViewController as! SPCreatePlayList
            edit.editPlayList = passPlayList
            edit.socialDelegate = self
        }else if segue.identifier == "Comments" {
            commentController = segue.destinationViewController as! SPComments
            commentController.playListId = passplayListId
            commentController.socialDelegate = self
            commentController.keyBoardDelegate = self
            commentController.playListUserId = PlayListUserId
        }
    }
    
}
class SPPlayListCell: UICollectionViewCell {
    
    @IBOutlet weak var imageContainer: UIView!
    @IBOutlet weak var playlistIcon: UIImageView!
    @IBOutlet weak var cdRound: UIView!
    @IBOutlet weak var PlayListName: UILabel!
    @IBOutlet weak var numberOfLikes: UILabel!
    @IBOutlet weak var numberOfComments: UILabel!
    @IBOutlet weak var likeImage: UIImageView!
    @IBOutlet weak var likeButton: UIButton!
    @IBOutlet weak var commentButton: UIButton!
    
}
