//
//  SPAddSongs.swift
//  Social Parrot
//
//  Created by Abdul Malik Aman on 28/06/16.
//  Copyright © 2016 INTELLIPATTERN. All rights reserved.
//

import UIKit
import AVFoundation
import MediaPlayer

class SPAddSongs: UIViewController, AVAudioPlayerDelegate {
    
    // MARK: - Variables
    var socialDelegate: SocialParrot_Delegate?
    @IBOutlet weak var AddSongsTable: UITableView!
    @IBOutlet weak var SearchText: SPImagedTextFiled!
    var checkType = 0
    @IBOutlet var CheckImages: [UIImageView]!
    var SongsArray: NSArray!
    var iTunesUrl = "https://itunes.apple.com/"
    var socialPlayer: AVAudioPlayer!
    var timeObserver: AnyObject!
    var is_Playing = false
    var uploadImage = ""
    var fromCreate = false
    var playListId: String!
    @IBOutlet weak var SongPlaceHolder: UILabel!
    var selectedIndex: Int!
    var tempSelectedIndex: Int!
    let playImage = UIImage(named: "Songs_Play")
    let pauseImage = UIImage(named: "Songs_Pause")
    var activityLoadIndex: Int!
    // MARK: - View Methods
    
    override func viewWillDisappear(animated: Bool) {
        if(is_Playing == true)
        {
            self.socialPlayer.pause()
        }
    }
    override func viewWillAppear(animated: Bool) {
        socialDelegate?.hideBackButton!(true)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        SongPlaceHolder.adjustsFontSizeToFitWidth = true
        // Do any additional setup after loading the view.
    }
    
    // MARK: - Actions
    @IBAction func Back(sender: AnyObject) {
        if fromCreate == true {
            self.navigationController?.popToRootViewControllerAnimated(false)
//            self.tabBarController?.selectedIndex = 2
//            socialDelegate?.navigateBack!(self.navigationController!, tag: 2)
        }else {
            self.tabBarController?.selectedIndex = 2
            self.navigationController?.popViewControllerAnimated(true)
        }
        
    }
    @IBAction func Search(sender: AnyObject) {
        self.view.endEditing(true)
        
        SearchText.resignFirstResponder()
        searchSongs(SearchText.text!)
        
    }
    func searchSongs(var keyWord: String) {
        var url: String!
        keyWord = keyWord.stringByReplacingOccurrencesOfString(" ", withString: "%20")
        
        if checkType == 0 {
            url = iTunesUrl + "search?term=\(keyWord)&limit=25&media=music&entity=musicTrack&attribute=songTerm"
        }else if checkType == 1 {
            url = iTunesUrl + "search?term=\(keyWord)&limit=25&media=music&entity=musicTrack&attribute=artistTerm"
        }else {
            url = iTunesUrl + "search?term=\(keyWord)&limit=25&media=music"
        }
        
        getMusic(url)
    }
    
    @IBAction func Purchase(sender: AnyObject) {
        let url = SongsArray[sender.tag]["trackViewUrl"] as? String
        if let appURL = NSURL(string: url!) {
            let canOpen = UIApplication.sharedApplication().canOpenURL(appURL)
            print("Can open \"\(appURL)\": \(canOpen)")
            UIApplication.sharedApplication().openURL(appURL)
        }else {
            UIApplication.sharedApplication().openURL(NSURL(string: url!)!)
        }

    }
    @IBAction func AddSong(sender: AnyObject) {
        socialDelegate?.showActivity!("Adding song..")
        let title = SongsArray[sender.tag]["trackName"] as? String
        let trackId = SongsArray[sender.tag]["trackId"] as? Int
        let artistName = SongsArray[sender.tag]["artistName"] as? String
        let previewURL = SongsArray[sender.tag]["previewUrl"] as? String
        let imageURL = SongsArray[sender.tag]["artworkUrl100"] as? String
        var collectionId = 0
//        collectionId = (SongsArray[sender.tag]["collectionId"] as? Int)!
        let iTunesURL = SongsArray[sender.tag]["trackViewUrl"] as? String
        
        let params: NSDictionary = ["PlayListId": "\(playListId)", "Title": title!, "TrackId": "\(trackId!)", "ArtistName": artistName!, "ImageURL": imageURL!, "URL": previewURL!, "CollectionId":"\(collectionId)", "iTunesURL": iTunesURL!, "isStreamable":"y"]
        postAddSongs(params)
    }
    
    @IBAction func Play(sender: AnyObject) {
        playSong(sender.tag)
    }
    func playSong(playIndex: Int) {
        activityLoadIndex = playIndex
        let musicUrl = SongsArray[playIndex]["previewUrl"] as? String
        
        dispatch_async(dispatch_get_main_queue(), { () -> Void in
            if self.is_Playing == false {
                self.selectedIndex = playIndex
                self.tempSelectedIndex = playIndex
                do {
                    let fileURL = NSURL(string:musicUrl!)
                    let soundData = NSData(contentsOfURL:fileURL!)
                    self.socialPlayer = try AVAudioPlayer(data: soundData!)
                    self.socialPlayer.prepareToPlay()
                    self.socialPlayer.volume = 1.0
                    self.socialPlayer.delegate = self
                    self.activityLoadIndex = nil
                    self.socialPlayer.play()
                    self.is_Playing = true
                    print("Playing")
                } catch {
                    print("Error getting the audio file")
                }
            }else {
                if self.tempSelectedIndex == playIndex {
                    self.selectedIndex = nil
                    self.activityLoadIndex = nil
                    self.is_Playing = false
                    self.socialPlayer.pause()
                    print("Paused")
                }else {
                    self.is_Playing = false
                     self.activityLoadIndex = nil
                    self.playSong(playIndex)
                    print("Stopped")
                }
            }
            self.AddSongsTable.reloadData()
        })
    }
    @IBAction func CheckServiceType(sender: AnyObject) {
        checkType = sender.tag
        for i in 0...2 {
            CheckImages[i].image = UIImage(named: "untick")
            if checkType == i {
                CheckImages[i].image = UIImage(named: "tick")
            }
        }
        
    }
    // MARK: - TableView Delegate Methods
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if SongsArray != nil {
            return SongsArray.count
        }else {
            return 0
        }
    }
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
        
    }
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = AddSongsTable.dequeueReusableCellWithIdentifier("AddSongs", forIndexPath: indexPath) as! SPSongsCell
        
        cell.SerialNum.text = "\(indexPath.row + 1)"
        cell.SongName.text = SongsArray[indexPath.row]["trackName"] as? String
        cell.SongArtist.text = SongsArray[indexPath.row]["artistName"] as? String
        cell.SongImage.tag = indexPath.row
        cell.PlayButton.tag = indexPath.row
        cell.addSongsButton.tag = indexPath.row
        cell.ActLoad.tag = indexPath.row
        
        if let url = NSURL(string: SongsArray[indexPath.row]["artworkUrl30"] as! String) {
            cell.SongImage.sd_setImageWithURL(url, placeholderImage: UIImage(named: "playlist"))
        }
        
        if self.activityLoadIndex != nil {
            if self.activityLoadIndex == indexPath.row {
                cell.ActLoad.startAnimating()
                cell.ActLoad.hidden = false
            }else {
                cell.ActLoad.stopAnimating()
                cell.ActLoad.hidden = true
            }
        }else {
            cell.ActLoad.stopAnimating()
            cell.ActLoad.hidden = true
        }
        
        if selectedIndex != nil {
            if selectedIndex == indexPath.row {
                cell.playingImage.image = pauseImage
            }else {
                cell.playingImage.image = playImage
            }
        }else {
            cell.playingImage.image = playImage
        }
        return cell
    }
    
    // MARK: - Audio Player Delegate
    func audioPlayerDidFinishPlaying(player: AVAudioPlayer, successfully flag: Bool) {
        if is_Playing == true {
            selectedIndex = nil
            is_Playing = false
            socialPlayer.stop()
            print("Stopped")
        }
        AddSongsTable.reloadData()
    }
    // MARK: - TextField Delegates
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        searchSongs(SP_DELEGATE.TrimString(textField.text!))
        return true
    }
    
    // MARK: - Custom Delegates
    func hideLogoutButton(isHide: Bool) {
        socialDelegate?.hideLogoutButton!(isHide)
    }
    func navigateTab(tab: Int) {
        socialDelegate?.navigateTab!(tab)
    }
    func hideBackButton(isHide: Bool) {
        socialDelegate?.hideBackButton!(isHide)
    }
    func showActivity(title: String) {
        socialDelegate?.showActivity!(title)
    }
    func removeActivity() {
        socialDelegate?.removeActivity!()
    }
    // MARK: - WebServices
    func getMusic(url: String) {
        socialDelegate?.showActivity!("Loading..")
        SP_NETWORK.getJson(url, method: "GET") { (Succeeded, Json) -> () in
            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                print(Succeeded)
                print(Json)
                self.socialDelegate?.removeActivity!()
                if Succeeded == true {
                    self.SongsArray = Json["results"] as? NSArray
                    
                }else {
                    self.SongsArray = nil
                }
                self.AddSongsTable.reloadData()
            })
        }
    }
    func postAddSongs(params: NSDictionary) {
        SP_NETWORK.postJson(WEB_SERVICE + "api/PlayList/AddSong", method: "POST", params: params as! Dictionary<String, AnyObject>, is_Dictionary: true) { (Succeeded, Json) -> () in
            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                print(Succeeded)
                print(Json)
                self.socialDelegate?.removeActivity!()
                if Succeeded == true {
                    if let status = Json["Status"] {
                        if status as? String == "1" {
                            
                            let alertsong = UIAlertController(title: nil, message: "Song is added", preferredStyle: .Alert)
                            self.presentViewController(alertsong, animated: true, completion: nil)
                            [NSThread .sleepForTimeInterval(3)]
                            
                            alertsong.dismissViewControllerAnimated(true, completion: nil)
                            
                            
                        }
                    }
                }
                self.AddSongsTable.reloadData()
            })
        }
    }
    
    // MARK: - System
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
