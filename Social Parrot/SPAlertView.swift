//
//  SPAlertView.swift
//  Social Parrot
//
//  Created by Abdul Malik Aman on 10/06/16.
//  Copyright © 2016 INTELLIPATTERN. All rights reserved.
//

import UIKit

class SPAlertView: UIView {

    @IBInspectable var Boarder_Colour: UIColor = UIColor.clearColor()
    @IBInspectable var Boarder_Width: CGFloat = 1.0
    @IBInspectable var Corner_Radius: CGFloat = 1.0
    
    override func drawRect(rect: CGRect) {
        
        self.layer.borderColor = Boarder_Colour.CGColor
        self.layer.borderWidth = Boarder_Width
        self.layer.cornerRadius = Corner_Radius
        
    }

}
