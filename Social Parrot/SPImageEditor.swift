//
//  SPImageEditor.swift
//  Social Parrot
//
//  Created by Abdul Malik Aman on 26/06/16.
//  Copyright © 2016 INTELLIPATTERN. All rights reserved.
//

import UIKit

protocol imageEditiDelegate {
    func passEditedImage(image: UIImage)
}
class SPImageEditor: UIViewController {

    // MARK: - Variables
    var delegate: imageEditiDelegate?
    var socialDelegate: SocialParrot_Delegate?
    @IBOutlet weak var imageScroll: UIScrollView!
    @IBOutlet weak var editedImage: UIImageView!
    var imageToEdit: UIImage!
    var cropedImage: UIImage!
    
    // MARK: - View Methods
    override func viewWillDisappear(animated: Bool) {
        socialDelegate?.hideLogoutButton!(false)
    }
    override func viewWillAppear(animated: Bool) {
        socialDelegate?.hideLogoutButton!(true)
    }
    override func viewDidLoad() {
        super.viewDidLoad()

        editedImage.image = imageToEdit
        
        // Do any additional setup after loading the view.
    }
    
    
    // MARK: - Actions
    @IBAction func Back(sender: AnyObject) {
        self.navigationController?.popViewControllerAnimated(true)
    }
    @IBAction func CropImage(sender: AnyObject) {
        let scale: CGFloat = 1.0 / imageScroll.zoomScale
        var visibleRect = CGRect()
        visibleRect.origin.x = imageScroll.contentOffset.x * scale
        visibleRect.origin.y = imageScroll.contentOffset.y * scale
        visibleRect.size.width = 300 * scale // imageScroll.bounds.size.width * scale
        visibleRect.size.height = 300 * scale //imageScroll.bounds.size.height * scale
        cropImage(imageToEdit, rect: visibleRect)
        self.navigationController?.popViewControllerAnimated(true)
    }
    func cropImage(srcImage: UIImage, rect: CGRect) -> UIImage {
        let cr: CGImageRef = CGImageCreateWithImageInRect(srcImage.CGImage, rect)!
        let cropped: UIImage = UIImage(CGImage: cr)
        delegate?.passEditedImage(srcImage)
        return cropped;
    }
    
    // MARK: - ScrollView Delegates
    func viewForZoomingInScrollView(scrollView: UIScrollView) -> UIView? {
        return editedImage
    }
    
    // MARK: - System
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
