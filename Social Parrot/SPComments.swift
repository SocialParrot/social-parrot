//
//  SPComments.swift
//  Social Parrot
//
//  Created by Abdul Malik Aman on 03/07/16.
//  Copyright © 2016 INTELLIPATTERN. All rights reserved.
//

import UIKit

protocol keyBoardUpDelegate {
    func commentkeyBoardUp(isUp: Bool)
}
class SPComments: UIViewController, UIGestureRecognizerDelegate, FloatRatingViewDelegate {
    
    // MARK: - Variables
    @IBOutlet weak var SendButton: UIButton!
    @IBOutlet weak var CommentCount: UILabel!
    @IBOutlet weak var LikesCount: UILabel!
    @IBOutlet weak var RatingView: FloatRatingView!
    @IBOutlet weak var CommentText: UITextView!
    @IBOutlet weak var favouriteImage: UIImageView!
    @IBOutlet weak var CommentTable: UITableView!
    var keyBoardDelegate: keyBoardUpDelegate?
    var socialDelegate: SocialParrot_Delegate?
    var is_Favourite = false
    var playListId: String!
    var playListUserId: String!
    var commentArray: NSArray!
    var is_AlertShown = false
    var selectedComment: Int!
    var CommentId: String!
    var CommentTxt: String!
    
    // MARK: - View Methods
    override func viewWillAppear(animated: Bool) {
        
        CommentId = ""
        
        if playListUserId == nil {
            playListUserId = USER_ID
        }
        if playListId != nil {
            let params = ["PlayListId": playListId, "UserId": playListUserId, "CurrentUser": USER_ID!]
            postGetAllComments(params)
        }
        
        RatingView.delegate = self
    }
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    
    
    // MARK: - Actions
    @IBAction func Favourite(sender: AnyObject) {
        self.view.endEditing(true)
        var params: NSDictionary!
        if is_Favourite == false {
            params = ["PlayListId": playListId, "RatingUserId": USER_ID!, "Favorite": "Y", "CurrentUser": USER_ID!]
        }else {
            params = ["PlayListId": playListId, "RatingUserId": USER_ID!, "Favorite": "N", "CurrentUser": USER_ID!]
        }
        
        postFavouritePlayList(params)
    }
    @IBAction func PostComment(sender: AnyObject) {
        self.view.endEditing(true)
        if SP_DELEGATE.TrimString(CommentText.text!) != "" {
            if CommentId != "" {
                let params = ["PlaylistId": playListId, "UserId": USER_ID!, "CommentText": CommentText.text!, "ID":CommentId]
                postComments(params)
            } else {
            let params = ["PlaylistId": playListId, "UserId": USER_ID!, "CommentText": CommentText.text!]
                postComments(params)
            }
        }else {
            SP_DELEGATE.showAlert("Message", message: "Comment Cannot be empty", buttonTitle: "OK")
        }
    }
    
    
    func scrollToTop(row: Int, section: Int, table: UITableView) {
        let indexPath = NSIndexPath(forRow: row, inSection: section)
        table.scrollToRowAtIndexPath(indexPath, atScrollPosition: .Top, animated: true)
    }
    
    
    // MARK: - TableView Delegate Methods
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if commentArray != nil {
            return commentArray.count
        }else {
            return 0
        }
    }
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var identifier: String!
        if commentArray[indexPath.row]["Self"] as? String != "Y" {
            identifier = "Opponent"
        }else {
            identifier = "Me"
        }
        let cell = CommentTable.dequeueReusableCellWithIdentifier(identifier, forIndexPath: indexPath) as! SPCommentCell
        
        cell.Comment.text = commentArray[indexPath.row]["CommentText"] as? String
        cell.Time.text = commentArray[indexPath.row]["Time"] as? String
        cell.userName.text = commentArray[indexPath.row]["NickName"] as? String
        
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        if USER_ID == commentArray[indexPath.row]["UserId"] as? String {
            CommentId = commentArray[indexPath.row]["ID"] as? String
            CommentTxt = commentArray[indexPath.row]["CommentText"] as? String
            
            let alert = UIAlertController()
            alert.addAction(UIAlertAction(title: "Edit", style: UIAlertActionStyle.Default, handler: { action in
                self.EditAction()
            }))
            alert.addAction(UIAlertAction(title: "Delete", style: UIAlertActionStyle.Default, handler: { action in
                self.DeleteAction()
            }))
            alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.Cancel, handler: nil))
            self.presentViewController(alert, animated: true, completion: nil)
        }
    }
    
    
    func EditAction() {
        CommentText.text = CommentTxt;
    }
    func DeleteAction() {
        let params = ["ID": CommentId]
        postDeleteComment(params)
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    func tableView(tableView: UITableView, estimatedHeightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    
    
    func postDeleteComment(params: NSDictionary) {
        socialDelegate?.showActivity!("Deleting..")
        SP_NETWORK.postJson(WEB_SERVICE + "api/PlayList/CommentDelete", method: "POST", params: params as! Dictionary<String, AnyObject>, is_Dictionary: true) { (Succeeded, Json) -> () in
            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                print(Succeeded)
                print(Json)
                self.socialDelegate?.removeActivity!()
                if Succeeded == true {
                    if let status = Json["Status"] {
                        if status as? String == "1" {
                            self.CommentId = ""
                            let params = ["PlayListId": self.playListId, "UserId": self.playListUserId, "CurrentUser": USER_ID!]
                            self.postGetAllComments(params)
                        }
                    }
                }
            })
        }
    }
    
    
    // MARK: - WEB SERVICES
    func postFavouritePlayList(params: NSDictionary) {
        socialDelegate?.showActivity!("Loading..")
        SP_NETWORK.postJson(WEB_SERVICE + "api/PlayList/MakeFavorite", method: "POST", params: params as! Dictionary<String, AnyObject>, is_Dictionary: true) { (Succeeded, Json) -> () in
            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                print(Succeeded)
                print(Json)
                self.socialDelegate?.removeActivity!()
                if Succeeded == true {
                    if params["Favorite"] as? String == "Y" {
                        self.favouriteImage.image = UIImage(named: "menu_Favourite_Sel")
                        self.is_Favourite = true
                    }else {
                        self.favouriteImage.image = UIImage(named: "menu_Favourite")
                        self.is_Favourite = false
                    }
                    
                }
            })
        }
    }
    
    func postGetAllComments(params: NSDictionary) {
        socialDelegate?.showActivity!("Loading..")
        SP_NETWORK.postJson(WEB_SERVICE + "api/PlayList/CommentsGetAll", method: "POST", params: params as! Dictionary<String, AnyObject>, is_Dictionary: true) { (Succeeded, Json) -> () in
            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                print(Succeeded)
                print(Json)
                self.socialDelegate?.removeActivity!()
                if Succeeded == true {
                    if let status = Json["Status"] {
                        if status as? String == "1" {
                            if let comments = Json["ResultSet"]!["Comments"] {
                                self.commentArray = comments as? NSArray
                            }
                            if let is_favourite = Json["ResultSet"]!["Favorite"] {
                                if is_favourite as? String == "Y" {
                                    self.is_Favourite = true
                                    self.favouriteImage.image = UIImage(named: "menu_Favourite_Sel")
                                }else {
                                    self.is_Favourite = false
                                    self.favouriteImage.image = UIImage(named: "menu_Favourite")
                                }
                                
                            }
                            if let is_Liked = Json["ResultSet"]!["Liked"] {
                                if is_Liked as? String == "0" {
                                    self.LikesCount.text = (is_Liked as? String)! + " Likes"
                                }else {
                                    self.LikesCount.text = "0 Likes"
                                }
                            }
                            if let is_Liked = Json["ResultSet"]!["Rate"] {
                                if is_Liked as? String != "" {
                                    self.RatingView.rating = Float((is_Liked as? String)!)!
                                }else {
                                    self.RatingView.rating = 0.0
                                }
                            }
                        }
                    }
                }
                self.CommentTable.reloadData()
                if self.commentArray != nil {
                    self.CommentCount.text = "\(self.commentArray.count) Comments"
                    self.scrollToTop(self.commentArray.count-1, section: 0, table: self.CommentTable)
                }else {
                    self.CommentCount.text = "0 Comments"
                }
            })
        }
    }
    func postComments(params: NSDictionary) {
        self.SendButton.userInteractionEnabled = false
        SP_NETWORK.postJson(WEB_SERVICE + "api/PlayList/UpdateComments", method: "POST", params: params as! Dictionary<String, AnyObject>, is_Dictionary: true) { (Succeeded, Json) -> () in
            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                print(Succeeded)
                print(Json)
                self.SendButton.userInteractionEnabled = true
                if Succeeded == true {
                    if let status = Json["Status"] {
                        if status as? String == "1" {
                            self.CommentText.text = ""
                            let params = ["PlayListId": self.playListId, "UserId": self.playListUserId, "CurrentUser": USER_ID!]
                            self.postGetAllComments(params)
                        }else {
                            SP_DELEGATE.showAlert("Message", message: "Failed to post Comment \n Please Try again", buttonTitle: "OK")
                        }
                        self.CommentId = ""
                    }
                }
                self.CommentTable.reloadData()
            })
        }
    }
    func postRatingPlayList(params: NSDictionary) {
        SP_NETWORK.postJson(WEB_SERVICE + "api/PlayList/UpdateRate", method: "POST", params: params as! Dictionary<String, AnyObject>, is_Dictionary: true) { (Succeeded, Json) -> () in
            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                print(Succeeded)
                print(Json)
                if Succeeded == true {
                    //                    if let status = Json["Status"] {
                    //
                    //                    }
                }
            })
        }
    }
    
    // MARK: - TextView Delegates
    func textViewDidBeginEditing(textView: UITextView) {
        keyBoardDelegate?.commentkeyBoardUp(true)
    }
    func textViewDidEndEditing(textView: UITextView) {
        keyBoardDelegate?.commentkeyBoardUp(false)
    }
    func textView(textView: UITextView, shouldChangeTextInRange range: NSRange, replacementText text: String) -> Bool {
        if(text == "\n") {
            textView.resignFirstResponder()
            return false
        }
        return true
    }
    
    // MARK: - Custom Rating
    func floatRatingView(ratingView: FloatRatingView, isUpdating rating:Float) {
        self.view.endEditing(true)
        let params = ["PlayListId": playListId, "RatingUserId": USER_ID!, "Rate": "\(rating)"]
        postRatingPlayList(params)
    }
    
    func floatRatingView(ratingView: FloatRatingView, didUpdate rating: Float) {
        // To get last rating
    }
    
    // MARK: - System
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    // Get the new view controller using segue.destinationViewController.
    // Pass the selected object to the new view controller.
    }
    */
    
}

class SPCommentCell: UITableViewCell {
    
    @IBOutlet weak var Time: UILabel!
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var Comment: UILabel!
}
