//
//  SPCustomDelegates.swift
//  Social Parrot
//
//  Created by Abdul Malik Aman on 11/06/16.
//  Copyright © 2016 INTELLIPATTERN. All rights reserved.
//

import UIKit

class SPCustomDelegates: NSObject {
    // MARK: - Variables
    var loadingView: UIView = UIView()
    var actView: UIView = UIView()
    var titleLabel: UILabel = UILabel()
    var activityIndicator: UIActivityIndicatorView = UIActivityIndicatorView()
    
    // MARK: - Delegates
    func CustomDateFromString(myTime: String, format: String) -> NSDate {
        let dateString = myTime
        SP_DATE_FORMATTER.dateFormat = format
        let dateFromString = SP_DATE_FORMATTER.dateFromString(dateString)
        return dateFromString!
    }
    func showActivity(myView: UIView, myTitle: String) {
        myView.userInteractionEnabled = false
        myView.window?.userInteractionEnabled = false
        myView.endEditing(true)
        actView.frame = CGRectMake(0, 0, myView.frame.width, myView.frame.height)
        actView.center = myView.center
        actView.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.3)
        
        loadingView.frame = CGRectMake(0, 0, 80, 80)
        loadingView.center = myView.center
        loadingView.backgroundColor = UIColor.clearColor() //THEME_COLOUR
        loadingView.clipsToBounds = true
        loadingView.layer.cornerRadius = 15
        
        activityIndicator.frame = CGRectMake(0.0, 0.0, 40.0, 40.0);
        activityIndicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.Gray
        activityIndicator.center = CGPointMake(loadingView.frame.size.width / 2, loadingView.frame.size.height / 2);
        activityIndicator.color = THEME_COLOUR
        
        titleLabel.frame = CGRectMake(5, loadingView.frame.height-20, loadingView.frame.width-10, 20)
        titleLabel.textColor = UIColor.whiteColor()
        titleLabel.adjustsFontSizeToFitWidth = true
        titleLabel.textAlignment = NSTextAlignment.Center
        titleLabel.text = "" // myTitle
        titleLabel.font = UIFont(name: FONT_NAME_R, size: 10)
        
        loadingView.addSubview(activityIndicator)
        actView.addSubview(loadingView)
        loadingView.addSubview(titleLabel)
        myView.addSubview(actView)
        activityIndicator.startAnimating()
    }
    func removeActivity(myView: UIView) {
        myView.userInteractionEnabled = true
        myView.window?.userInteractionEnabled = true
        activityIndicator.stopAnimating()
        actView.removeFromSuperview()
    }
    func showAlert(title: String, message: String, buttonTitle: String) {
        let AM_Alert = UIAlertView()
        AM_Alert.title = title
        AM_Alert.addButtonWithTitle(buttonTitle)
        AM_Alert.message = message
        AM_Alert.show()
    }
    func TrimString(trimString: String)-> String {
        let whitespace: NSCharacterSet = NSCharacterSet.whitespaceAndNewlineCharacterSet()
        let trimmed = trimString.stringByTrimmingCharactersInSet(whitespace)
        return trimmed
    }
    func TrimNumbers(trimString: String)-> Bool {
        let whitespace: NSCharacterSet = NSCharacterSet.whitespaceAndNewlineCharacterSet()
        let disallowedCharacterSet = NSCharacterSet(charactersInString: "0123456789").invertedSet
        let trimmed = trimString.stringByTrimmingCharactersInSet(whitespace)
        let replacementStringIsLegal = trimmed.rangeOfCharacterFromSet(disallowedCharacterSet) == nil
        if replacementStringIsLegal == true {
            return true
        }else {
            return false
        }
    }
    func isValidEmail(testStr:String) -> Bool {
        let emailRegEx = "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluateWithObject(testStr)
    }
    func checkNullToNil(value : AnyObject?) -> AnyObject? {
        if value is NSNull {
            return nil
        } else {
            return value
        }
    }
    func attributed(attrString: NSAttributedString, isBold: Bool, underLine: Int,fontsize: CGFloat, fontColor: UIColor) -> NSMutableAttributedString {
        let mutableAttrString: NSMutableAttributedString = NSMutableAttributedString(attributedString: attrString)
        let headerStart: Int = 0
        let headerEnd: Int = attrString.length
        
        if isBold == true {
            mutableAttrString.addAttribute(NSFontAttributeName, value: UIFont.boldSystemFontOfSize(fontsize), range: NSMakeRange(headerStart, headerEnd))
        }else {
            mutableAttrString.addAttribute(NSFontAttributeName, value: UIFont.systemFontOfSize(fontsize), range: NSMakeRange(headerStart, headerEnd))
        }
        if underLine != 0 {
            mutableAttrString.addAttribute(NSUnderlineStyleAttributeName, value: underLine, range: NSMakeRange(headerStart, headerEnd))
        }
        
        mutableAttrString.addAttribute(NSForegroundColorAttributeName, value: fontColor, range: NSMakeRange(headerStart, headerEnd))
        
        return mutableAttrString
    }
    func ShakeAnimation(animateView: UIView) {
        //        if VIBRATE == true {
        //            AudioServicesPlayAlertSound(SystemSoundID(kSystemSoundID_Vibrate))
        //        }
        let animation = CABasicAnimation(keyPath: "position")
        animation.duration = 0.04
        animation.repeatCount = 2
        animation.autoreverses = true
        animation.fromValue = NSValue(CGPoint: CGPointMake(animateView.center.x - 10, animateView.center.y))
        animation.toValue = NSValue(CGPoint: CGPointMake(animateView.center.x + 10, animateView.center.y))
        animateView.layer.addAnimation(animation, forKey: "position")
    }
    func encodeImage(image: UIImage) -> String {
        let imageData:NSData = UIImageJPEGRepresentation(image, 0.5)!
        let encodedImage: String = imageData.base64EncodedStringWithOptions(.Encoding64CharacterLineLength)
//        print(encodedImage)
        return encodedImage
    }
    func decodeImage(imageString: String) -> UIImage {
        let dataDecoded:NSData = NSData(base64EncodedString: imageString, options: NSDataBase64DecodingOptions.IgnoreUnknownCharacters)!
        return UIImage(data: dataDecoded)!
    }
    func storeImage(image: UIImage, imageName: String) -> String {
        let paths = NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true)[0] as String
        let filePathToWrite = "\(paths)/\(imageName)"
        print(filePathToWrite)
        let imageData: NSData = UIImageJPEGRepresentation(image, 1.0)!
        FILE_MANAGER.createFileAtPath(filePathToWrite, contents: imageData, attributes: nil)
        let imagePath = paths + "/\(imageName)"
        return imagePath
    }
    func loadImageFromPath(path: String) -> UIImage? {
        let image = UIImage(contentsOfFile: path)
        if image == nil {
            print("missing image at: \(path)")
        }
        return image
    }
    func removeFileFromPath(fileName: String) {
        do {
            try FILE_MANAGER.removeItemAtPath(fileName)
        }
        catch let error as NSError {
            print("Ooops! Something went wrong: \(error)")
        }
    }
}
