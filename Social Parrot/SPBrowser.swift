//
//  SPBrowser.swift
//  Social Parrot
//
//  Created by Abdul Malik Aman on 11/06/16.
//  Copyright © 2016 INTELLIPATTERN. All rights reserved.
//

import UIKit

class SPBrowser: UIViewController {

    // MARK: - Variables
    @IBOutlet weak var Browser: UIWebView!
    @IBOutlet weak var TitleLabel: UILabel!
    var loadURL: String!
    var pageTitle: String!
    
    // MARK: - View Methods
    override func viewDidLoad() {
        super.viewDidLoad()

        TitleLabel.text = pageTitle
        TitleLabel.adjustsFontSizeToFitWidth = true
        
        let url = NSURL (string: loadURL);
        let requestObj = NSURLRequest(URL: url!);
        Browser.loadRequest(requestObj);
        // Do any additional setup after loading the view.
    }
    // MARK: - Actions
    @IBAction func Back(sender: AnyObject) {
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    // MARK: - WebView Delegates
    func webViewDidStartLoad(webView: UIWebView) {
        SP_DELEGATE.showActivity(self.view, myTitle: "Loading..")
    }
    func webViewDidFinishLoad(webView: UIWebView) {
        SP_DELEGATE.removeActivity(self.view)
    }
    func webView(webView: UIWebView, didFailLoadWithError error: NSError?) {
        SP_DELEGATE.removeActivity(self.view)
        SP_DELEGATE.showAlert("Message", message: "Failed to load request", buttonTitle: "OK")
    }

    // MARK: - System
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
