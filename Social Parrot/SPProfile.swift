//
//  SPProfile.swift
//  Social Parrot
//
//  Created by Abdul Malik Aman on 18/06/16.
//  Copyright © 2016 INTELLIPATTERN. All rights reserved.
//

import UIKit

class SPProfile: UIViewController, SocialParrot_Delegate {
    
    // MARK: - Variables
    var socialDelegate: SocialParrot_Delegate?
    @IBOutlet weak var ProfileTable: UITableView!
    @IBOutlet weak var ProfilePic: UIImageView!
    @IBOutlet weak var UserName: UILabel!
    @IBOutlet weak var followButton: UIButton!
    @IBOutlet weak var profileBg: UIView!
    @IBOutlet weak var BackButton: SPRoundButton!
    
    var is_myProfile = true
    var is_Follow = false
    var is_PageLoaded = false
    var titleArray: [String] = ["Team", "Position", "Jersy No."]
    let titleImage: [String] = ["reg_addteam", "reg_position", "reg_jersy"]
    var ValueArray: [String] = ["", "", ""]
    var Description: String = ""
    var FacebookProfile: String!
    var TwitterProfile: String!
    var InstagramProfile: String!
    var Email: String!
    var ImageString: String!
    var Jersy: String!
    var NickName: String!
    var Message: String!
    var FirstName: String!
    var LastName: String!
    var Following: String = ""
    var PlayList: String = ""
    var Follower: String = ""
    var passType: Int!
    var profileUserId: String!
    var ImageResults: NSDictionary!
    
    // MARK: - View Methods
    override func viewWillAppear(animated: Bool) {
        if profileUserId != nil {
            is_myProfile = false
            BackButton.hidden = true
            let params: NSDictionary = ["currentUserId": USER_ID!, "UserId": profileUserId!, "Email" : ""]
            postUserProfile(params)
            socialDelegate?.hideBackButton!(false)
        }else {
            is_myProfile = true
            BackButton.hidden = true
            socialDelegate?.hideBackButton!(false)
            if USER_ID != nil {
                let params: NSDictionary = ["currentUserId": USER_ID!, "UserId": USER_ID!,"Email" : EMAIL_ID!]
                postUserProfile(params)
            }else {
                let params: NSDictionary = ["currentUserId": "", "UserId": "","Email" : EMAIL_ID!]
                postUserProfile(params)
            }
        }
        
        if is_myProfile == true {
            followButton.hidden = true
        }else if profileUserId == USER_ID {
            followButton.hidden = true
        }else {
            followButton.hidden = false
        }

    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.navigationBarHidden = true
        socialDelegate?.hideBackButton!(false)
        self.tabBarController?.tabBar.hidden = true
        ProfilePic.layer.cornerRadius = 45
        ProfilePic.layer.masksToBounds = true
        //        self.ProfileTable.estimatedRowHeight = 500.0
        //        let params: NSDictionary = ["UserId": "","Email" : EMAIL_ID!]
        //        postUserProfile(params)
        
        
        self.profileBg.backgroundColor = UIColor(patternImage: UIImage(named: "profilebackground.png")!)
        
        // Do any additional setup after loading the view.
    }
    
    // MARK: - Actions
    
    @IBAction func Back(sender: AnyObject) {
        self.navigationController?.popViewControllerAnimated(true)
    }
    @IBAction func SocialNetworks(sender: AnyObject) {
        var URL: String!
        if sender.tag == 0 {
            URL = FacebookProfile
        }else if sender.tag == 1 {
            URL = TwitterProfile
        }else {
            URL = InstagramProfile
        }
        if URL != nil {
            if URL != "" {
                if let appURL = NSURL(string: URL) {
                    let canOpen = UIApplication.sharedApplication().canOpenURL(appURL)
                    print("Can open \"\(appURL)\": \(canOpen)")
                    UIApplication.sharedApplication().openURL(appURL)
                }else {
                    UIApplication.sharedApplication().openURL(NSURL(string: URL)!)
                }
            }else{
                SP_DELEGATE.showAlert("Message", message: "Profile not found !", buttonTitle: "OK")
            }
        }else{
            SP_DELEGATE.showAlert("Message", message: "Profile not found !", buttonTitle: "OK")
        }
        
    }
    
    @IBAction func Following(sender: AnyObject) {
        passType = 1
        performSegueWithIdentifier("Following", sender: self)
        
    }
    @IBAction func PlayList(sender: AnyObject) {
        if profileUserId == USER_ID {
            socialDelegate?.navigateTab!(2)
        }else if profileUserId == nil {
            socialDelegate?.navigateTab!(2)
        }else {
            let playList = storyboard?.instantiateViewControllerWithIdentifier("SPPlayList") as? SPPlayList
            //        self.tabBarController?.selectedIndex = 2
            if profileUserId != nil {
                playList?.PlayListUserId = profileUserId
            }else {
                playList?.PlayListUserId = USER_ID!
            }
            playList?.socialDelegate = self
            showViewController(playList!, sender: self)
        }
        
    }
    @IBAction func Follower(sender: AnyObject) {
        passType = 2
        performSegueWithIdentifier("Following", sender: self)
        
    }
    
    @IBAction func Follow(sender: AnyObject) {
        var params: NSDictionary!
        if is_Follow == false {
            params = ["FollowerId": USER_ID!, "FollowingId": profileUserId, "Follow": "Y"]
        }else {
            params = ["FollowerId": USER_ID!, "FollowingId": profileUserId, "Follow": "N"]
        }
        postFollowUser(params)
    }
    func updateFollowButton(is_follow: Bool) {
        if is_follow == false {
            self.followButton.setTitle("Unfollow", forState: UIControlState.Normal)
            self.followButton.setBackgroundImage(UIImage(named: "unfollowButton.png"), forState: .Normal)
            self.is_Follow = true
        }else {
            
            self.followButton.setTitle("Follow", forState: UIControlState.Normal)
            self.followButton.setBackgroundImage(UIImage(named: "followButton.png"), forState: .Normal)
            self.is_Follow = false
        }
    }
    func SaveImage(image: UIImage) {
        if PROFILE_PICTURE != "" {
            SP_DELEGATE.removeFileFromPath("ProfilePicture")
        }
        if self.profileUserId == nil {
            PROFILE_PICTURE = SP_DELEGATE.storeImage(image, imageName: "ProfilePicture")
            SP_DEFAULTS.setValue(PROFILE_PICTURE, forKey: "ProfilePic")
        }
    }
    
    // MARK: - TableView Delegate Methods
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if is_myProfile == true {
            if section == 1 {
                return 3
            }else {
                return 1
            }
        }else {
            return 1
        }
    }
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        if is_PageLoaded == true {
            if is_myProfile == true {
                return 3
            }else {
                return 2
            }
        }else {
            return 0
        }
        
    }
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var identifier: String!
        var textViewSection : Int!
        if indexPath.section == 0 {
            identifier = "Following"
        }else if indexPath.section == 1 {
            if is_myProfile == true {
                
                identifier = "Info"
            }else {
                textViewSection = 1
                identifier = "TextView"
            }
        }else {
            textViewSection = 2
            identifier = "TextView"
        }
        let cell = ProfileTable.dequeueReusableCellWithIdentifier(identifier, forIndexPath: indexPath) as! SPProfileCell
        if indexPath.section == 0 {
            var following: NSMutableAttributedString!
            var follower: NSMutableAttributedString!
            var playList: NSMutableAttributedString!
            let next = SP_DELEGATE.attributed(NSAttributedString(string: "\n"), isBold: false, underLine: 0, fontsize: 14, fontColor: THEME_COLOUR)
            let FollowingTitle = SP_DELEGATE.attributed(NSAttributedString(string: "FOLLOWING"), isBold: false, underLine: 0, fontsize: 10, fontColor: THEME_COLOUR)
            
            let FollowerTitle = SP_DELEGATE.attributed(NSAttributedString(string: "PLAYLIST"), isBold: false, underLine: 0, fontsize: 10, fontColor: THEME_COLOUR)
            
            let PlayListTitle = SP_DELEGATE.attributed(NSAttributedString(string: "FOLLOWERS"), isBold: false, underLine: 0, fontsize: 10, fontColor: THEME_COLOUR)
            
            let Following = SP_DELEGATE.attributed(NSAttributedString(string: self.Following), isBold: false, underLine: 0, fontsize: 17.5, fontColor: THEME_COLOUR)
            
            let Follower = SP_DELEGATE.attributed(NSAttributedString(string: self.PlayList), isBold: false, underLine: 0, fontsize: 17.5, fontColor: THEME_COLOUR)
            
            
            let PlayList = SP_DELEGATE.attributed(NSAttributedString(string: self.Follower), isBold: false, underLine: 0, fontsize: 17.5, fontColor: THEME_COLOUR)
            
            
            
            following = Following
            following.appendAttributedString(next)
            following.appendAttributedString(FollowingTitle)
            follower = Follower
            follower.appendAttributedString(next)
            follower.appendAttributedString(FollowerTitle)
            playList = PlayList
            playList.appendAttributedString(next)
            playList.appendAttributedString(PlayListTitle)
            cell.Following.attributedText = following
            cell.PlayList.attributedText = follower
            cell.Followers.attributedText = playList
        }else if indexPath.section == 1 {
            if is_myProfile == true {
                cell.infoLabel.text = titleArray[indexPath.row]
                cell.infoImage.image = UIImage(named: titleImage[indexPath.row])
                cell.infoDetails.text = ValueArray[indexPath.row]
            }else {
                
                // cell.ProfileDescription.textAlignment=NSTextAlignment.Justified
                
                let sampleText = Description
                
                let paragraphStyle = NSMutableParagraphStyle()
                paragraphStyle.alignment = NSTextAlignment.Justified
                
                let attributedString = NSAttributedString(string: sampleText,
                    attributes: [
                        NSParagraphStyleAttributeName: paragraphStyle,
                        NSBaselineOffsetAttributeName: NSNumber(float: 0)
                    ])
                
                
                
                cell.ProfileDescription.attributedText = attributedString
                
            }
        }else {
            cell.ProfileDescription.text = Description
        }
        
        return cell
    }
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        if is_myProfile == true {
            if indexPath.section == 0 {
                return 55
            }else if indexPath.section == 1 {
                return 40
            }else {
                return UITableViewAutomaticDimension
            }
        }else {
            if indexPath.section == 0 {
                return 55
            }else {
                return UITableViewAutomaticDimension
            }
        }
    }
    func tableView(tableView: UITableView, estimatedHeightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    // MARK: - WebServices
    func postUserProfile(params: NSDictionary) {
        socialDelegate?.showActivity!("Loading..")
        SP_NETWORK.postJson(WEB_SERVICE + "api/Account/UserView", method: "POST", params: params as! Dictionary<String, AnyObject>, is_Dictionary: true) { (Succeeded, Json) -> () in
            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                print(Succeeded)
                print(Json)
                self.socialDelegate?.removeActivity!()
                if Succeeded == true {
                    self.is_PageLoaded = true
                    if let status = Json["Status"] {
                        if status as? String == "1" {
                            if let Result = Json["UserDataView"] {
                                
                                if let userType = Result["SpecialInvitee"] {
                                    if userType as? String == "N" {
                                        self.is_myProfile = false
                                        IS_SPECIAL_INVITEE = false
                                        SP_DEFAULTS.setValue(false, forKey: "is_SpecialInvitee")
                                    } else {
                                        self.is_myProfile = true
                                        IS_SPECIAL_INVITEE = true
                                        SP_DEFAULTS.setValue(true, forKey: "is_SpecialInvitee")
                                    }
                                } else {
                                    IS_SPECIAL_INVITEE = false
                                    SP_DEFAULTS.setValue(false, forKey: "is_SpecialInvitee")
                                }
                                
                                if self.profileUserId == nil {
                                    if let url = NSURL(string: Result["ImageURL"] as! String) {
                                        if url != "" {
                                            self.ProfilePic.sd_setImageWithURL(url, placeholderImage: UIImage(named: "Profile_bg"), completed: { (UIImage, NSError, SDImageCacheType, url) -> Void in
                                                self.SaveImage(self.ProfilePic.image!)
                                            })
//                                    self.ProfilePic.sd_setImageWithURL(url, placeholderImage: UIImage(named: "Profile_bg"))
//                                    self.SaveImage(self.ProfilePic.image!)
                                        }
                                    }
                                }else {
                                    if let url = NSURL(string: Result["ImageURL"] as! String) {
                                        if url != "" {
                                            self.ProfilePic.sd_setImageWithURL(url, placeholderImage: UIImage(named: "Profile_bg"))
                                        }
                                    }
                                }
                                //                                let ImageParams = ["UserId": Result["UserId"]]
                                //                                self.postImage(ImageParams)
                                
                                
                                self.NickName = Result["NickName"] as? String
                                self.FirstName = Result["FirstName"] as? String
                                self.LastName = Result["LastName"] as? String
                                
                                if self.profileUserId == nil {
                                    USER_NAME = self.FirstName + " " + self.LastName
                                    SP_DEFAULTS.setValue(USER_NAME, forKey: "User_Name")
                                    self.UserName.text = USER_NAME
                                }else {
                                    self.UserName.text = self.FirstName + " " + self.LastName
                                }
                                
                                self.Description = (Result["Description"] as? String)!
                                self.FacebookProfile = Result["FBProfile"] as? String
                                self.TwitterProfile = Result["TweetProfile"] as? String
                                self.InstagramProfile = Result["InstaProfile"] as? String
                                self.Email = Result["Email"] as? String
                                self.ValueArray[0] = (Result["Team"] as? String)!
                                self.ValueArray[1] = (Result["Position"] as? String)!
                                self.ValueArray[2] = (Result["Jersey"] as? String)!
                                if let isUser = Result["Following"] {
                                    if isUser as? String == "N" {
                                        self.updateFollowButton(true)
                                    }else {
                                        self.updateFollowButton(false)
                                    }
                                }
                                if self.profileUserId == nil {
                                    USER_ID = (Result["UserId"] as? String)!
                                    SP_DEFAULTS.setValue(USER_ID!, forKey: "User_Id")
                                }else {
                                    self.profileUserId = (Result["UserId"] as? String)!
                                }
                                
                            }
                            if let follows_data = Json["UserFollowUpDataView"]
                            {
                                self.Follower = (follows_data["FollowerUserCount"] as? String)!
                                self.Following = (follows_data["FollowingUserCount"] as? String)!
                                self.PlayList = (follows_data["PlayListCount"] as? String)!
                            }
                        }
                    }
                }
                self.ProfileTable.reloadData()
            })
        }
    }
    func postFollowUser(params: NSDictionary) {
        socialDelegate?.showActivity!("Loading..")
        SP_NETWORK.postJson(WEB_SERVICE + "api/Account/UserFollowUpUpdate", method: "POST", params: params as! Dictionary<String, AnyObject>, is_Dictionary: true) { (Succeeded, Json) -> () in
            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                print(Succeeded)
                print(Json)
                self.socialDelegate?.removeActivity!()
                if Succeeded == true {
                    if let status = Json["Status"] {
                        if self.is_Follow == false {
                            self.followButton.setTitle("Unfollow", forState: UIControlState.Normal)
                            self.followButton.setBackgroundImage(UIImage(named: "unfollowButton.png"), forState: .Normal)
                            self.is_Follow = true
                        }else {
                            
                            
                            self.followButton.setTitle("Follow", forState: UIControlState.Normal)
                            self.followButton.setBackgroundImage(UIImage(named: "followButton.png"), forState: .Normal)
                            self.is_Follow = false
                        }
                    }
                }
                self.ProfileTable.reloadData()
            })
        }
    }
    
    
    
    func postImage(params: NSDictionary) {
        //socialDelegate?.showActivity!("Loading..")
        SP_NETWORK.postJson(WEB_SERVICE + "api/Account/SearchUserImage", method: "POST", params: params as! Dictionary<String, AnyObject>, is_Dictionary: true) { (Succeeded, Json) -> () in
            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                print(Succeeded)
                print(Json)
                //self.socialDelegate?.removeActivity!()
                if Succeeded == true {
                    self.ImageResults = Json["ResultSet"] as? NSDictionary
                    
                    if self.ImageResults != nil {
                        self.ImageString = self.ImageResults["Image"] as? String
                        
                        if PROFILE_PICTURE != "" {
                            SP_DELEGATE.removeFileFromPath("ProfilePicture")
                        }
                        
                        if self.ImageString != "" {
                            
                            self.ProfilePic.image = SP_DELEGATE.decodeImage(self.ImageString)
                            if self.profileUserId == nil {
                                PROFILE_PICTURE = SP_DELEGATE.storeImage(self.ProfilePic.image!, imageName: "ProfilePicture")
                                SP_DEFAULTS.setValue(PROFILE_PICTURE, forKey: "ProfilePic")
                            }
                            
                            
                        }
                    }else {
                        if PROFILE_PICTURE != "" {
                            SP_DELEGATE.removeFileFromPath("ProfilePicture")
                        }
                    }
                    
                }else {
                    if PROFILE_PICTURE != "" {
                        SP_DELEGATE.removeFileFromPath("ProfilePicture")
                    }
                }
                self.ProfileTable.reloadData()
            })
        }
    }
    
    // MARK: - Custom Delegates
    func hideLogoutButton(isHide: Bool) {
        socialDelegate?.hideLogoutButton!(isHide)
    }
    func navigateTab(tab: Int) {
        socialDelegate?.navigateTab!(tab)
    }
    func hideBackButton(isHide: Bool) {
        socialDelegate?.hideBackButton!(isHide)
    }
    func showActivity(title: String) {
        socialDelegate?.showActivity!(title)
    }
    func removeActivity() {
        socialDelegate?.removeActivity!()
    }
    // MARK: - System
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "Following" {
            let Following = segue.destinationViewController as! SPFollow
            Following.socialDelegate = self
            Following.toPass = passType
            if profileUserId == nil {
                Following.profileUserid = USER_ID
            }else {
                Following.profileUserid = profileUserId
            }
        }
    }
    
    
}
class SPProfileCell: UITableViewCell {
    
    @IBOutlet weak var Following: UILabel!
    @IBOutlet weak var PlayList: UILabel!
    @IBOutlet weak var Followers: UILabel!
    @IBOutlet weak var infoImage: UIImageView!
    @IBOutlet weak var infoLabel: UILabel!
    @IBOutlet weak var infoDetails: UILabel!
    @IBOutlet weak var ProfileDescription: UILabel!
    
}
