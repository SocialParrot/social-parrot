//
//  SPConstants.swift
//  Social Parrot
//
//  Created by Abdul Malik Aman on 11/06/16.
//  Copyright © 2016 INTELLIPATTERN. All rights reserved.
//

import UIKit

let App_Version = "1.0.0"
let APPDELEGATE = UIApplication.sharedApplication().delegate as? AppDelegate
//var WEB_SERVICE = "http://socialparrots.us-west-2.elasticbeanstalk.com/"
var WEB_SERVICE = "http://api.socialparrots.com/"
//var WEB_SERVICE = "http://192.168.0.111:8005/"
var USER_ID = SP_DEFAULTS.stringForKey("User_Id")
var EMAIL_ID = SP_DEFAULTS.stringForKey("mailId")
let THEME_COLOUR = UIColor(red: 0.7843, green: 0.1765, blue: 0.2, alpha: 1.0) /* #c82d33 */

// MARK:- Public Variables
let SP_DELEGATE: SPCustomDelegates = SPCustomDelegates()
let SP_NETWORK: SPNetwork = SPNetwork()
var IS_SESSION_EXPIRED = SP_DEFAULTS.boolForKey("Login")
var IS_SPECIAL_INVITEE = SP_DEFAULTS.boolForKey("is_SpecialInvitee")
let FILE_MANAGER = NSFileManager.defaultManager()
let FONT_NAME = "Helvetica-Light"
let FONT_NAME_B = "Helvetica-Bold"
let FONT_NAME_R = "Helvetica"
let SP_DEFAULTS = NSUserDefaults.standardUserDefaults()
let SP_DATE_FORMATTER = NSDateFormatter()
var IS_REACHABLE: Bool!
var USER_NAME = SP_DEFAULTS.stringForKey("User_Name")
var PROFILE_PICTURE = SP_DEFAULTS.stringForKey("ProfilePic")
var KEYBOARD_FRAME: CGFloat = 216.0
let Device = UIDevice.currentDevice()
var iPad =  (Device.userInterfaceIdiom == UIUserInterfaceIdiom.Pad)
var iPhone = (Device.userInterfaceIdiom == UIUserInterfaceIdiom.Phone)
var bounds: CGRect = UIScreen.mainScreen().bounds
var SCREEN_HEIGHT:CGFloat = bounds.size.height
