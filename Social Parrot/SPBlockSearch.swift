//
//  SPBlockSearch.swift
//  Social Parrot
//
//  Created by Intellipattern Mac book on 05/07/16.
//  Copyright © 2016 INTELLIPATTERN. All rights reserved.
//

import UIKit

class SPBlockSearch: UIViewController, SocialParrot_Delegate {
    
    @IBOutlet weak var blockingSubview: UIView!
    
    @IBOutlet weak var BlockSearchTable: UITableView!
    
    @IBOutlet weak var BlkSrchTxt: SPImagedTextFiled!
    
    
    @IBOutlet weak var blockSearchBar: UIImageView!
    @IBOutlet weak var blockingtLabel: UILabel!
    var blockedUserId: String!
    
    @IBAction func blockingCancel(sender: AnyObject) {
        
        self.blockingSubview.hidden = true
        
    }
    @IBAction func SrcBtn(sender: AnyObject) {
        
        BlkSrchTxt.resignFirstResponder()
        let params = ["UserId": USER_ID!, "Search": SP_DELEGATE.TrimString(BlkSrchTxt.text!)]
        postUnBlockedUsersGet(params)
        
    }
    
    @IBAction func blockingOk(sender: AnyObject) {
        
        let params = ["UserId": USER_ID,"BlockedUserId": blockedUserId,"Action": "Block"]
        postBlockedUsersUpdate(params)
        self.blockingSubview.hidden = true
        
    }
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        self.blockingSubview.hidden = true
    }
    
    var BlockSearchResults: NSArray!
    var newBlockSearchResults: NSArray!
    var ImageResults: NSDictionary!
    var followIndex: Int!
    
    // MARK: - Variables
    var socialDelegate: SocialParrot_Delegate?
    
    // MARK: - Actions
    
    override func viewWillAppear(animated: Bool) {
        /* self.navigationController?.navigationBarHidden = true
        socialDelegate?.navigateTab!(1)
        socialDelegate?.hideBackButton!(false)*/
        
        self.blockingSubview.hidden = true
        //let params = ["UserId": USER_ID!, "Favorite": "Y"]
        let params = ["UserId":USER_ID!, "Search": ""]
        postUnBlockedUsersGet(params)
    }
    
    
    // MARK: - TableView Delegate Methods
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if BlockSearchResults != nil {
            return BlockSearchResults.count
        }else {
            return 0
        }
    }
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
        
    }
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = BlockSearchTable.dequeueReusableCellWithIdentifier("BlksearchUser", forIndexPath: indexPath) as! BlockSrch
        cell.profileImage.layer.cornerRadius = 32.5
        cell.profileImage.layer.masksToBounds = true
        
        let firstName = BlockSearchResults[indexPath.row]["FirstName"] as? String
        let lastname = BlockSearchResults[indexPath.row]["LastName"] as? String
        cell.profileName.text = firstName! + " " + lastname!
        
        if let url = NSURL(string: BlockSearchResults[indexPath.row]["ImageURL"] as! String) {
            cell.profileImage.sd_setImageWithURL(url, placeholderImage: UIImage(named: "Profile_bg"))
        }
        
        
        return cell
    }
    
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        self.blockingSubview.hidden=false
        
        let firstName = BlockSearchResults[indexPath.row]["FirstName"] as? String
        let lastname = BlockSearchResults[indexPath.row]["LastName"] as? String
        blockedUserId = BlockSearchResults[indexPath.row]["UserId"] as? String
        // cellSearchName.text = firstName! + " " + lastname!
        // let fullName = firstName! + " " + lastname!
        
        
        let fullName = SP_DELEGATE.attributed(NSAttributedString(string: firstName! + " " + lastname!), isBold: true, underLine: 0, fontsize: 16, fontColor: UIColor.blackColor())
        
        let areYouSure = SP_DELEGATE.attributed(NSAttributedString(string: "Are yoy sure\n"), isBold: true, underLine: 0, fontsize: 16, fontColor: UIColor.blackColor())
        
        let wantToBlock = SP_DELEGATE.attributed(NSAttributedString(string: "Want to Block\n"), isBold: false, underLine: 0, fontsize: 12, fontColor: UIColor.blackColor())
        
        var blockLabelText: NSMutableAttributedString!
        
        blockLabelText = areYouSure
        blockLabelText.appendAttributedString(wantToBlock)
        blockLabelText.appendAttributedString(fullName)
        
        self.blockingtLabel.attributedText = blockLabelText
        
        
    }
    
    // MARK: - Custom Delegates
    func hideLogoutButton(isHide: Bool) {
        socialDelegate?.hideLogoutButton!(isHide)
    }
    func navigateTab(tab: Int) {
        socialDelegate?.navigateTab!(tab)
    }
    func hideBackButton(isHide: Bool) {
        socialDelegate?.hideBackButton!(isHide)
    }
    func showActivity(title: String) {
        socialDelegate?.showActivity!(title)
    }
    func removeActivity() {
        socialDelegate?.removeActivity!()
    }
    
    /*
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    // Get the new view controller using segue.destinationViewController.
    // Pass the selected object to the new view controller.
    }
    */
    
    
    // MARK: - TextField Delegates
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        let params = ["UserId": USER_ID!, "Search": SP_DELEGATE.TrimString(textField.text!)]
        postUnBlockedUsersGet(params)
        return true
    }

    
    func postUnBlockedUsersGet(params: NSDictionary) {
        socialDelegate?.showActivity!("Loading..")
        SP_NETWORK.postJson(WEB_SERVICE + "api/Account/UnBlockedUsersGet", method: "POST", params: params as! Dictionary<String, AnyObject>, is_Dictionary: true) { (Succeeded, Json) -> () in
            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                print(Succeeded)
                print(Json)
                self.socialDelegate?.removeActivity!()
                if Succeeded == true {
                    self.BlockSearchResults = Json["ResultSet"] as? NSArray
                }
                self.BlockSearchTable.reloadData()
            })
        }
    }
    
    
    func postBlockedUsersUpdate(params: NSDictionary) {
        socialDelegate?.showActivity!("Blocking..")
        SP_NETWORK.postJson(WEB_SERVICE + "api/Account/BlockedUsersUpdate", method: "POST", params: params as! Dictionary<String, AnyObject>, is_Dictionary: true) { (Succeeded, Json) -> () in
            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                print(Succeeded)
                print(Json)
                self.socialDelegate?.removeActivity!()
                if Succeeded == true {
                    self.newBlockSearchResults = Json["ResultSet"] as? NSArray
                    let params = ["UserId": USER_ID!, "Favorite": "Y"]
                    
                    
                    
                    if let status = Json["Status"] {
                        if status as? String == "1" {
                            let params = ["UserId":USER_ID!, "Search": ""]
                            self.postUnBlockedUsersGet(params)
                            
                            SP_DELEGATE.showAlert("Message", message: "Blocked", buttonTitle: "OK")
                            
                            //let alertsong = UIAlertController(title: nil, message: "Blocked", preferredStyle: .Alert)
                            // self.presentViewController(alertsong, animated: true, completion: nil)
                            // [NSThread .sleepForTimeInterval(3)]
                            //  alertsong.dismissViewControllerAnimated(true, completion: nil)
                            
                        }else {
                            SP_DELEGATE.showAlert("Message", message: "Blocking failed", buttonTitle: "OK")
                        }
                        
                    }
                    
                    
                    //self.postSearchAllUser(params)
                }
                //  self.BlockSearchTable.reloadData()
            })
        }
    }
    
}


class BlockSrch: UITableViewCell {
    
    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var profileName: UILabel!
    @IBOutlet weak var BlockImg: UIImageView!
    
}
