//
//  SPBlockedProfiles.swift
//  Social Parrot
//
//  Created by Abdul Malik Aman on 24/06/16.
//  Copyright © 2016 INTELLIPATTERN. All rights reserved.
//

import UIKit

class SPBlockedProfiles: UIViewController, SocialParrot_Delegate {
    
    // MARK: - Variables
    var socialDelegate: SocialParrot_Delegate?
    var Blocked_Array: NSArray!
    var newUnBlockSearchResults: NSArray!
    
    @IBOutlet weak var UnblockingSubview: UIView!
    @IBOutlet weak var unblockingtLabel: UILabel!
    @IBOutlet weak var tableBlocked: UITableView!

    var unblockedUserId: String!
    var ImageResults: NSDictionary!
    
    @IBAction func unblockingCancel(sender: AnyObject) {
        
        self.UnblockingSubview.hidden = true
    }
    
    @IBAction func unblockingOk(sender: AnyObject) {
        
        let params = ["UserId": USER_ID,"BlockedUserId": unblockedUserId,"Action": "Unblock"]
        postUnBlockedUsersUpdate(params)
        self.UnblockingSubview.hidden = true
    }
    // MARK: - View Methods
    override func viewWillDisappear(animated: Bool) {
        socialDelegate?.hideBackButton!(false)
        socialDelegate?.hideLogoutButton!(false)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.hidden=true
        
       // let params = ["UserId": USER_ID!]
          //let params = ["UserId": "c2fc3ffb-4b1a-4a9f-8e1f-7d3e8ba6d43d"]
    
     //   postBlocked(params)
      
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(animated: Bool) {
        self.navigationController?.navigationBarHidden = true
        socialDelegate?.hideBackButton!(false)
        socialDelegate?.hideLogoutButton!(true)
        
        self.UnblockingSubview.hidden = true
        let params = ["UserId": USER_ID!]
        //let params = ["UserId": "c2fc3ffb-4b1a-4a9f-8e1f-7d3e8ba6d43d"]
        
        postBlocked(params)

    }
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        self.UnblockingSubview.hidden = true
    }
    
    
    @IBAction func AddBlockUser(sender: AnyObject) {
        
    performSegueWithIdentifier("BlksearchUser", sender: self)
    
        
    }
    
    
    
    // MARK: - TableView Delegate Methods
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if Blocked_Array != nil {
            print("count...",Blocked_Array.count)
            return Blocked_Array.count
        }else {
            return 0
        }
    }
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableBlocked.dequeueReusableCellWithIdentifier("Blocked", forIndexPath: indexPath) as! SPBlockedCell
        cell.profileImg.layer.cornerRadius = 26
        cell.profileImg.layer.masksToBounds = true
        
        if let FirstName = Blocked_Array[indexPath.row]["FirstName"] as? String {
            if let lastName = Blocked_Array[indexPath.row]["LastName"] as? String {
                cell.profileNam.text = FirstName + " " + lastName
            }
            
        }
        

        
        if let url = NSURL(string: Blocked_Array[indexPath.row]["ImageURL"] as! String) {
            cell.profileImg.sd_setImageWithURL(url, placeholderImage: UIImage(named: "Profile_bg"))
        }
        
        return cell
    }
    
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        self.UnblockingSubview.hidden=false
        
        let firstName = Blocked_Array[indexPath.row]["FirstName"] as? String
        let lastname = Blocked_Array[indexPath.row]["LastName"] as? String
        unblockedUserId = Blocked_Array[indexPath.row]["BlockedUserId"] as? String
        // cellSearchName.text = firstName! + " " + lastname!
        // let fullName = firstName! + " " + lastname!
        
        
        let fullName = SP_DELEGATE.attributed(NSAttributedString(string: firstName! + " " + lastname!), isBold: true, underLine: 0, fontsize: 16, fontColor: UIColor.blackColor())
        
        let areYouSure = SP_DELEGATE.attributed(NSAttributedString(string: "Are you sure\n"), isBold: true, underLine: 0, fontsize: 16, fontColor: UIColor.blackColor())
        
        let wantToBlock = SP_DELEGATE.attributed(NSAttributedString(string: "Want to Unblock\n"), isBold: false, underLine: 0, fontsize: 12, fontColor: UIColor.blackColor())
        
        var blockLabelText: NSMutableAttributedString!
        
        blockLabelText = areYouSure
        blockLabelText.appendAttributedString(wantToBlock)
        blockLabelText.appendAttributedString(fullName)
        
        self.unblockingtLabel.attributedText = blockLabelText
        print("msg...",fullName)
        
    }
    
    
    // MARK: - Custom Delegates
    func hideLogoutButton(isHide: Bool) {
        socialDelegate?.hideLogoutButton!(isHide)
    }
    func navigateTab(tab: Int) {
        socialDelegate?.navigateTab!(tab)
    }
    func hideBackButton(isHide: Bool) {
        socialDelegate?.hideBackButton!(isHide)
    }
    func showActivity(title: String) {
        socialDelegate?.showActivity!(title)
    }
    func removeActivity() {
        socialDelegate?.removeActivity!()
    }

    
    // MARK: - WebServices
    func postBlocked(params: NSDictionary) {
        socialDelegate?.showActivity!("Loading..")
        SP_NETWORK.postJson(WEB_SERVICE + "api/Account/BlockedUsersGet", method: "POST", params: params as! Dictionary<String, AnyObject>, is_Dictionary: true) { (Succeeded, Json) -> () in
            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                print(Succeeded)
                print(Json)
                self.socialDelegate?.removeActivity!()
                if Succeeded == true {
                    if let status = Json["Status"] {
                        if status as? String == "1" {
                            
                            self.Blocked_Array = Json["ResultSet"] as! NSArray
                        }
                        else if status as? String == "0" {
                            
                            self.Blocked_Array = nil
                        }
                    }
                }
                self.tableBlocked.reloadData()
            })
        }
    }


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "BlksearchUser" {
            let searchBlock = segue.destinationViewController as? SPBlockSearch
            searchBlock?.socialDelegate = self
        }

    }

    func postUnBlockedUsersUpdate(params: NSDictionary) {
        socialDelegate?.showActivity!("Unblocking..")
        SP_NETWORK.postJson(WEB_SERVICE + "api/Account/BlockedUsersUpdate", method: "POST", params: params as! Dictionary<String, AnyObject>, is_Dictionary: true) { (Succeeded, Json) -> () in
            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                print(Succeeded)
                print(Json)
                self.socialDelegate?.removeActivity!()
                if Succeeded == true {
                    self.newUnBlockSearchResults = Json["ResultSet"] as? NSArray
                    let params = ["UserId": USER_ID!, "Favorite": "Y"]
                    
                    
                    
                    if let status = Json["Status"] {
                        if status as? String == "1" {
                            let params = ["UserId":USER_ID!]
                            self.postBlocked(params)
                            
                            SP_DELEGATE.showAlert("Message", message: "Unblocked", buttonTitle: "OK")
                            
                            
                            //let alertsong = UIAlertController(title: nil, message: "Blocked", preferredStyle: .Alert)
                            // self.presentViewController(alertsong, animated: true, completion: nil)
                            // [NSThread .sleepForTimeInterval(3)]
                            //  alertsong.dismissViewControllerAnimated(true, completion: nil)
                            
                        }else {
                            SP_DELEGATE.showAlert("Message", message: "Unblocking failed", buttonTitle: "OK")
                        }
                        
                    }
                    
                    
                    //self.postSearchAllUser(params)
                }
                //  self.BlockSearchTable.reloadData()
            })
        }
    }

}

class SPBlockedCell: UITableViewCell {
    
    @IBOutlet weak var profileImg: UIImageView!
   
    @IBOutlet weak var profileNam: UILabel!
    
    
}

