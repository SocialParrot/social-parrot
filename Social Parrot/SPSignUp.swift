//
//  SPSignUp.swift
//  Social Parrot
//
//  Created by Abdul Malik Aman on 11/06/16.
//  Copyright © 2016 INTELLIPATTERN. All rights reserved.
//

import UIKit

class SPSignUp: UIViewController {

    // MARK: - Variables
    @IBOutlet weak var SignUpTable: UITableView!
    
    @IBOutlet weak var signUpLabel: UILabel!
    var is_Falied: Int!
    var errorMessage: String!
    let PlaceHolderArray = ["E-mail Address", "Username", "", "Password", "Confirm password"]
    var ValueArray: [String] = [String](count: 5, repeatedValue: "")
    var table_h: CGFloat!
    var is_keyboard_up = false
    var becomeResponder: Int!
    var is_Agree = false
    var is_SpecialInvitee = false
    
    // MARK: - View Methods
    override func viewDidLayoutSubviews() {
        if is_keyboard_up == true {
            SignUpTable.frame.size.height = self.view.frame.height - (KEYBOARD_FRAME + 105)
        }else {
            SignUpTable.frame.size.height = table_h
        }
    }
    override func viewWillAppear(animated: Bool) {
        self.view.endEditing(true)
        print(ValueArray)
        is_Falied = nil
        errorMessage = ""
        becomeResponder = nil
//        ValueArray = [String](count: 5, repeatedValue: "")
        SignUpTable.reloadData()
        self.navigationController?.navigationBarHidden = true
        
        if is_SpecialInvitee == true
        {
            self.signUpLabel.text = "Special Invitee SignUp"
            self.signUpLabel.adjustsFontSizeToFitWidth = true
            self.signUpLabel.font = UIFont (name: "Helvetica Neue", size: 25)
        }
        
    }
    override func viewDidLoad() {
        super.viewDidLoad()

        table_h = SignUpTable.frame.size.height
        // Do any additional setup after loading the view.
    }

    // MARK:- Actions
    @IBAction func Reset(sender: AnyObject) {
        self.view.endEditing(true)
        is_Falied = nil
        errorMessage = ""
        becomeResponder = nil
        ValueArray = [String](count: 5, repeatedValue: "")
        SignUpTable.reloadData()
    }
    @IBAction func SignUp(sender: AnyObject) {
        self.view.endEditing(true)
        ValidateSignUp()
    }
    @IBAction func Back(sender: AnyObject) {
        self.view.endEditing(true)
        self.navigationController?.popViewControllerAnimated(true)
    }
    @IBAction func AgreementSignIng(sender: AnyObject) {
        if is_Agree == false {
            is_Agree = true
        }else {
            is_Agree = false
        }
        SignUpTable.reloadData()
    }
    func ValidateSignUp() {
        is_Falied = nil
        becomeResponder = nil
        errorMessage = ""
        if SP_DELEGATE.TrimString(ValueArray[0]) == "" {
            is_Falied = 0
            errorMessage = "*Enter E-mail Address"
        }else if SP_DELEGATE.isValidEmail(ValueArray[0]) == false {
            is_Falied = 0
            errorMessage = "*Enter a valid E-mail id"
        }else if SP_DELEGATE.TrimString(ValueArray[1]) == "" {
            is_Falied = 1
            errorMessage = "*Enter User Name"
        }else if SP_DELEGATE.TrimString(ValueArray[3]) == "" {
            is_Falied = 3
            errorMessage = "*Enter Password"
        }else if ValueArray[3].characters.count < 7 {
            is_Falied = 3
            errorMessage = "*Enter atlease 7 characters"
        }else if SP_DELEGATE.TrimString(ValueArray[4]) == "" {
            is_Falied = 4
            errorMessage = "*Confirm Password"
        }else if ValueArray[3] != ValueArray[4] {
            is_Falied = 4
            errorMessage = "*Password Doesnot Match"
        }else if is_Agree == false {
            is_Falied = 5
        }else {
            if is_SpecialInvitee == true{
                let params: NSDictionary = ["Email": ValueArray[0],
                    "NickName": ValueArray[1],
                    "Password": ValueArray[3],
                    "ConfirmPassword": ValueArray[4],
                    "SpecialInvitee": "Y"]
                postSignUp(params)
            }
            else{
                
                let params: NSDictionary = ["Email": ValueArray[0],
                    "NickName": ValueArray[1],
                    "Password": ValueArray[3],
                    "ConfirmPassword": ValueArray[4],
                    "SpecialInvitee": "N"]
                postSignUp(params)
            }
        }
        SignUpTable.reloadData()
    }
    func scrollToTop(row: Int, section: Int, table: UITableView) {
        let indexPath = NSIndexPath(forRow: row, inSection: section)
        table.scrollToRowAtIndexPath(indexPath, atScrollPosition: .Top, animated: true)
    }
    
    // MARK:- TableView
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 6
    }
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var identifier: String!
        if indexPath.row == 2 {
            identifier = "label"
        }else if indexPath.row == 5 {
            identifier = "button"
        }else {
            identifier = "text"
        }
        let cell = SignUpTable.dequeueReusableCellWithIdentifier(identifier, forIndexPath: indexPath) as! SPSignUpCell
        if indexPath.row != 2 && indexPath.row != 5 {
            cell.AlertLabel.hidden = true
            cell.SignUpText.tag = indexPath.row
            cell.SignUpText.placeholder = PlaceHolderArray[indexPath.row]
            cell.SignUpText.text = ValueArray[indexPath.row]
            
            if indexPath.row != 4 {
                cell.SignUpText.returnKeyType = UIReturnKeyType.Next
            }else {
                cell.SignUpText.returnKeyType = UIReturnKeyType.Default
            }
            if is_Falied != nil {
                if indexPath.row == is_Falied {
                    cell.AlertLabel.hidden = false
                    cell.AlertLabel.text = errorMessage
                    SP_DELEGATE.ShakeAnimation(cell.AlertLabel)
                }
            }
            if becomeResponder != nil {
                if indexPath.row == becomeResponder {
                    cell.SignUpText.becomeFirstResponder()
                }
            }
        }
        if indexPath.row == 3 || indexPath.row == 4 {
            cell.SignUpText.secureTextEntry = true
            cell.SignUpText.Image_Name = "password"
        }else if indexPath.row == 5 {
            if is_Agree == false {
                cell.AgreementImage.image = UIImage(named: "uncheck")
            }else {
                cell.AgreementImage.image = UIImage(named: "checkin")
            }
            if is_Falied != nil {
                if is_Falied == 5 {
                    SP_DELEGATE.ShakeAnimation(cell.AgreementImage)
                }
            }
            
        }else {
            if indexPath.row == 0 {
                cell.SignUpText.secureTextEntry = false
                cell.SignUpText.Image_Name = "mail"
                
            }else if indexPath.row == 1 {
                cell.SignUpText.secureTextEntry = false
                cell.SignUpText.Image_Name = "userIcon"
            }else if indexPath.row == 5 {
                cell.AgreementLabel.adjustsFontSizeToFitWidth = true
            }
        }
        return cell
    }
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
    }
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        if indexPath.row == 2 {
            return 35
        }else if indexPath.row == 5 {
            return 70
        }else {
            return 60
        }
    }
    
    // MARK: - TextField Delegates
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        is_Falied = nil
        if textField.tag != 4 {
            if textField.tag == 1 {
                becomeResponder = textField.tag + 2
            }else {
                becomeResponder = textField.tag + 1
            }
        }else {
            becomeResponder = nil
            textField.resignFirstResponder()
        }
        SignUpTable.reloadData()
        return true
    }
    func textFieldDidBeginEditing(textField: UITextField) {
        is_keyboard_up = true
        scrollToTop(textField.tag, section: 0, table: SignUpTable)
        viewDidLayoutSubviews()
    }
    func textFieldDidEndEditing(textField: UITextField) {
        is_keyboard_up = false
        ValueArray[textField.tag] = textField.text!
        viewDidLayoutSubviews()
    }
    
    // MARK: - WebServices
    func postSignUp(params: NSDictionary) {
        SP_DELEGATE.showActivity(self.view, myTitle: "Loading..")
        SP_NETWORK.postJson(WEB_SERVICE + "api/Account/Register", method: "POST", params: params as! Dictionary<String, AnyObject>, is_Dictionary: true) { (Succeeded, Json) -> () in
            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                print(Succeeded)
                print(Json)
                SP_DELEGATE.removeActivity(self.view)
                if Succeeded == true {
                    if let status = Json["Status"] {
                        if status as? String == "1" {
                            EMAIL_ID = self.ValueArray[0]
                            SP_DEFAULTS.setValue(EMAIL_ID, forKey: "mailId")
                            SP_DEFAULTS.setValue(Json["Id"], forKey: "User_Id")
                            self.performSegueWithIdentifier("Registration", sender: self)
                            
                        }else {
                            let messages = Json["Message"] as! NSArray
                            SP_DELEGATE.showAlert("Message", message: messages[0] as! String, buttonTitle: "OK")
                        }
                        
                    }
                }
                self.SignUpTable.reloadData()
            })
        }
    }
    // MARK: - System
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "Registration" {
            let Registration = segue.destinationViewController as! SPRegistration
            Registration.is_SpecialInvitee = is_SpecialInvitee
            Registration.valueArray[8] = ValueArray[0]
            Registration.valueArray[9] = ValueArray[1]
        }
    }


}
class SPSignUpCell: UITableViewCell {
    
    @IBOutlet weak var SignUpText: SPImagedTextFiled!
    @IBOutlet weak var AlertLabel: UILabel!
    @IBOutlet weak var AgreementLabel: UILabel!
    @IBOutlet weak var AgreementImage: UIImageView!
    
}
