//
//  SPFollow.swift
//  Social Parrot
//
//  Created by Abdul Malik Aman on 19/06/16.
//  Copyright © 2016 INTELLIPATTERN. All rights reserved.
//

import UIKit

class SPFollow: UIViewController,SocialParrot_Delegate {

    // MARK: - Variables
    var socialDelegate: SocialParrot_Delegate?
    var Follow_Array: NSArray!
    var toPass: Int!
    var profileUserid: String!
    var SearchResults: NSArray!
    var rootContanerView = SPRootTab()
    @IBOutlet weak var followTable: UITableView!
    @IBOutlet weak var titleFollow: UILabel!
    @IBOutlet weak var SearchBar: UISearchBar!
    var ImageResults: NSDictionary!
    
    @IBOutlet weak var popupLabel: UILabel!
    @IBOutlet weak var followSubView: UIView!
    
    @IBOutlet weak var popupOkOutlet: UIButton!
    var followIndex: Int!

    @IBOutlet weak var BackButton: SPRoundButton!
    
    // MARK: - View Methods
    override func viewWillAppear(animated: Bool) {
        
        self.followSubView.hidden = true
        
        if profileUserid == USER_ID {
            BackButton.hidden = false
            socialDelegate?.hideBackButton!(true)
        }else {
            BackButton.hidden = false
            socialDelegate?.hideBackButton!(true)
        }
        if(toPass==1) {
            let params = ["Action": "Following", "UserId": profileUserid!, "Search": ""]
            postFollower(params)
            titleFollow.text = "Following"
            
        }else if(toPass==2) {
            let params = ["Action": "Follower", "UserId": profileUserid!, "Search": ""]
            postFollower(params)
            titleFollow.text = "Follower"
        }

    }
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }

    // MARK: - Actions
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        self.followSubView.hidden = true
    }
    @IBAction func Back(sender: AnyObject) {
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    @IBAction func FollowBtn(sender: AnyObject) {
        
        followIndex = sender.tag!
        
        if toPass == 1{
            popupOkOutlet.backgroundColor = UIColor(red: 255/255, green: 12/255, blue: 16/255, alpha: 0.51)
            popupOkOutlet.setTitle("Unfollow", forState: .Normal)
            
            let firstName = Follow_Array[self.followIndex]["FirstName"]as? String
                let lastName = Follow_Array[self.followIndex]["LastName"]as? String
            
            let fullName = SP_DELEGATE.attributed(NSAttributedString(string: firstName! + " " + lastName!), isBold: true, underLine: 0, fontsize: 16, fontColor: UIColor.blackColor())
            
            let areYouSure = SP_DELEGATE.attributed(NSAttributedString(string: "Are you sure\n"), isBold: true, underLine: 0, fontsize: 16, fontColor: UIColor.blackColor())
            
            let wantToBlock = SP_DELEGATE.attributed(NSAttributedString(string: "Want to Unfollow\n"), isBold: false, underLine: 0, fontsize: 12, fontColor: UIColor.blackColor())
            
            var blockLabelText: NSMutableAttributedString!
            
            blockLabelText = areYouSure
            blockLabelText.appendAttributedString(wantToBlock)
            blockLabelText.appendAttributedString(fullName)
            
            self.popupLabel.attributedText = blockLabelText
            
            if Follow_Array[self.followIndex]["FollowerId"] as? String == USER_ID {
                self.followSubView.hidden = false
            }
        }
        else if toPass == 2{
            
            if Follow_Array[self.followIndex]["ReFollow"] as? String == "Y"{
                popupOkOutlet.backgroundColor =  UIColor(red: 255/255, green: 12/255, blue: 16/255, alpha: 0.51)
                popupOkOutlet.setTitle("Unfollow", forState: .Normal)
            
            
            let firstName = Follow_Array[self.followIndex]["FirstName"]as? String
            let lastName = Follow_Array[self.followIndex]["LastName"]as? String
            
            
            let fullName = SP_DELEGATE.attributed(NSAttributedString(string: firstName! + " " + lastName!), isBold: true, underLine: 0, fontsize: 16, fontColor: UIColor.blackColor())
            
            let areYouSure = SP_DELEGATE.attributed(NSAttributedString(string: "Are you sure\n"), isBold: true, underLine: 0, fontsize: 16, fontColor: UIColor.blackColor())
            
            let wantToBlock = SP_DELEGATE.attributed(NSAttributedString(string: "Want to Unfollow\n"), isBold: false, underLine: 0, fontsize: 12, fontColor: UIColor.blackColor())
            
            var blockLabelText: NSMutableAttributedString!
            
            blockLabelText = areYouSure
            blockLabelText.appendAttributedString(wantToBlock)
            blockLabelText.appendAttributedString(fullName)
            
            self.popupLabel.attributedText = blockLabelText
            }else{
                popupOkOutlet.backgroundColor = UIColor(red: 16/255, green: 187/255, blue: 16/255, alpha: 0.51)
                popupOkOutlet.setTitle("Follow", forState: .Normal)
                
                
                let firstName = Follow_Array[self.followIndex]["FirstName"]as? String
                let lastName = Follow_Array[self.followIndex]["LastName"]as? String
                
                
                let fullName = SP_DELEGATE.attributed(NSAttributedString(string: firstName! + " " + lastName!), isBold: true, underLine: 0, fontsize: 16, fontColor: UIColor.blackColor())
                
                let areYouSure = SP_DELEGATE.attributed(NSAttributedString(string: "Are you sure\n"), isBold: true, underLine: 0, fontsize: 16, fontColor: UIColor.blackColor())
                
                let wantToBlock = SP_DELEGATE.attributed(NSAttributedString(string: "Want to Follow\n"), isBold: false, underLine: 0, fontsize: 12, fontColor: UIColor.blackColor())
                
                var blockLabelText: NSMutableAttributedString!
                
                blockLabelText = areYouSure
                blockLabelText.appendAttributedString(wantToBlock)
                blockLabelText.appendAttributedString(fullName)
                
                self.popupLabel.attributedText = blockLabelText
            }
            if Follow_Array[self.followIndex]["FollowingId"] as? String == USER_ID {
                self.followSubView.hidden = false
            }
        }
    }
    
    
    @IBAction func popupCancel(sender: AnyObject) {
        self.followSubView.hidden = true
    }
    
    
    @IBAction func popupOk(sender: AnyObject) {
    if toPass == 1{
        let params = ["FollowerId": Follow_Array[self.followIndex]["FollowerId"], "FollowingId": Follow_Array[self.followIndex]["FollowingId"], "Follow": "N"]
        postFollowUser(params)
    }else if(toPass==2)
    {
        if Follow_Array[self.followIndex]["ReFollow"] as? String == "Y"{
        let params = ["FollowingId": Follow_Array[self.followIndex]["FollowerId"], "FollowerId": Follow_Array[self.followIndex]["FollowingId"], "Follow": "N"]
            print(params)
        postFollowUser(params)
        }else{
            let params = ["FollowingId": Follow_Array[self.followIndex]["FollowerId"], "FollowerId": Follow_Array[self.followIndex]["FollowingId"], "Follow": "Y"]
            print(params)
            postFollowUser(params)
        }
    }
    }

    
    // MARK: - TableView Delegate Methods
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if Follow_Array != nil {
            return Follow_Array.count
        }else {
            return 0
        }
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
    
        let cell = followTable.dequeueReusableCellWithIdentifier("follow", forIndexPath: indexPath) as! SPFollowCell
        cell.profileImage.layer.cornerRadius = 26
        cell.profileImage.layer.masksToBounds = true
        cell.followingIcon.tag = indexPath.row
        
        if(toPass==1) {
            cell.followingIcon.setBackgroundImage(UIImage(named: "followMinus"), forState: UIControlState.Normal)
            
            if Follow_Array[indexPath.row]["FollowerId"] as? String == USER_ID {
                cell.followingIcon.hidden = false
            }else {
                cell.followingIcon.hidden = true
            }
        }
        else if(toPass==2)
        {
            if Follow_Array[indexPath.row]["ReFollow"] as? String == "Y"{
                cell.followingIcon.setBackgroundImage(UIImage(named: "following"), forState: UIControlState.Normal)
            }else {
                cell.followingIcon.setBackgroundImage(UIImage(named: "followPlus"), forState: UIControlState.Normal)
            }
            
            if Follow_Array[indexPath.row]["FollowingId"] as? String == USER_ID {
                cell.followingIcon.hidden = false
            }else {
                cell.followingIcon.hidden = true
            }
        }
        
        if let FirstName = Follow_Array[indexPath.row]["FirstName"] as? String {
            if let lastName = Follow_Array[indexPath.row]["LastName"] as? String {
                cell.profileName.text = FirstName + " " + lastName
            }
            
        }
        
        if let url = NSURL(string: Follow_Array[indexPath.row]["ImageURL"] as! String) {
            cell.profileImage.sd_setImageWithURL(url, placeholderImage: UIImage(named: "Profile_bg"))
        }
        
     
        
        return cell
    }
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        var forKey: String!
        if(toPass==1) {
            forKey = "FollowingId"
        }else if(toPass==2) {
            forKey = "FollowerId"
        }
        let profile = storyboard?.instantiateViewControllerWithIdentifier("SPProfile") as? SPProfile
        profile!.profileUserId = Follow_Array[indexPath.row][forKey] as? String
        
        print("pro_id...",Follow_Array[indexPath.row][forKey] as? String)
        print("pro_id...",Follow_Array[indexPath.row])
        profile?.socialDelegate = self
        showViewController(profile!, sender: self)
    }
    
    // MARK: - Custom Delegates
    func hideLogoutButton(isHide: Bool) {
        socialDelegate?.hideLogoutButton!(isHide)
    }
    func navigateTab(tab: Int) {
        socialDelegate?.navigateTab!(tab)
    }
    func hideBackButton(isHide: Bool) {
        socialDelegate?.hideBackButton!(isHide)
    }
    func showActivity(title: String) {
        socialDelegate?.showActivity!(title)
    }
    func removeActivity() {
        socialDelegate?.removeActivity!()
    }
    
    // MARK: - SearchBar Delegate
    func searchBarSearchButtonClicked(searchBar: UISearchBar) {
        if(toPass==1) {
            let params = ["Action": "Following", "UserId": profileUserid!, "Search": SP_DELEGATE.TrimString(searchBar.text!)]
            postFollower(params)
        }else if(toPass==2) {
            let params = ["Action": "Follower", "UserId": profileUserid!, "Search": SP_DELEGATE.TrimString(searchBar.text!)]
            postFollower(params)
        }
        searchBar.resignFirstResponder()
    }
    
    // MARK: - WebServices
    func postFollower(params: NSDictionary) {
        socialDelegate?.showActivity!("Loading..")
        SP_NETWORK.postJson(WEB_SERVICE + "api/Account/UserFollowView", method: "POST", params: params as! Dictionary<String, AnyObject>, is_Dictionary: true) { (Succeeded, Json) -> () in
            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                print(Succeeded)
                print(Json)
                self.socialDelegate?.removeActivity!()
                if Succeeded == true {
                    if let status = Json["Status"] {
                        if status as? String == "1" {
                            self.Follow_Array = Json["ResultSet"] as! NSArray
                        }
                        else if status as? String == "0" {
                            
                            self.Follow_Array = nil
                        }
                    }
                }
                self.followTable.reloadData()
            })
        }
    }
    
    func postFollowUser(params: NSDictionary) {
        socialDelegate?.showActivity!("Loading..")
        SP_NETWORK.postJson(WEB_SERVICE + "api/Account/UserFollowUpUpdate", method: "POST", params: params as! Dictionary<String, AnyObject>, is_Dictionary: true) { (Succeeded, Json) -> () in
            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                print(Succeeded)
                print(Json)
                self.socialDelegate?.removeActivity!()
                if Succeeded == true {
                    if let status = Json["Status"] {
                        if status as? String == "1" {
                           
                            self.followSubView.hidden = true
                            
                            if(self.toPass==1) {
                                let params = ["Action": "Following", "UserId": self.profileUserid!, "Search": ""]
                                self.postFollower(params)
                            }else if(self.toPass==2) {
                                if self.Follow_Array[self.followIndex]["ReFollow"] as? String == "Y"{
                                    let params = ["Action": "Follower", "UserId": self.profileUserid!, "Search": ""]
                                self.postFollower(params)
                                }else{
                                    let params = ["Action": "Follower", "UserId": self.profileUserid!, "Search": ""]
                                    self.postFollower(params)
                                }
                            }
                            
                        }
                    }
                }
                self.followTable.reloadData()
            })
        }
    }
    
    
    // MARK: - System
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

    
}

class SPFollowCell: UITableViewCell {
    
    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var profileName: UILabel!
    @IBOutlet weak var followingIcon: UIButton!
    
}





