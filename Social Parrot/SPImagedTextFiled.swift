//
//  SPImagedTextFiled.swift
//  Social Parrot
//
//  Created by Abdul Malik Aman on 10/06/16.
//  Copyright © 2016 INTELLIPATTERN. All rights reserved.
//

import UIKit

class SPImagedTextFiled: UITextField {

    @IBInspectable var Boarder_Colour: UIColor = UIColor.clearColor()
    @IBInspectable var Boarder_Width: CGFloat = 1.0
    @IBInspectable var Corner_Radius: CGFloat = 1.0
    @IBInspectable var Padding_BG: UIColor = UIColor.clearColor()
    @IBInspectable var Image_Name: String = ""
    @IBInspectable var Padding: CGFloat = 0
    
    override func drawRect(rect: CGRect) {
        
        self.layer.borderColor = Boarder_Colour.CGColor
        self.layer.borderWidth = Boarder_Width
        self.layer.cornerRadius = Corner_Radius
        
        let intendView = UIView(frame: CGRectMake(0, 0, Padding, self.frame.height))
        let intendImageView = UIImageView(frame: CGRectMake(0, 0, Padding - 5, self.frame.height))
        intendImageView.contentMode = UIViewContentMode.ScaleAspectFit
        intendView.backgroundColor = Padding_BG
        intendView.addSubview(intendImageView)
        intendImageView.image = UIImage(named: Image_Name)
        
        self.leftView = intendView
        self.leftViewMode = UITextFieldViewMode.Always
    }

}
